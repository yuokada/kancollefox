Components.utils.import("resource://kancollefoxmodules/windows.jsm");
Components.utils.import("resource://kancollefoxmodules/callback.jsm");

KancolleFoxConfig.checkConfigVersion();
KancolleFoxCallBack.initialize(localData);

var KancolleFoxOverlay =
{
	KancolleFoxWindows: {},

	launchOpen: function()
	{
		for (var id in functionNames)
		{
			var checked = KancolleFoxConfig.loadLaunchEnabled("settings", "launch." + id);
			if (checked) KancolleFoxOverlay.open(id);
		}
	},

	open: function(windowName)
	{
		var rectParam = "centerscreen,width=1024,height=640";
		var ext = ".html";

		switch (windowName)
		{
			case "kanmado":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 800, 480);
				ext = ".xul";
				break;

			case "ship":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 326, 550);
				break;

			case "deck":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 725, 845);
				break;

			case "arsenal":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 353, 107);
				break;

			case "armory":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 387, 302);
				break;

			case "checker":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 498, 383);
				break;

			case "timer":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 380, 425);
				break;

			case "ndockCalc":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 725, 330);
				break;

			case "collection":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 545, 299);
				break;

			case "capture":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 640, 69);
				break;

			case "battle":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 465, 382);
				break;

			case "settings":
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 256, 480);
				break;

			default:
				rectParam = KancolleFoxOverlay.createRectParam(windowName, 512, 256);
				break;
		}

		if (
			KancolleFoxWindows[windowName] &&
			KancolleFoxWindows[windowName] != null &&
			KancolleFoxWindows[windowName] != undefined &&
			! KancolleFoxWindows[windowName].closed)
		{
			KancolleFoxWindows[windowName].focus();
			return;
		}
		else
		{
			KancolleFoxWindows[windowName] = window.open(
				"chrome://kancollefox/content/view/" + windowName + ext,
				windowName,
				"chrome,scrollbars=yes,resizable=yes," + rectParam
			);
			KancolleFoxWindows[windowName].focus();
		}
	},

	createRectParam: function(windowName, width, height)
	{
		var rectangle = KancolleFoxConfig.loadWindowRectangle(windowName);

		var rectParam = 
					"centerscreen" +
					",width=" + width +
					",height=" + height;

		if (rectangle != null && rectangle.left != null)
		{
			if (rectangle.left > -32000 && rectangle.top > -32000)
			{
				rectParam =
					"width=" + rectangle.width +
					",height=" + rectangle.height +
					",left=" + rectangle.left +
					",top=" + rectangle.top;
			}
			else
			{
				rectParam =
					"centerscreen" +
					",width=" + rectangle.width +
					",height=" + rectangle.height;
			}
		}

		return rectParam;
	}
};
