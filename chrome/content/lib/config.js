var KancolleFoxConfig =
{
	ConfigVersion: 1,

	checkConfigVersion: function()
	{
		var branch = Components
			.classes["@mozilla.org/preferences-service;1"]
			.getService(Components.interfaces.nsIPrefService)
			.getBranch("extensions.jp.ne.sakura.kumiho.");

		var setupVersion = 0;

		try
		{
			setupVersion = branch.getIntPref("kancollefox.ConfigVersion");
		}
		catch (e)
		{
			setupVersion = 0;
		}

		if (setupVersion < KancolleFoxConfig.ConfigVersion)
		{
			branch.deleteBranch("kancollefox.");
			branch.setIntPref("kancollefox.ConfigVersion", KancolleFoxConfig.ConfigVersion);
		}
	},

	clearAll: function()
	{
		var branch = Components
			.classes["@mozilla.org/preferences-service;1"]
			.getService(Components.interfaces.nsIPrefService)
			.getBranch("extensions.jp.ne.sakura.kumiho.");

		branch.deleteBranch("kancollefox.");
		branch.setIntPref("kancollefox.ConfigVersion", KancolleFoxConfig.ConfigVersion);
	},

	getPrefBranch: function(windowName)
	{
		return Components
			.classes["@mozilla.org/preferences-service;1"]
			.getService(Components.interfaces.nsIPrefService)
			.getBranch("extensions.jp.ne.sakura.kumiho.kancollefox." + windowName + ".");
	},

	loadBoolParameter: function(windowName, functionName)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch(windowName);
			return branch.getBoolPref(functionName);
		}
		catch (x)
		{
			return false;
		}
	},

	saveBoolParameter: function(windowName, functionName, flag)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch(windowName);
			branch.setBoolPref(functionName, flag);
		}
		catch (x) {}
	},

	loadWindowRectangle: function(windowName)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch(windowName);

			var x = branch.getIntPref("window.left");
			var y = branch.getIntPref("window.top");
			var w = branch.getIntPref("window.width");
			var h = branch.getIntPref("window.height");

			return { left: x, top: y, width: w, height: h };
		}
		catch (x)
		{
			return null;
		}
	},

	saveWindowRectangle: function(windowName, left, top, width, height)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch(windowName);

			branch.setIntPref("window.left",   left);
			branch.setIntPref("window.top",    top);
			branch.setIntPref("window.width",  width);
			branch.setIntPref("window.height", height);
		}
		catch (x) {}
	},

	loadHeaderRowInterval: function(windowName)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch(windowName);
			return branch.getIntPref("headerRow.interval");
		}
		catch (x)
		{
			return 12;
		}
	},

	saveHeaderRowInterval: function(windowName, interval)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch(windowName);

			branch.setIntPref("headerRow.interval",   interval);
		}
		catch (x) {}
	},

	loadCaputureScale: function()
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("capture");
			return {
				width: branch.getIntPref("captureScale.width"),
				height: branch.getIntPref("captureScale.height")
			};
		}
		catch (x)
		{
			return {
				width: 800,
				height: 480
			};
		}
	},

	saveCaputureScale: function(scale)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("capture");

			branch.setIntPref("captureScale.width",  scale.width);
			branch.setIntPref("captureScale.height", scale.height);
		}
		catch (x) {}
	},

	loadSignature: function()
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("capture");
			return decodeURIComponent(branch.getCharPref("signature"));
		}
		catch (x)
		{
			return "";
		}
	},

	saveSignature: function(signature)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("capture");
			return branch.setCharPref("signature", encodeURIComponent(signature));
		}
		catch (x)
		{ }
	},

	loadFolderPath: function()
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("capture");
			return decodeURIComponent(branch.getCharPref("folderPath"));
		}
		catch (x)
		{
			return "";
		}
	},

	saveFolderPath: function(path)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("capture");
			return branch.setCharPref("folderPath", encodeURIComponent(path));
		}
		catch (x)
		{ }
	},

	loadFormat: function()
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("capture");
			return decodeURIComponent(branch.getCharPref("format"));
		}
		catch (x)
		{
			return "png";
		}
	},

	saveFormat: function(fileType)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("capture");
			return branch.setCharPref("format", encodeURIComponent(fileType));
		}
		catch (x)
		{ }
	},

	loadLaunchEnabled: function(windowName, functionName)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch(windowName);
			return branch.getBoolPref(functionName);
		}
		catch (x)
		{
			return true;
		}
	},

	saveLaunchEnabled: function(windowName, functionName, flag)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch(windowName);
			branch.setBoolPref(functionName, flag);
		}
		catch (x) {}
	},

	loadSoundType: function()
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("sound");
			return decodeURIComponent(branch.getCharPref("soundType"));
		}
		catch (x)
		{
			return "chime";
		}
	},

	saveSoundType: function(soundType)
	{
		try
		{
			var branch = KancolleFoxConfig.getPrefBranch("sound");
			return branch.setCharPref("soundType", encodeURIComponent(soundType));
		}
		catch (x)
		{ }
	}
};
