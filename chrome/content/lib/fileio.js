var FileIO =
{
	saveTable: function(defaultName, table, header)
	{
		var tsvData = "";

		tsvData += header;

		var trows = table.getElementsByTagName("tr");
		for (var ii = 0; ii < trows.length; ii++)
		{
			var tcols = trows[ii].getElementsByTagName("td");
			if (tcols.length == 0) continue;

			for (var jj = 0; jj < tcols.length; jj++)
			{
				var text = tcols[jj].innerHTML;
				text = text.replace("<br>", "");
				tsvData += text;

				var colspan = tcols[jj].getAttribute("colspan");
				if (! colspan) colspan = 1;
				for (var kk = 0; kk < colspan; kk++)
				{
					tsvData += "\t";
				}

			}
			tsvData += "\n";
		}

		try
		{
			FileIO.saveTextFile(defaultName, tsvData);
		}
		catch(e) {}
	}
	,
	saveTextFile: function(defaultName, text)
	{
		var blob = new Blob( [text], { type: "application/x-msdownload" } );
		var anchor = document.getElementById("save");
		anchor.href = window.URL.createObjectURL(blob);
	
		anchor.download = defaultName;
		anchor.click()
	}
}