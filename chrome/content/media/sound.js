var Sound =
{
	audio :
	{
		chime: 
		{
			expedition: new Audio("chrome://kancollefox/content/media/chime/expedition.mp3"),
			kdock: new Audio("chrome://kancollefox/content/media/chime/kdock.mp3"),
			ndock: new Audio("chrome://kancollefox/content/media/chime/ndock.mp3")
		},
		se: 
		{
			expedition: new Audio("chrome://kancollefox/content/media/se/expedition.mp3"),
			kdock: new Audio("chrome://kancollefox/content/media/se/kdock.mp3"),
			ndock: new Audio("chrome://kancollefox/content/media/se/ndock.mp3")
		},
		child: 
		{
			expedition: new Audio("chrome://kancollefox/content/media/child/expedition.mp3"),
			kdock: new Audio("chrome://kancollefox/content/media/child/kdock.mp3"),
			ndock: new Audio("chrome://kancollefox/content/media/child/ndock.mp3")
		},
		girl: 
		{
			expedition: new Audio("chrome://kancollefox/content/media/girl/expedition.mp3"),
			kdock: new Audio("chrome://kancollefox/content/media/girl/kdock.mp3"),
			ndock: new Audio("chrome://kancollefox/content/media/girl/ndock.mp3")
		},
		woman: 
		{
			expedition: new Audio("chrome://kancollefox/content/media/woman/expedition.mp3"),
			kdock: new Audio("chrome://kancollefox/content/media/woman/kdock.mp3"),
			ndock: new Audio("chrome://kancollefox/content/media/woman/ndock.mp3")
		}
	},

	play: function(soundType, soundName)
	{
		Sound.audio[soundType][soundName].play();
		Sound.audio[soundType][soundName] = new Audio("chrome://kancollefox/content/media/" + soundType + "/" + soundName + ".mp3");
	}

};
