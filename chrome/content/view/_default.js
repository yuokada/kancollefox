var windowSetings =
{

	load: function()
	{
	},

	save: function()
	{
		KancolleFoxConfig.saveWindowRectangle(
			window.name,
			window.screenX,
			window.screenY,
			window.innerWidth,
			window.innerHeight
		);
	}
};



var Util =
{
	timeToRemainingTimeString: function(time)
	{
		try
		{
			var now = new Date();

			if (time > now)
			{
				var sec = (time - now) / 1000;

				if (sec <= 0) return "完了";

				var hh = Math.floor(sec / 3600);
				var mm = Math.floor(sec / 60) % 60;
				var ss = Math.floor(sec % 60);

				if (mm < 10) mm = "0" + mm;
				if (ss < 10) ss = "0" + ss;

				return hh + ":" + mm + ":" + ss;
			}
			else
			{
				return "終了";
			}
		}
		catch (e)
		{
			return "";
		}
	}
	,
	dateTimeToString : function(time)
	{
		try
		{
			var yyyy = time.getYear() + 1900;
			var MM	 = time.getMonth() + 1;
			var dd	 = time.getDate();
			var hh	 = time.getHours();
			var mm	 = time.getMinutes();
			var ss	 = time.getSeconds();

			if (MM < 10) MM = "0" + MM;
			if (dd < 10) dd = "0" + dd;
			if (hh < 10) hh = "0" + hh;
			if (mm < 10) mm = "0" + mm;
			if (ss < 10) ss = "0" + ss;

			return yyyy + "/" + MM + "/" + dd + " " + hh + ":" + mm + ":" + ss;
		}
		catch (e)
		{
			return "";
		}
	}
	,
	dateToString : function(time)
	{
		try
		{
			var yyyy = time.getYear() + 1900;
			var MM	 = time.getMonth() + 1;
			var dd	 = time.getDate();

			if (MM < 10) MM = "0" + MM;
			if (dd < 10) dd = "0" + dd;

			return yyyy + "/" + MM + "/" + dd;
		}
		catch (e)
		{
			return "";
		}
	}
	,
	timeToString : function(time)
	{
		try
		{
			var dd	 = time.getDate();
			var hh	 = time.getHours();
			var mm	 = time.getMinutes();
			var ss	 = time.getSeconds();

			if (dd < 10) dd = "0" + dd;
			if (hh < 10) hh = "0" + hh;
			if (mm < 10) mm = "0" + mm;
			if (ss < 10) ss = "0" + ss;

			return dd + " " + hh + ":" + mm + ":" + ss;
		}
		catch (e)
		{
			return "";
		}
	}
	,
	millSecondsToTimeString: function(msec)
	{
			var hh	 = Math.floor(msec / 1000 / 3600);
			var mm	 = Math.floor(msec / 60000) % 60;
			var ss	 = Math.floor(msec / 1000) % 60;

			if (hh < 10) hh = "0" + hh;
			if (mm < 10) mm = "0" + mm;
			if (ss < 10) ss = "0" + ss;

			return hh + ":" + mm + ":" + ss;
	}
	,
	minutesToTime: function(mins)
	{
		var hh = Math.floor(mins / 60);
		var mm = mins % 60;

		if (mm < 10) mm = "0" + mm;

		return hh + ":" + mm;
	}
	,
	/* **************************************************
	 * 順列のすべてのパターンを生成します。
	 * *************************************************/
	getPermutation: function(src, dst, p)
	{
		if (src.length > 0)
		{
			for (var ii = 0; ii < src.length; ii++)
			{
				dst.push(src.splice(ii, 1));
				dst = Util.getPermutation(src, dst, p);
				src.splice(ii, 0, dst.pop());
			}
		}
		else
		{
			p.push(Util.copyArray(dst));
		}

		return dst;
	}
	,
	/* **************************************************
	 * 組み合わせのすべてのパターンを生成します。
	 * *************************************************/
	getCombination: function(src, dst, c)
	{
		if (src.length > 0)
		{
			for (var ii = 0; ii < dst.length; ii++)
			{
				dst[ii].push(src.shift());
				dst = Util.getCombination(src, dst, c);
				src.unshift(dst[ii].pop());
			}
		}
		else
		{
			var temp = [];
			for (var ii = 0; ii < dst.length; ii++)
			{
				temp.push(Util.copyArray(dst[ii]));
			}
			c.push(temp);
		}

		return dst;
	},

	copyArray: function (array)
	{
		return [].concat(array);
	}
	,
	getInputText: function(id)
	{
		try
		{
			return document.getElementById(id).value;
		}
		catch(e)
		{
			return "";
		}
	}
	,
	getInputInteger: function(id)
	{
		var val = document.getElementById(id).value;

		try
		{
			var intVal = val.replace(/[^0-9]/g, "");
			if (isNaN(intVal)) return "";
			else return intVal;
		} catch (e){
			return "";
		}
	}
	,
	getChecked: function(id)
	{
		return document.getElementById(id).checked;
	}
	,
	getSelectedOptionText: function(id)
	{
		var ele = document.getElementById(id);
		return ele.options[ele.selectedIndex].innerHTML;
	}
	,
	getSelectedOptionValue: function(id)
	{
		var ele = document.getElementById(id);
		return ele.options[ele.selectedIndex].value;
	}
	,
	compare: function(a, b)
	{
		if (a < b) return -1;
		if (a > b) return 1;
		return 0;
	}
};

var Calc =
{
	countSlot: function(ship)
	{
		var ret =
		{
			attached: 0,
			total: ApiMaster.ship[ship.api_ship_id].api_slot_num,
			free: 0
		};

		for (var ii = 0; ii < ret.total; ii++)
		{
			if (ship.api_slot[ii] != -1)
			{
				ret.attached ++;
			}
		}

		ret.free = ret.total - ret.attached;

		return ret;
	}
	,
	calcSeiku: function(ship)
	{
	}
	,
	calcKyouka: function(ship)
	{
		var ret =
		{
			karyoku:
			{
				now:  0,
				max: 0,
				lack: 0
			},
			raisou:
			{
				now:  0,
				max: 0,
				lack: 0
			},
			taiku:
			{
				now:  0,
				max: 0,
				lack: 0
			},
			soukou:
			{
				now:  0,
				max: 0,
				lack: 0
			}
		}

		ret.karyoku.now = ship.api_kyouka[0];
		ret.raisou.now = ship.api_kyouka[1];
		ret.taiku.now = ship.api_kyouka[2];
		ret.soukou.now = ship.api_kyouka[3];

		ret.karyoku.max = ApiMaster.ship[ship.api_ship_id].api_houg[1] - ApiMaster.ship[ship.api_ship_id].api_houg[0];
		ret.raisou.max = ApiMaster.ship[ship.api_ship_id].api_raig[1] - ApiMaster.ship[ship.api_ship_id].api_raig[0];
		ret.taiku.max = ApiMaster.ship[ship.api_ship_id].api_tyku[1] - ApiMaster.ship[ship.api_ship_id].api_tyku[0];
		ret.soukou.max = ApiMaster.ship[ship.api_ship_id].api_souk[1] - ApiMaster.ship[ship.api_ship_id].api_souk[0];

		ret.karyoku.lack = ret.karyoku.max- ret.karyoku.now;
		ret.raisou.lack = ret.raisou.max- ret.raisou.now;
		ret.taiku.lack = ret.taiku.max- ret.taiku.now;
		ret.soukou.lack = ret.soukou.max- ret.soukou.now;

		return ret;
	}
	,
	calcDamage: function(ship)
	{
		if (ship.api_nowhp == 0) return "轟沈";
		if (ship.api_nowhp * 4 <= ship.api_maxhp * 1) return "大破";
		if (ship.api_nowhp * 4 <= ship.api_maxhp * 2) return "中破";
		if (ship.api_nowhp * 4 <= ship.api_maxhp * 3) return "小破";
		if (ship.api_nowhp * 4 < ship.api_maxhp * 4) return "微損";
		return "正常";
	}
	,
	calcTired: function(ship)
	{
		if (ship.api_cond > 49) return "きらきら";
		if (ship.api_cond > 39) return "正常";
		if (ship.api_cond > 29) return "間宮";
		if (ship.api_cond > 19) return "疲労";
		return "強疲労";
	}
	,
	checkAfterShip: function(ship)
	{
		var ret =
		{
			level: "",
			ship: null,
			upgradeOk: 0
		};

		var afterId = ApiMaster.ship[ship.api_ship_id].api_aftershipid;
		if (afterId > 0 && afterId != ship.api_ship_id)
		{
			ret.ship = ApiMaster.ship[afterId];
			ret.level = ApiMaster.ship[ship.api_ship_id].api_afterlv;

			if (ret.level <= ship.api_lv)
			{
				ret.upgradeOk = 1;
			}
		}

		return ret;
	}
};

var RowCursor =
{
	hover: function(elem)
	{
		elem.className += " ActiveRow";
	},

	leave: function(elem)
	{
		elem.className = elem.className.replace(" ActiveRow", "");
	}
};

var Form =
{
	initializeSTypeItem: function(selectElement)
	{
		if (selectElement.options.length > 1) return;

		selectElement.options.length = 0;

		var option = document.createElement("option");
		option.value = "";
		option.text = "";
		selectElement.appendChild(option);

		try
		{
			for (var key in ApiMaster.sType)
			{
				option = document.createElement("option");
				option.value = key;
				if (key == "8")
				{
					option.text = key + ":" + ApiMaster.sType[key].api_name + "(高速)";
				}
				else if (key == "")
				{
					option.text = key + ":" + ApiMaster.sType[key].api_name;
				}
				else
				{
					option.text = key + ":" + ApiMaster.sType[key].api_name;
				}

				selectElement.appendChild(option);
			}
		}
		catch (e) { }
	}
};





Array.prototype.indexOf = function(o) {
	for (var ii in this) {
		if (this[ii].toString() == o.toString()) {
			return ii;
		}
	}
	return -1;
}