Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};

var p = {};

var lastUpdateTime = 0;

var size   = 160;
var radius = size * 0.35;
var center = size / 2;
var dissolution = 4;

var f =
{
	initialize: function()
	{
		if (KancolleFoxConfig.loadBoolParameter(window.name, "configured"))
		{
			document.getElementById("DispChart").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.chart");
			document.getElementById("DispNo").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.no");
			document.getElementById("DispType").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.type");
			document.getElementById("DispLv").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.level");
			document.getElementById("DispCondition").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.cindition");
			document.getElementById("DispFuel").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.fuel");
			document.getElementById("DispSpec").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.spec");
			document.getElementById("DispEquip").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.equip");
		}

		f.initParameters();

		setInterval(function() { f.update(); }, 500);
	}
	,
	finalize: function()
	{
		windowSetings.save();

		KancolleFoxConfig.saveBoolParameter(window.name, "configured", true);
		KancolleFoxConfig.saveBoolParameter(window.name, "display.chart", Util.getChecked("DispChart"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.no", Util.getChecked("DispNo"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.type", Util.getChecked("DispType"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.level", Util.getChecked("DispLv"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.cindition", Util.getChecked("DispCondition"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.fuel", Util.getChecked("DispFuel"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.spec", Util.getChecked("DispSpec"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.equip", Util.getChecked("DispEquip"));
	}
	,
	update: function()
	{
		if (!f.checkUpdated()) return;

		lastUpdateTime = new Date();

		var deckList = selectDeck();
		document.getElementById("contents").innerHTML = generateHtml(deckList);
		drawGraph(deckList);

		document.title = title + " 最終更新日時: " + Util.dateTimeToString(lastUpdateTime);
	}
	,
	initParameters: function()
	{
		p = {
			dispChart : "",
			dispNo : "",
			dispType : "",
			dispLv : "",
			dispCondition : "",
			dispFuel : "",
			dispSpec : "",
			dispEquip : ""
		};
	}
	,
	checkUpdated: function()
	{
		var ret = false;

		var dispChart			= Util.getChecked("DispChart");
		var dispNo	= Util.getChecked("DispNo");
		var dispType	= Util.getChecked("DispType");
		var dispLv	= Util.getChecked("DispLv");
		var dispCondition	= Util.getChecked("DispCondition");
		var dispFuel	= Util.getChecked("DispFuel");
		var dispSpec	= Util.getChecked("DispSpec");
		var dispEquip	= Util.getChecked("DispEquip");

		ret = 
			(
				ApiMember.ship.updateTime
				&& ApiMember.shipInDeck.updateTime
				&& ApiMember.deck.updateTime
			)
			&&
			(
				// 初回更新
				(! lastUpdateTime)

				// APIデータが更新されたとき
				|| (lastUpdateTime < ApiMember.ship.updateTime)
				|| (lastUpdateTime < ApiMember.deck.updateTime)
				|| (lastUpdateTime < ApiMember.slotItem.updateTime)

				// 表示項目の変更
				|| (p.dispChart != dispChart)
				|| (p.dispNo != dispNo)
				|| (p.dispType != dispType)
				|| (p.dispLv != dispLv)
				|| (p.dispCondition != dispCondition)
				|| (p.dispFuel != dispFuel)
				|| (p.dispSpec != dispSpec)
				|| (p.dispEquip != dispEquip)
			);

		if (ret)
		{
			p = {
				dispChart : dispChart,
				dispNo : dispNo,
				dispType : dispType,
				dispLv : dispLv,
				dispCondition : dispCondition,
				dispFuel : dispFuel,
				dispSpec : dispSpec,
				dispEquip : dispEquip
			};
		}

		return ret;
	}
};

function selectDeck()
{
	var ret = [];

	var level = ApiMember.basic.data.api_level + ((ApiMember.basic.data.api_level % 5) ? (5 - ApiMember.basic.data.api_level % 5) : 0);

	for (var ii = 0; ii < ApiMember.basic.data.api_count_deck; ii++)
	{
		var deck = ApiMember.deck.data[ii];

		var data = {};
		data.name = deck.api_name;
		data.ship = [];
		data.shipCount = 0;

		data.taikyu = 0;
		data.soukou = 0;
		data.karyoku = 0;
		data.raisou = 0;
		data.taiku = 0;
		data.taisen = 0;
		data.kaihi = 0;
		data.sakuteki = 0;
		data.sakuteki2 = 0;
		data.luck = 0;
		data.seiku = 0;

		var sakuteki2 = 
		{
			base: 0,
			艦上爆撃機: 0,
			艦上攻撃機: 0,
			艦上偵察機: 0,
			水上偵察機: 0,
			水上爆撃機: 0,
			小型電探: 0,
			大型電探: 0,
			探照灯: 0
		};

		for (var jj = 0; jj < 6; jj++)
		{
			var ship = {};
			ship.no = jj + 1;

			var id = ApiMember.deck.data[ii].api_ship[jj];

			if (id != -1)
			{
				var member = ApiMember.ship.data[id];
				var master = ApiMaster.ship[member.api_ship_id];
				var stype = ApiMaster.sType[master.api_stype];

				ship.id = id;
				ship.member = member;
				ship.master = master;
				ship.stype = stype;

				ship.seiku = calcSlotSeiku(member);
				ship.slotItems = getOnSlotItems(member);
				ship.sakuteki2 = calcSakuteki2(member);

				data.shipCount ++;

				data.taikyu += ship.member.api_maxhp;
				data.soukou += ship.member.api_soukou[0];
				data.karyoku += ship.member.api_karyoku[0];
				data.raisou += ship.member.api_raisou[0];
				data.taiku += ship.member.api_taiku[0];
				data.taisen += ship.member.api_taisen[0];
				data.kaihi += ship.member.api_kaihi[0];
				data.sakuteki += ship.member.api_sakuteki[0];
				data.luck += ship.member.api_lucky[0];
				data.seiku += ship.seiku;

				sakuteki2.base		 += ship.sakuteki2.base;
				sakuteki2.艦上爆撃機 += ship.sakuteki2.艦上爆撃機;
				sakuteki2.艦上攻撃機 += ship.sakuteki2.艦上攻撃機;
				sakuteki2.艦上偵察機 += ship.sakuteki2.艦上偵察機;
				sakuteki2.水上偵察機 += ship.sakuteki2.水上偵察機;
				sakuteki2.水上爆撃機 += ship.sakuteki2.水上爆撃機;
				sakuteki2.小型電探	 += ship.sakuteki2.小型電探;
				sakuteki2.大型電探	 += ship.sakuteki2.大型電探;
				sakuteki2.探照灯	 += ship.sakuteki2.探照灯;

			}
			else
			{
				ship.id = "";
				ship.member = "";
				ship.master = "";
				ship.stype = "";
			}

			data.ship.push(ship);
		}

		data.sakuteki2 =
			Math.sqrt(sakuteki2.base) * 1.6841056
			+ sakuteki2.艦上爆撃機	  * 1.0376255
			+ sakuteki2.艦上攻撃機	  * 1.3677954
			+ sakuteki2.艦上偵察機	  * 1.6592780
			+ sakuteki2.水上偵察機	  * 2.0000000
			+ sakuteki2.水上爆撃機	  * 1.7787282
			+ sakuteki2.小型電探	  * 1.0045358
			+ sakuteki2.大型電探	  * 0.9906638
			+ sakuteki2.探照灯		  * 0.9067950
			+ level 				  * -0.6142467
			;

		data.sakuteki2 = data.sakuteki2.toFixed(2);
		ret.push(data);
	}
	return ret;
}

function generateHtml(deckList)
{
	var headSpan = 1
		+ (p.dispChart ? 1 : 0)
		+ (p.dispNo ? 1 : 0)
		+ (p.dispType ? 1 : 0)
		+ (p.dispLv ? 1 : 0)
		+ (p.dispCondition ? 1 : 0)
		+ (p.dispFuel ? 2 : 0)
		+ (p.dispSpec ? 11 : 0)
		+ (p.dispEquip ? 5 : 0)
		;
	var totlaSpan = 1
		+ (p.dispNo ? 1 : 0)
		+ (p.dispType ? 1 : 0)
		+ (p.dispLv ? 1 : 0)
		+ (p.dispCondition ? 1 : 0)
		+ (p.dispFuel ? 2 : 0)
		;

	var html = "";
	html += "	<table id=\"ShipTable\"><tbody>";

	for (var ii = 0; ii < deckList.length; ii++)
	{
		html += generateDeckTabel(deckList[ii], ii, headSpan, totlaSpan);
	}

	html += "	</tbody></table>";

	return html;
}

function generateDeckTabel(data, deckNo, headSpan, totlaSpan)
{
	var html = "";

	html += generateThr(data, headSpan);

	for (var ii = 0; ii < data.ship.length; ii++)
	{
		html += generateTdr(data.ship[ii], deckNo);
	}

	html += generateTotalRow(data, totlaSpan);

	return html;
}

function generateThr(data, colspan)
{
	var html = "";

	html += "		<tr>";
	html += "			<th class=\"deckHead\" colspan=\"" + colspan + "\">" + data.name + "</th>";
	html += "		</tr>";

	html += "		<tr class=\"h1\">";

	if (p.dispChart)
	{
		html += "			<th>総合力</th>";
	}

	if (p.dispNo)
	{
		html += "			<th>No.</th>";
	}

	html += "			<th>艦名</th>";

	if (p.dispType)
	{
		html += "			<th>艦種</th>";
	}

	if (p.dispLv)
	{
		html += "			<th>Lv</th>";
	}

	if (p.dispCondition)
	{
		html += "			<th>調子</th>";
	}

	if (p.dispFuel)
	{
		html += "			<th>燃料</th>";
		html += "			<th>弾薬</th>";
	}

	if (p.dispSpec)
	{
		html += "			<th>耐久</th>";
		html += "			<th>装甲</th>";
		html += "			<th>回避</th>";
		html += "			<th>火力</th>";
		html += "			<th>雷装</th>";
		html += "			<th>対空</th>";
		html += "			<th>対潜</th>";
		html += "			<th>索敵</th>";
		html += "			<th>索敵2-5(秋)</th>";
		html += "			<th>運</th>";
		html += "			<th>制空</th>";
	
	}

	if (p.dispEquip)
	{
		html += "			<th colspan=\"5\">装備</th>";
	}

	html += "		</tr>";

	return html;
}

function generateTdr(data, deckNo)
{
	var html = "";

	html += "		<tr>";

	if (p.dispChart && data.no == 1)
	{
		html += "			<td rowspan=\"7\" id=\"cv_" + deckNo + "\"></td>";
	}

	if (p.dispNo)
	{
		html += "			<td class=\"number\">" + data.no + "</td><!-- No. -->";
	}

	html += "			<td>" + (data.id ? data.master.api_name : "") + "</td><!-- 艦名 -->";

	if (p.dispType)
	{
		html += "			<td>" + (data.id ? data.stype.api_name : "") + "</td><!-- 艦種 -->";
	}

	if (p.dispLv)
	{
		html += "			<td class=\"number\">" + (data.id ? data.member.api_lv : "") + "</td><!-- Lv -->";
	}

	if (p.dispCondition)
	{
		html += "			<td class=\"number\">" + (data.id ? data.member.api_cond : "") + "</td><!-- 調子 -->";
	}

	if (p.dispFuel)
	{
		html += "			<td class=\"number\">" + (data.id ? data.member.api_fuel : "") + "</td><!-- 燃料 -->";
		html += "			<td class=\"number\">" + (data.id ? data.member.api_bull : "") + "</td><!-- 弾薬 -->";
	}

	if (p.dispSpec)
	{
		html += "			<td class=\"number\">" + (data.id ? data.member.api_maxhp : "") + "</td><!-- 耐久 -->";
		html += "			<td class=\"number\">" + (data.id ? data.member.api_soukou[0] : "") + "</td><!-- 装甲 -->";
		html += "			<td class=\"number\">" + (data.id ? data.member.api_kaihi[0] : "") + "</td><!-- 回避 -->";
		html += "			<td class=\"number\">" + (data.id ? data.member.api_karyoku[0] : "") + "</td><!-- 火力 -->";
		html += "			<td class=\"number\">" + (data.id ? data.member.api_raisou[0] : "") + "</td><!-- 雷装 -->";
		html += "			<td class=\"number\">" + (data.id ? data.member.api_taiku[0] : "") + "</td><!-- 対空 -->";
		html += "			<td class=\"number\">" + (data.id ? data.member.api_taisen[0] : "") + "</td><!-- 対潜 -->";
		html += "			<td class=\"number\">" + (data.id ? data.member.api_sakuteki[0] : "") + "</td><!-- 索敵 -->";
		html += "			<td class=\"number\">" + (data.id ? "" : "") + "</td><!-- 索敵2-5 -->";
		html += "			<td class=\"number\">" + (data.id ? data.member.api_lucky[0] : "") + "</td><!-- 運 -->";
		html += "			<td class=\"number\">" + (data.id ? data.seiku : "") + "</td><!-- 制空 -->";
	}

	if (p.dispEquip)
	{
		for (var ii = 0; ii < 5; ii++)
		{
			html += "			<td>" + (data.id ? "[" + data.slotItems[ii].count + "] " + data.slotItems[ii].name
					+ (data.slotItems[ii].level ? " ★+" + data.slotItems[ii].level : "") : "") + "</td><!-- 装備 -->";
		}
	}

	html += "		</tr>";

	return html;
}

function generateTotalRow(data, colspan)
{
	var html = "";

	html += "		<tr class=\"total\">";

	html += "			<td colspan=\"" + colspan + "\" class=\"right\">艦隊総合(概算値)</td>";

	if (p.dispSpec)
	{
		html += "			<td class=\"number\">" + data.taikyu + "</td><!-- 耐久 -->";
		html += "			<td class=\"number\">" + data.soukou + "</td><!-- 装甲 -->";
		html += "			<td class=\"number\">" + data.kaihi + "</td><!-- 回避 -->";
		html += "			<td class=\"number\">" + data.karyoku + "</td><!-- 火力 -->";
		html += "			<td class=\"number\">" + data.raisou + "</td><!-- 雷装 -->";
		html += "			<td class=\"number\">" + data.taiku + "</td><!-- 対空 -->";
		html += "			<td class=\"number\">" + data.taisen + "</td><!-- 対潜 -->";
		html += "			<td class=\"number\">" + data.sakuteki + "</td><!-- 索敵 -->";
		html += "			<td class=\"number\">" + data.sakuteki2 + "</td><!-- 索敵 -->";
		html += "			<td class=\"number\">" + data.luck + "</td><!-- 運 -->";
		html += "			<td class=\"number\">" + data.seiku + "</td><!-- 制空 -->";
	}

	if (p.dispEquip)
	{
		html += "			<td colspan=\"5\"></td><!-- 装備1 -->";
	}

	html += "		</tr>";

	return html;
}

function calcSakuteki2(ship)
{
	var ret = {
		base: ship.api_sakuteki[0],
		艦上爆撃機: 0,
		艦上攻撃機: 0,
		艦上偵察機: 0,
		水上偵察機: 0,
		水上爆撃機: 0,
		小型電探: 0,
		大型電探: 0,
		探照灯: 0
	};

	for (var ii = 0; ii < ship.api_slot.length; ii++)
	{
		var item = ApiMember.slotItem.data[ship.api_slot[ii]];
		if (item)
		{
			var master = ApiMaster.slotItem[item.api_slotitem_id];

			if (master.api_saku)
			{
				switch (master.api_type[2])
				{
					case 7:
					{
						ret.艦上爆撃機 += master.api_saku;
						break;
					}
					case 8:
					{
						ret.艦上攻撃機 += master.api_saku;
						break;
					}
					case 9:
					{
						ret.艦上偵察機 += master.api_saku;
						break;
					}
					case 10:
					{
						ret.水上偵察機 += master.api_saku;
						break;
					}
					case 11:
					{
						ret.水上爆撃機 += master.api_saku;
						break;
					}
					case 12:
					{
						ret.小型電探 += master.api_saku;
						break;
					}
					case 13:
					{
						ret.大型電探 += master.api_saku;
						break;
					}
					case 29:
					{
						ret.探照灯 += master.api_saku;
						break;
					}
				}

				ret.base -= master.api_saku;
			}
		}
	}

	return ret;
}

function calcSlotSeiku(ship)
{
	var ret = 0;
	for (var ii = 0; ii < ship.api_slot.length; ii++)
	{
		var item = ApiMember.slotItem.data[ship.api_slot[ii]];
		if (item)
		{
			var master = ApiMaster.slotItem[item.api_slotitem_id];
			ret += Math.floor(master.api_tyku * Math.sqrt(ship.api_onslot[ii]));
		}
	}
	return ret;
}

function getOnSlotItems(ship)
{
	var ret = [];

	for (var ii = 0; ii < ship.api_slot.length; ii++)
	{
		var item = ApiMember.slotItem.data[ship.api_slot[ii]];
		if (item)
		{
			ret.push({
				name: ApiMaster.slotItem[item.api_slotitem_id].api_name,
				count: ship.api_onslot[ii],
				level: item.api_level
			});
		}
		else if (ship.api_slotnum > ii)
		{
			ret.push({
				name: "(空き)",
				count: ship.api_onslot[ii]
			});
		}
		else
		{
			ret.push({
				name: "-",
				count: "-"
			});
		}
	}

	return ret;
}

function calcDeckSpeck(total, count)
{
	if (! count) return "";

	var avg = (total / count);
	return Math.floor(avg *(1 + Math.sqrt(count - 1)));
}

function drawGraph(deckList)
{
	for (var ii = 0; ii < deckList.length; ii++)
	{
		var container = document.getElementById("cv_" + ii);
		if (container)
		{
			container.appendChild(generateCanvas(deckList[ii]));
		}
	}
}

function generateCanvas(deck)
{
	var canvas = document.createElement("canvas");

	canvas.width = size;
	canvas.height = size;

	var context = canvas.getContext("2d");
	if (!context) return;

	context.fillStyle = "#000000";
	context.fillRect(0, 0, canvas.width, canvas.height);

	var title = ["火", "雷", "空", "潜", "避", "耐", "装", "索", "運"];
	var speck = [
		calcDeckSpeck(deck.taikyu, deck.shipCount),
		calcDeckSpeck(deck.soukou, deck.shipCount),
		calcDeckSpeck(deck.karyoku, deck.shipCount),
		calcDeckSpeck(deck.raisou, deck.shipCount),
		calcDeckSpeck(deck.taiku, deck.shipCount),
		calcDeckSpeck(deck.taisen, deck.shipCount),
		calcDeckSpeck(deck.kaihi, deck),
		calcDeckSpeck(deck.sakuteki, deck.shipCount),
		calcDeckSpeck(deck.luck, deck.shipCount)
	];


	drawFrame(context, title);
	drawChart(context, speck);

	return canvas;
}

function drawFrame(context, title)
{
	var count = title.length;
	var angle = Math.PI * 2 / count;

	for (var ii = 0; ii < count; ii++)
	{
		var avexes = {
			x1: center + (radius + 3) * Math.cos(-Math.PI / 2 + angle * ii),
			y1: center + (radius + 3) * Math.sin(-Math.PI / 2 + angle * ii),
			x2: center,
			y2: center
		};

		drawLine(context, avexes, "#222222");
	}

	for (var rr = 0; rr < dissolution; rr++)
	{
		var avexes = [];
		var radius2 = radius * (dissolution - rr) / dissolution;

		for (var ii = 0; ii < count; ii++)
		{
			avexes.push({
				x: center + radius2 * Math.cos(-Math.PI / 2 + angle * ii),
				y: center + radius2 * Math.sin(-Math.PI / 2 + angle * ii)
			});
		}

		if (rr == 0)
			drawPolygon(context, avexes, "#ffffff");
		else
			drawPolygon(context, avexes, "#999999");
	}

	for (var ii = 0; ii < count; ii++)
	{
		var avexes = {
			x: center + (radius + 8) * Math.cos(-Math.PI / 2 + angle * ii) - 5,
			y: center + (radius + 8) * Math.sin(-Math.PI / 2 + angle * ii) + 5
		};
		drawText(context, title[ii], avexes, "#000000", "#ffffff");
	}
}

function drawChart(context, values)
{
	var count = values.length;
	var angle = Math.PI * 2 / count;

	var avexes = [];
	for (var ii = 0; ii < count; ii++)
	{
		avexes.push({
			x: center + radius * Math.cos(-Math.PI / 2 + angle * ii) * values[ii] / 800,
			y: center + radius * Math.sin(-Math.PI / 2 + angle * ii) * values[ii] / 800
		});
	}

	fillPolygon(context, avexes, "rgba(255, 64, 64, 0.25)");
	drawPolygon(context, avexes, "rgba(255, 64, 64, 0.75)");
}



function drawLine(context, avexes, style)
{
	context.beginPath();
	context.strokeStyle = style;
	context.moveTo(avexes.x1, avexes.y1);
	context.lineTo(avexes.x2, avexes.y2);
	context.stroke();
}

function drawPolygon(context, avexes, style)
{
	context.beginPath();
	context.strokeStyle = style;
	context.moveTo(avexes[avexes.length - 1].x, avexes[avexes.length - 1].y);
	for(var ii = 0; ii < avexes.length; ii++)
	{
		context.lineTo(avexes[ii].x, avexes[ii].y);
	}
	context.stroke();
}

function fillPolygon(context, avexes, style)
{
	context.beginPath();
	context.fillStyle = style;
	context.moveTo(avexes[avexes.length - 1].x, avexes[avexes.length - 1].y);
	for(var ii = 0; ii < avexes.length; ii++)
	{
		context.lineTo(avexes[ii].x, avexes[ii].y);
	}
	context.fill();
}

function drawText(context, text, position, strokeStyle, fillStyle)
{
	context.font = "10px 'ＭＳ Ｐゴシック'";
	context.strokeStyle = strokeStyle;
	context.strokeText(text, position.x, position.y);
	context.fillStyle = fillStyle;
	context.fillText(text, position.x, position.y);
}
