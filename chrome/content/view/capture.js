//Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};


var p =
{
	width: 800,
	height: 400,
};

var f = {
	initialize: function()
	{
		// 保存先フォルダのパスとファイルフォーマットを設定します。
		document.getElementById("pathValue").value = "";
		document.getElementById("format").value = "png";

		document.getElementById("signature").value = "";
		document.getElementById("isWriteDate").checked = false;

		if (KancolleFoxConfig.loadBoolParameter(window.name, "configured"))
		{
			document.getElementById("pathValue").value = KancolleFoxConfig.loadFolderPath();
			document.getElementById("format").value = KancolleFoxConfig.loadFormat();
			document.getElementById("signature").value = KancolleFoxConfig.loadSignature();
			document.getElementById("isWriteDate").checked = KancolleFoxConfig.loadBoolParameter(window.name, "isWriteDate");
		}

		// 撮影サイズのスライダーの最大値と現在値のデフォルトを設定します。
		document.getElementById("width").max = 800;
		document.getElementById("height").max = 480;
		document.getElementById("width").value = 800;
		document.getElementById("height").value = 480;

		// Flashのサイズを取得してスライダーの最大値への適用を試みます。
		try
		{
			var kc = f.getKancolle();
			if (kc)
			{
				var flash = kc
					.linkedBrowser._contentWindow.wrappedJSObject
					.window.document.getElementById("game_frame")
					.contentWindow.document.getElementById("flashWrap");

				if (flash.clientWidth != 0)
					document.getElementById("width").max = flash.clientWidth;

				if (flash.clientHeight != 0)
					document.getElementById("height").max = flash.clientHeight;
			}
		}
		catch (e) {}

		// 撮影サイズのスライダーの現在値をコンフィグから取得して設定します。
		if (KancolleFoxConfig.loadBoolParameter(window.name, "configured"))
		{
			// コンフィグに設定がある場合。
			var scale = KancolleFoxConfig.loadCaputureScale();
			document.getElementById("width").value = scale.width;
			document.getElementById("height").value = scale.height;
		}
		else
		{
			// コンフィグに設定が無い場合は最大値をセットします。
			document.getElementById("width").value = document.getElementById("width").max;
			document.getElementById("height").value = document.getElementById("height").max;
		}

		// 撮影用のキャンバスのサイズをFlashのサイズと合わせます。
		p.width = document.getElementById("width").max;
		p.height = document.getElementById("height").max;

		document.getElementById("canvas").width = p.width;
		document.getElementById("canvas").height = p.height;

		// 撮影サイズ更新時のイベントを発火します。
		scaleChanged();

		// イベント処理を割り当てます。
		document.getElementById("width").oninput = scaleChanged;
		document.getElementById("height").oninput = scaleChanged;

		document.getElementById("path").onclick = selectPath;
//		document.getElementById("capture").onclick = capture;
//		document.getElementById("save").onclick = save;
		document.getElementById("captureAndSave").onclick = takeSnapshot;
		document.getElementById("compact").onclick = compactWindow;
		document.getElementById("fit").onclick = fitWindow;
	}
	,
	finalize: function()
	{
		windowSetings.save();
		KancolleFoxConfig.saveBoolParameter(window.name, "configured", true);
		KancolleFoxConfig.saveCaputureScale({
			width:	document.getElementById("width").value,
			height: document.getElementById("height").value
		});

		var path = document.getElementById("pathValue").value;
		if (path)
			KancolleFoxConfig.saveFolderPath(document.getElementById("pathValue").value);

		var format = document.getElementById("format").value;
		if (format)
			KancolleFoxConfig.saveFormat(document.getElementById("format").value);

		KancolleFoxConfig.saveSignature(document.getElementById("signature").value);
		KancolleFoxConfig.saveBoolParameter(window.name, "isWriteDate", Util.getChecked("isWriteDate"));
	}
	,
	getKancolle: function()
	{
		try
		{
			var url = "www.dmm.com/netgame/social/-/gadgets/=/app_id=854854";

			var winMed = Components.classes["@mozilla.org/appshell/window-mediator;1"]
				.getService(Components.interfaces.nsIWindowMediator);
			var browsers = winMed.getEnumerator("navigator:browser");

			while (browsers.hasMoreElements())
			{
				var browserInstance = browsers.getNext().gBrowser;

				for (var ii = 0; ii < browserInstance.tabContainer.childNodes.length; ii++)
				{
					var browser = browserInstance.getBrowserAtIndex(ii);
					if (browser.currentURI.spec.indexOf(url) != -1)
					{
						return browserInstance.tabContainer.childNodes[ii];
					}
				}
			}
		}
		catch (e) {}

		return null;
	}
};

function calcScale(width, height, maxWidth, maxHeight)
{
	var scale = 1.0;
	if (width / maxWidth > height / maxHeight)
	{
		scale = height / maxHeight;
	}
	else
	{
		scale = width / maxWidth;
	}

	return scale;
}

function scaleChanged()
{
	var width = document.getElementById("width").value;
	var height = document.getElementById("height").value;
	var maxWidth = document.getElementById("width").max;
	var maxHeight = document.getElementById("height").max;

	scale = calcScale(width, height, maxWidth, maxHeight);

	p.width = Math.max(Math.floor(maxWidth * scale), 1);
	p.height = Math.max(Math.floor(maxHeight * scale), 1);
	document.getElementById("widthValue").innerHTML = p.width + "px/" + maxWidth + "px";
	document.getElementById("heighthValue").innerHTML = p.height + "px/" + maxHeight + "px";
}

function capture()
{
	var kc = f.getKancolle();
	if (! kc) return false;

	var kcWin = kc
		.linkedBrowser._contentWindow.wrappedJSObject;

	var w = kcWin.document.getElementById("w"); // ←追加

	var gameFrame = kcWin
		.window.document.getElementById("game_frame");
	var flash = gameFrame
		.contentWindow.document.getElementById("flashWrap");

	var ww = flash.clientWidth;
	var hh = flash.clientHeight;
	var xx = gameFrame.offsetLeft + flash.offsetLeft;
//	var yy = gameFrame.offsetTop + flash.offsetTop; // ←↓変更
	var yy = w.offsetTop + gameFrame.offsetTop + flash.offsetTop;
	var scale = calcScale(p.width, p.height, ww, hh);
	var canvas = document.getElementById("canvas");
	canvas.width = ww * scale;
	canvas.height = hh * scale;

	var ctx = canvas.getContext("2d");
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	ctx.scale(scale, scale);
	ctx.drawWindow(kcWin, xx, yy, ww, hh, "rgb(255,255,255)");
	ctx.save();

	drawText(ctx, ww, hh);

	document.title = title + " 撮影成功: ファイルに保存するには『保存』ボタンを押してください。";

	return true;
}


function drawText(ctx, width, height)
{
	var text = "";
	text += document.getElementById("signature").value;
	if (text != "")
	{
		text = "©" + text;
	}
	if (document.getElementById("isWriteDate").checked)
	{
		text += " " + Util.dateTimeToString(new Date());
	}
	if (text == "") return;

	var xx = width - 5;
	var yy = height - 5;

	ctx.textAlign = "end";
	ctx.textBaseline = "bottom";
	ctx.font = "22px sans-serif";
	ctx.lineWidth = 3.2;
	ctx.strokeStyle = "#442200";
	ctx.fillStyle = "#ffffff";

	ctx.strokeText(text, xx, yy);
	ctx.fillText(text, xx, yy);
	ctx.save();

}


function save()
{
	var path =document.getElementById("pathValue").value;
	var format = Util.getSelectedOptionText("format");

	var mimeType = "image/" + format;
	var url = canvas.toDataURL(mimeType);
	var io = Components.classes['@mozilla.org/network/io-service;1']
		.getService(Components.interfaces.nsIIOService);
	url = io.newURI(url, null, null);

	if( !url ) return null;

	var file = Components.classes["@mozilla.org/file/local;1"]
		.createInstance(Components.interfaces.nsILocalFile);

	var pathExists = false;
	if (path)
	{
		file.initWithPath(path);
		pathExists = file.exists();
	}
	if (!path || !pathExists)
	{
		var file = selectPath();
		if (file)
		{
			try
			{
				path = file.path.toString();
			}
			catch (e)
			{
				alert("保存先のフォルダが見つかりません。");
			}
		}
	}

	if (!path)
	{
		alert("保存先が選択されていません。");
		return;
	}

	var fileName = generateFileName();
	var filePath = path + "\\" + fileName + "." + format;

	file.initWithPath(filePath);
	
	var wbp = Components.classes['@mozilla.org/embedding/browser/nsWebBrowserPersist;1']
		.createInstance(Components.interfaces.nsIWebBrowserPersist);
	try {
		wbp.saveURI(url, null, null, null, null, null, file, null);
	} catch (e) {
		wbp.saveURI(url, null, null, null, null, file, null);
	}

	document.title = title + " 保存成功: " + fileName + "." + format;
}

function takeSnapshot()
{
	if (capture()) save();
}

function selectPath()
{
	var ret = null;

	var path = document.getElementById("pathValue").value;

	var nsIFilePicker = Components.interfaces.nsIFilePicker;
	var filePicker = Components.classes['@mozilla.org/filepicker;1']
		.createInstance(Components.interfaces.nsIFilePicker);

	filePicker.init(window, "艦これスクリーンショットの保存フォルダ", filePicker.modeGetFolder);
	filePicker.appendFilters(filePicker.filterImages);

	var file = Components.classes["@mozilla.org/file/local;1"]
		.createInstance(Components.interfaces.nsILocalFile);

	var pathExists = false;
	if (path)
	{
		file.initWithPath(path);
		pathExists = file.exists();
	}

	filePicker.displayDirectory = file;
	if (filePicker.show() == filePicker.returnCancel || !filePicker.file ) return null;

	document.getElementById("pathValue").value = filePicker.file.path.toString();

	return filePicker.file;
}

function generateFileName()
{
	var now = new Date();

	var yy = now.getFullYear();
	var mm = now.getMonth() + 1;
	var dd = now.getDate();
	var hh = now.getHours();
	var mn = now.getMinutes();
	var ss = now.getSeconds();

	if (mm < 10) mm = "0" + mm;
	if (dd < 10) dd = "0" + dd;
	if (hh < 10) hh = "0" + hh;
	if (mn < 10) mn = "0" + mn;
	if (ss < 10) ss = "0" + ss;

	return "kancolle-" + yy + mm + dd + "-"  + hh + mn + ss;
}

function compactWindow()
{
	window.innerWidth = 640;
	window.innerHeight = 69;
}


function fitWindow()
{
	window.innerWidth = Math.max(document.getElementById("canvas").width, 640);
	window.innerHeight = document.getElementById("canvas").height + 69;
}
