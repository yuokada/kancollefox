Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};

var p = {};

var limit = 10;
var tooMuch = false;
var damegadShips = [];
var ndockCount = 2;
var ndockClock = [];

var lastUpdateTime = 0;

var f =
{
	initialize: function()
	{
		f.initParameters();

		document.getElementById("update").onclick = f.update;
	}
	,
	finalize: function()
	{
		windowSetings.save();
	}
	,
	update: function()
	{
		if (!f.checkUpdated()) return;

		ndockCount = ApiMember.basic.data.api_count_ndock;
		tooMuch = false;
		lastUpdateTime = new Date();

		document.getElementById("contents").innerHTML = generateHtml();

		document.title = title + " 最終更新日時: " + Util.dateTimeToString(lastUpdateTime);
	}
	,
	initParameters: function()
	{
		p = {
			order : "",
			limit: limit
		};
	}
	,
	checkUpdated: function()
	{
		var ret = false;

		var order = Util.getSelectedOptionValue("order");

		ret = 
			(
				ApiMember.ship.updateTime
			);

		if (ret)
		{
			p = {
				order : order
			};
		}

		return ret;
	}
};

function generateHtml()
{
	var ndockShips = getNDockShips();
	ndockClock = new Array(ndockCount);

	for (var ii = 0; ii < ndockCount; ii++)
	{
		sortInDock(ndockShips[ii]);
	}

	var html = "";
	html += "	<table id=\"ShipTable\"><tbody>";

	html += generateThr(ndockShips);
	html += generateInDockTdr(ndockClock);

	while(true)
	{
		var exists = false;

		var row = [];
		for (var ii = 0; ii < ndockShips.length; ii++)
		{
			var ship = ndockShips[ii].shift();
			if (ship)
			{
				exists = true;
				row.push(ship);
			}
			else
			{
				row.push(null);
			}
		}

		if (exists)
		{
			html += generateTdr(row, ndockClock);
		}
		else
		{
			break;
		}
	}

	html += "	</tbody></table>";

	return html;
}

function generateThr()
{
	var html = "";

	html += "		<tr>";
	for (var ii = 0; ii < ndockCount; ii++)
	{
		html += "			<th colspan=\"3\" class=\"lb rb\">第" + (ii + 1) + "ドック</th>";
	}

	html += "		</tr>";
	html += "		<tr>";

	for (var ii = 0; ii < ndockCount; ii++)
	{
		html += "			<th rowspan=\"2\" class=\"lb\">時刻<br>自/至</th>";
		html += "			<th>ID</th>";
		html += "			<th class=\"rb\">艦名</th>";
	}

	html += "		</tr>";
	html += "		<tr>";

	for (var ii = 0; ii < ndockCount; ii++)
	{
		html += "			<th>LV</th>";
		html += "			<th class=\"rb\">入渠時間</th>";
	}
	html += "		</tr>";

	return html;
}

function generateInDockTdr(ndockClock)
{
	var now = new Date().getTime();

	var html = "";
	html += "		<tr class=\"indock\">";

	for (var ii = 0; ii < ndockCount; ii++)
	{

		var ship = ApiMember.ship.data[ApiMember.ndock.data[ii].api_ship_id];

		var timeFrom = ship ? "-" : "";
		var id = ship ? ship.api_id : "";
		var name = ship ? ApiMaster.ship[ship.api_ship_id].api_name : "";

		html += "			<td class=\"number t schedule lb\">" + timeFrom + "</td>";
		html += "			<td class=\"number t\">" + id + "</td>";
		html += "			<td class=\"t rb\">" + name + "</td>";
	}

	html += "		</tr>";
	html += "		<tr class=\"indock\">";

	for (var ii = 0; ii < ndockCount; ii++)
	{
		var ship = ApiMember.ship.data[ApiMember.ndock.data[ii].api_ship_id];

		var timeTo = "";
		var lv = ship ? ship.api_lv : "";
		var ndockTime = ship ? Util.millSecondsToTimeString(ship.api_ndock_time) : "";

		if (ship)
		{
			if (ApiMember.ndock.data[ii].api_complete_time > now)
			{
				ndockClock[ii] = ApiMember.ndock.data[ii].api_complete_time;
				timeTo = Util.timeToString(new Date(ndockClock[ii]));
			}
			else
			{
				ndockClock[ii] = now;
				timeTo = "終了";
			}
		}
		else
		{
			ndockClock[ii] = now;
		}

		html += "			<td class=\"number b schedule lb\">" + timeTo + "</td>";
		html += "			<td class=\"number b\">" + lv + "</td>";
		html += "			<td class=\"number b rb\">" + ndockTime + "</td>";
	}

	html += "		</tr>";

	return html;
}

function generateTdr(rowShips, ndockClock)
{
	var html = "";

	html += "		<tr>";

	for (var ii = 0; ii < rowShips.length; ii++)
	{
		var ship = rowShips[ii];

		var timeFrom = ship ? Util.timeToString(new Date(ndockClock[ii])) : "";
		var id = ship ? ship.api_id : "";
		var name = ship ? ApiMaster.ship[rowShips[ii].api_ship_id].api_name : "";

		html += "			<td class=\"number t schedule lb\">" + timeFrom + "</td>";
		html += "			<td class=\"number t\">" + id + "</td>";
		html += "			<td class=\"t rb\">" + name+ "</td>";
	}

	html += "		</tr>";
	html += "		<tr>";

	for (var ii = 0; ii < rowShips.length; ii++)
	{
		var ship = rowShips[ii];

		if (ship)
		{
			ndockClock[ii] = ndockClock[ii] + ship.api_ndock_time;
		}

		var timeTo = ship ? Util.timeToString(new Date(ndockClock[ii])) : "";
		var lv = ship ? ship.api_lv : "";
		var ndockTime = ship ? Util.millSecondsToTimeString(ship.api_ndock_time) : "";

		html += "			<td class=\"number b schedule lb\">" + timeTo + "</td>";
		html += "			<td class=\"number b\">" + lv + "</td>";
		html += "			<td class=\"number b rb\">" + ndockTime + "</td>";
	}

	html += "		</tr>";

	return html;
}

function sortInDock(ndockShips)
{
	switch (p.order)
	{
		case "入渠時間が長い":
			ndockShips.sort(
				function (a, b)
				{
					return b.api_ndock_time - a.api_ndock_time;
				}
			);
			break;
		case "入渠時間が短い":
			ndockShips.sort(
				function (a, b)
				{
					return a.api_ndock_time - b.api_ndock_time;
				}
			);
			break;
		case "損傷が大きい":
			ndockShips.sort(
				function (a, b)
				{
					return (b.api_maxhp - b.api_nowhp) - (a.api_maxhp - a.api_nowhp);
				}
			);
			break;
		case "損傷が小さい":
			ndockShips.sort(
				function (a, b)
				{
					return (a.api_maxhp - a.api_nowhp) - (b.api_maxhp - b.api_nowhp);
				}
			);
			break;
		case "レベルが高い":
			ndockShips.sort(
				function (a, b)
				{
					return b.api_lv - a.api_lv;
				}
			);
			break;
		case "レベルが低い":
			ndockShips.sort(
				function (a, b)
				{
					return a.api_lv - b.api_lv;
				}
			);
			break;
	}
}

function getNDockShips()
{
	var ships = selectDamagedShips();
	var ndock = [];
	for (var ii = 0; ii < ndockCount; ii++) ndock.push([]);

	var c = [];
	try
	{
		Util.getCombination(ships, ndock, c);
	}
	catch(e)
	{
		tooMuch = true;
	}

	return calcNDockTime(c);
}

function calcNDockTime(c)
{
	var ret = [];

	var nowTime = new Date().getTime();
	var minLastTime = Number.MAX_VALUE;
	var minTopTime = Number.MIN_VALUE;

	try
	{
		for (var ii = 0; ii < ndockCount; ii++)
			ret.push([]);

		for (var ii = 0; ii < c.length; ii++)
		{
			var ndockTime = [];
			for (var jj = 0; jj < ndockCount; jj++)
			{
				if (ApiMember.ndock.data[jj].api_complete_time && ApiMember.ndock.data[jj].api_complete_time > nowTime)
					ndockTime.push(ApiMember.ndock.data[jj].api_complete_time);
				else
					ndockTime.push(nowTime);
			}

			var docks = c[ii];
			for (var jj = 0; jj < docks.length; jj++)
			{
				for (var kk = 0; kk < docks[jj].length; kk++)
				{
					ndockTime[jj] += docks[jj][kk].api_ndock_time;
					if (ndockTime[jj] > minLastTime) break;
				}

				if (ndockTime[jj] > minLastTime) break;
			}

			if (getMaxValue(ndockTime) > minLastTime) continue;
			if (getMaxValue(ndockTime) == minLastTime && getMinValue(ndockTime) <= minTopTime) continue;

			var temp = [];
			for (var jj = 0; jj < docks.length; jj++)
				temp.push(Util.copyArray(docks[jj]));
			ret = Util.copyArray(temp);
			minLastTime = getMaxValue(ndockTime);
			minTopTime = getMinValue(ndockTime);
		}
	}
	catch(e)
	{
		tooMuch = true;
	}

	return ret;
}

function selectDamagedShips()
{
	var ret = [];
	var dameged = [];

	var inDock = [];
	for (var ii = 0; ii < ndockCount; ii++)
		inDock.push(ApiMember.ndock.data[ii].api_ship_id);

	for (var key in ApiMember.ship.data)
	{
		var ship = ApiMember.ship.data[key];
		if (inDock.indexOf(ship.api_id) == -1 && ship.api_ndock_time)
			ret.push(ship);
	}

	ret.sort(
		function (a, b)
		{
			return b.api_ndock_time - a.api_ndock_time;
		}
	);

	while (p.limit < ret.length)
	{
		ret.pop();
		tooMuch = true;
	}

	return ret;
}


function getMaxValue(array)
{
	return Math.max.apply(null, array)
}

function getMinValue(array)
{
	return Math.min.apply(null, array)
}