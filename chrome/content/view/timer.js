Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};

var p = {};

var f =
{
	status:
	{
		ndock:
		[
			"",
			"",
			"",
			""
		]
		,
		kdock:
		[
			"",
			"",
			"",
			""
		]
		,
		expedition:
		[
			"",
			"",
			"",
			""
		]
	}
	,
	initialize: function()
	{
		if (KancolleFoxConfig.loadBoolParameter(window.name, "configured"))
		{
			document.getElementById("DispName").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.name");
			document.getElementById("AlarmNDock").checked = KancolleFoxConfig.loadBoolParameter(window.name, "alarm.nDock");
			document.getElementById("AlarmKDock").checked = KancolleFoxConfig.loadBoolParameter(window.name, "alarm.kDock");
			document.getElementById("AlarmExpedition").checked = KancolleFoxConfig.loadBoolParameter(window.name, "alarm.expedition");
		}
		setInterval(function() { f.update(); }, 100);
	}
	,
	finalize: function()
	{
		windowSetings.save();
		KancolleFoxConfig.saveBoolParameter(window.name, "configured", true);
		KancolleFoxConfig.saveBoolParameter(window.name, "display.name", Util.getChecked("DispName"));
		KancolleFoxConfig.saveBoolParameter(window.name, "alarm.nDock", Util.getChecked("AlarmNDock"));
		KancolleFoxConfig.saveBoolParameter(window.name, "alarm.kDock", Util.getChecked("AlarmKDock"));
		KancolleFoxConfig.saveBoolParameter(window.name, "alarm.expedition", Util.getChecked("AlarmExpedition"));
	}
	,
	update: function()
	{
		var html = "";

		try { html += generateNDockHtml(); } catch(e) {}
		try { html += generateKDockHtml(); } catch(e) {}
		try { html += generateExpeditionHtml(); } catch(e) {}

		document.getElementById("contents").innerHTML = html;
	}
}

function generateNDockHtml(index)
{
	var html = "";

	html += "	<div class=\"timerHead\">入渠ドック</div>";
	html += "	<table id=\"NDock\" class=\"table\"><tbody>";
	html += "		<tr>";
	html += "			<th>No.</th>";
	html += "			<th>ID</th>";
	html += "			<th>艦名</th>";
	html += "			<th>終了時刻</th>";
	html += "			<th>残り時間</th>";
	html += "		</tr>";

	for (var ii = 0; ii < ApiMember.basic.data.api_count_ndock; ii++)
	{
		html += generateNDockRow(ii);
	}

	html += "	</tbody></table>";

	return html;
}

function generateNDockRow(index)
{
	var dock = ApiMember.ndock.data[index];

	if (!dock) return "";

	var dockId = dock.api_id ?
		dock.api_id :
		"";

	var id = dock.api_ship_id ?
		dock.api_ship_id :
		"";

	var name = dock.api_ship_id ?
		ApiMaster.ship[ApiMember.ship.data[dock.api_ship_id].api_ship_id].api_name :
		 "";

	var compTime = dock.api_complete_time ?
		Util.timeToString(new Date(dock.api_complete_time)) :
		"";

	var lackTime = dock.api_ship_id ?
		Util.timeToRemainingTimeString(dock.api_complete_time) :
		"";

	var rowClass = "";
	if (lackTime == "終了") rowClass = "終了";

	if (lackTime == "")
	{
		f.status.ndock[index] = 0;
	}
	else if (lackTime == "終了")
	{
		if (f.status.ndock[index] != 0)
		{
			if (Util.getChecked("AlarmNDock"))
				Sound.play(KancolleFoxConfig.loadSoundType(), "ndock");
		}
		f.status.ndock[index] = 0;
	}
	else
	{
		f.status.ndock[index] = 1;
	}

	var html = "";
	html += "		<tr>";
	html += "			<td class=\"number\">" + dockId + "</td>";
	html += "			<td class=\"number\">" + id + "</td>";
	html += "			<td>" + name + "</td>";
	html += "			<td class=\"datetime\">" + compTime + "</td>";
	html += "			<td class=\"datetime " + rowClass + "\">" + lackTime + "</td>";
	html += "		</tr>";

	return html;
}

function generateKDockHtml()
{
	var dispName = Util.getChecked("DispName");

	var html = "";
	html += "	<div class=\"timerHead\">工廠ドック</div>";
	html += "	<table id=\"KDock\" class=\"table\"><tbody>";
	html += "		<tr>";
	html += "			<th>No.</th>";
	if (dispName)
	{
		html += "			<th>ID</th>";
		html += "			<th>艦名</th>";
	}
	html += "			<th>終了時刻</th>";
	html += "			<th>残り時間</th>";
	html += "		</tr>";

	for (var ii = 0; ii < ApiMember.basic.data.api_count_kdock; ii++)
	{
		html += generateKDockRow(ii, dispName);
	}

	html += "	</tbody></table>";

	return html;
}

function generateKDockRow(index, dispName)
{

	var dock = ApiMember.kdock.data[index];

	if (!dock) return "";

	var dockId = dock.api_id ?
		dock.api_id :
		"";

	var id = dock.api_created_ship_id && dispName  ?
		dock.api_created_ship_id :
		"";

	var name = dock.api_created_ship_id && dispName ?
		ApiMaster.ship[dock.api_created_ship_id].api_name :
		"";

	var compTime = dock.api_complete_time ? 
		Util.timeToString(new Date(dock.api_complete_time)) :
		"";

	var lackTime = dock.api_created_ship_id ?
		Util.timeToRemainingTimeString(dock.api_complete_time) :
		"";

	var rowClass = "";
	if (lackTime == "終了") rowClass = "終了";

	if (lackTime == "")
	{
		f.status.kdock[index] = 0;
	}
	else if (lackTime == "終了")
	{
		if (f.status.kdock[index] != 0)
		{
			if (Util.getChecked("AlarmKDock"))
				Sound.play(KancolleFoxConfig.loadSoundType(), "kdock");
		}
		f.status.kdock[index] = 0;
	}
	else
	{
		f.status.kdock[index] = 1;
	}

	var html = "";

	html += "		<tr>";
	html += "			<td class=\"number\">" + dockId + "</td>";
	if (dispName)
	{
		html += "			<td class=\"number\">" + id + "</td>";
		html += "			<td>" + name + "</td>";
	}
	html += "			<td class=\"datetime\">" + compTime + "</td>";
	html += "			<td class=\"datetime " + rowClass + "\">" + lackTime + "</td>";
	html += "		</tr>";

	return html;
}

function generateExpeditionHtml()
{
	var html = "";

	html += "	<div class=\"timerHead\">遠征</div>";
	html += "	<table id=\"Expedition\" class=\"table\"><tbody>";
	html += "		<tr>";
	html += "			<th rowspan=\"2\">艦隊</th>";
	html += "			<th colspan=\"3\">遠征先</th>";
	html += "		</tr>";
	html += "		<tr>";
	html += "			<th>所要時間</th>";
	html += "			<th>終了時刻</th>";
	html += "			<th>残り時間</th>";
	html += "		</tr>";

	for (var ii = 1; ii < ApiMember.basic.data.api_count_deck; ii++)
	{
		html += generateExpeditionRow(ii);
	}

	html += "	</tbody></table>";

	return html;
}

function generateExpeditionRow(index)
{
	// アラーム処理を追加する。

	var deck = ApiMember.deck.data[index];

	if (!deck) return "";

	var onMission = deck.api_mission[0];

	var deckName = deck.api_name;

	var mission = onMission ?
		ApiMaster.mission[deck.api_mission[1]].api_name :
		 "";

	var time = onMission ?
		ApiMaster.mission[deck.api_mission[1]].api_time + "分" :
		"";

	var completeTime = onMission ?
		Util.dateTimeToString(new Date(deck.api_mission[2])) :
		"";

	var lackTime =	onMission ?
		Util.timeToRemainingTimeString(deck.api_mission[2]) :
		"";

	var rowClass = "";
	if (lackTime == "終了") rowClass = "終了";

	if (lackTime == "")
	{
		f.status.expedition[index] = 0;
	}
	else if (lackTime == "終了")
	{
		if (f.status.expedition[index] != 0)
		{
			if (Util.getChecked("AlarmExpedition"))
				Sound.play(KancolleFoxConfig.loadSoundType(), "expedition");
		}
		f.status.expedition[index] = 0;
	}
	else
	{
		f.status.expedition[index] = 1;
	}

	var html = "";

	html += "		<tr>";
	html += "			<td rowspan=\"2\">" + deckName + "</td>";
	html += "			<td colspan=\"3\">" + mission + "</td>";
	html += "		</tr>";
	html += "		<tr>";
	html += "			<td class=\"datetime\">" + time + "</td>";
	html += "			<td class=\"datetime\">" + completeTime + "</td>";
	html += "			<td class=\"datetime " + rowClass + "\">" + lackTime + "</td>";
	html += "		</tr>";

	return html;
}
