Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};


var f =
{
	initialize: function()
	{
		var html = "";
		if (KancolleFoxConfig.loadBoolParameter(window.name, "configured"))
		{
			for (var id in functionNames)
			{
				var checked = KancolleFoxConfig.loadLaunchEnabled(window.name, "launch." + id) ? " checked" : "";
				html += "<div>"
					+ "<input type=\"checkbox\" id=\"" + id + "\"" + checked + " onchange=\"saveSettings();\">"
					+ functionNames[id]
					+ "</div>";
			}

			var soundType = KancolleFoxConfig.loadSoundType();
			for (var key in document.getElementById("soundType").getElementsByTagName("input"))
			{
				var radio = document.getElementById("soundType").getElementsByTagName("input")[key];
				if (radio.value == soundType)
				{
					radio.checked = true;
				}
			}
		}
		else
		{
			for (var id in functionNames)
			{
				var checked = KancolleFoxConfig.loadLaunchEnabled(window.name, id) ? " checked" : "";
				html += "<div>"
					+ "<input type=\"checkbox\" id=\"" + id + "\" checked onchange=\"saveSettings();\">"
					+ functionNames[id]
					+ "</div>";
			}
		}
		document.getElementById("functions").innerHTML = html;
		document.getElementById("reset").onclick = onReset_Clicked;

		document.getElementById("play1-1").onclick = function() { Sound.play("chime", "expedition"); };
		document.getElementById("play1-2").onclick = function() { Sound.play("chime", "kdock"); };
		document.getElementById("play1-3").onclick = function() { Sound.play("chime", "ndock"); };

		document.getElementById("play2-1").onclick = function() { Sound.play("child", "expedition"); };
		document.getElementById("play2-2").onclick = function() { Sound.play("child", "kdock"); };
		document.getElementById("play2-3").onclick = function() { Sound.play("child", "ndock"); };

		document.getElementById("play3-1").onclick = function() { Sound.play("girl", "expedition"); };
		document.getElementById("play3-2").onclick = function() { Sound.play("girl", "kdock"); };
		document.getElementById("play3-3").onclick = function() { Sound.play("girl", "ndock"); };

		document.getElementById("play4-1").onclick = function() { Sound.play("woman", "expedition"); };
		document.getElementById("play4-2").onclick = function() { Sound.play("woman", "kdock"); };
		document.getElementById("play4-3").onclick = function() { Sound.play("woman", "ndock"); };

		document.getElementById("play5-1").onclick = function() { Sound.play("se", "expedition"); };
		document.getElementById("play5-2").onclick = function() { Sound.play("se", "kdock"); };
		document.getElementById("play5-3").onclick = function() { Sound.play("se", "ndock"); };
	}
	,
	finalize: function()
	{
		windowSetings.save();
		KancolleFoxConfig.saveBoolParameter(window.name, "configured", true);
		saveSettings();
	}
};

function saveSettings()
{
	for (var id in functionNames)
	{
		KancolleFoxConfig.saveLaunchEnabled(
			window.name, "launch." + id, document.getElementById(id).checked);
	}

	for (var key in document.getElementById("soundType").getElementsByTagName("input"))
	{
		var radio = document.getElementById("soundType").getElementsByTagName("input")[key];
		if (radio.checked)
		{
			KancolleFoxConfig.saveSoundType(radio.value);
		}
	}
}

function onReset_Clicked()
{
	if (! confirm("保存してあるすべての設定を初期化します。\nこの操作を完了すると取り消すことができません。\nよろしいですか？")) return;

	KancolleFoxConfig.clearAll();

	alert("完了");
}