var BattleCalc =
{
	// 開幕航空戦
	calcKoukuStage3: function (data)
	{
		for (var ii = 0; ii < 6; ii++)
		{
			if (data.api_fdam)
			{
				var fDamage = data.api_fdam[ii + 1];
				if (fDamage && fDamage != -1)
				{
					fDamage = Math.floor(fDamage);
					graphData.f.ship[ii].nowHP -= fDamage;

					if (graphData.f.ship[ii].nowHP < 0) graphData.f.ship[ii].nowHP = 0;
				}
			}

			var eDamage = data.api_edam[ii + 1];
			if (eDamage && eDamage != -1)
			{
				eDamage = Math.floor(eDamage);
				graphData.e.ship[ii].nowHP -= eDamage;

				if (graphData.e.ship[ii].nowHP < 0) graphData.e.ship[ii].nowHP = 0;
			}
		}
	},

	// 支援艦攻撃
	calcSupportAttack: function(data)
	{
		if (data.api_support_airatack)
		{
			// 航空支援
			BattleCalc.calcKoukuStage3(data.api_support_airatack.api_stage3);
			return;
		}

		for (var ii = 0; ii < 6; ii++)
		{
			// 支援射撃・支援長距離雷撃
			if (data.api_support_hourai)
			{
				var hDamage = data.api_support_hourai.api_damage[ii + 1];
				if (hDamage && hDamage != -1)
				{
					hDamage = Math.floor(hDamage);
					graphData.e.ship[ii].nowHP -= hDamage;

					if (graphData.e.ship[ii].nowHP < 0) graphData.e.ship[ii].nowHP = 0;
				}
			}
		}
	},

	// 開幕雷撃戦
	calcOpeningAttack: function(data)
	{
		for (var ii = 0; ii < 6; ii++)
		{
			if (data.api_fdam)
			{
				var fDamage = data.api_fdam[ii + 1];
				if (fDamage && fDamage != -1)
				{
					fDamage = Math.floor(fDamage);
					graphData.f.ship[ii].nowHP -= fDamage;

					if (graphData.f.ship[ii].nowHP < 0) graphData.f.ship[ii].nowHP = 0;
				}
			}

			if (data.api_edam)
			{
				var eDamage = data.api_edam[ii + 1];
				if (eDamage && eDamage != -1)
				{
					eDamage = Math.floor(eDamage);
					graphData.e.ship[ii].nowHP -= eDamage;

					if (graphData.e.ship[ii].nowHP < 0) graphData.e.ship[ii].nowHP = 0;
				}
			}
		}
	},

	// 砲撃戦
	calcHougeki: function(data)
	{
		if (! data.api_df_list) return;
		for(var ii = 0; ii < data.api_df_list.length; ii++)
		{
			var df = data.api_df_list[ii];
			var damage = data.api_damage[ii];

			// df, damage はスカラーとリストが混在する
			//		dfがスカラーならdamageもスカラー、リストならリスト
			if (getType(damage) == "Array")
			{
				// リストの場合
				for(var jj = 0; jj < data.api_df_list[ii].length; jj++)
				{
					var df = data.api_df_list[ii][jj];
					var damage = data.api_damage[ii][jj];

					if (df != -1 && damage > 0)
					{
						damage = Math.floor(damage);
		
						if (df >= 7)
						{
							graphData.e.ship[df - 7].nowHP -= damage;
							if (graphData.e.ship[df - 7].nowHP < 0) graphData.e.ship[df - 7].nowHP = 0;
						}
						else
						{
							graphData.f.ship[df - 1].nowHP -= damage;
							if (graphData.f.ship[df - 1].nowHP < 0) graphData.f.ship[df - 1].nowHP = 0;
						}
					}
				}
			}
			else
			{
				// スカラーの場合
				if (df != -1 && damage != -1)
				{
					damage = Math.floor(damage);

					if (df >= 7)
					{
						graphData.e.ship[df - 7].nowHP -= damage;
						if (graphData.e.ship[df - 7].nowHP < 0) graphData.e.ship[df - 7].nowHP = 0;
					}
					else
					{
						graphData.f.ship[df - 1].nowHP -= damage;
						if (graphData.f.ship[df - 1].nowHP < 0) graphData.f.ship[df - 1].nowHP = 0;
					}
				}
			}
		}
	},

	// 砲撃戦(夜戦)
	calcHougekiYasen: function(data)
	{
		for(var ii = 0; ii < data.api_df_list.length; ii++)
		{
			for(var jj = 0; jj < data.api_df_list[ii].length; jj++)
			{
				var df = data.api_df_list[ii][jj];
				var damage = data.api_damage[ii][jj];

				if (df != -1 && damage > 0)
				{
					damage = Math.floor(damage);
		
					if (df >= 7)
					{
						graphData.e.ship[df - 7].nowHP -= damage;
						if (graphData.e.ship[df - 7].nowHP < 0) graphData.e.ship[df - 7].nowHP = 0;
					}
					else
					{
						graphData.f.ship[df - 1].nowHP -= damage;
						if (graphData.f.ship[df - 1].nowHP < 0) graphData.f.ship[df - 1].nowHP = 0;
					}
				}
			}
		}
	},

	// 雷撃戦
	calcRaigeki: function(data)
	{
		for (var ii = 0; ii < 6; ii++)
		{
			var fDamage = data.api_fdam[ii + 1];
			if (fDamage && fDamage != -1)
			{
				fDamage = Math.floor(fDamage);
				graphData.f.ship[ii].nowHP -= fDamage;

				if (graphData.f.ship[ii].nowHP < 0) graphData.f.ship[ii].nowHP = 0;
			}

			var eDamage = data.api_edam[ii + 1];

			if (eDamage && eDamage != -1)
			{
				eDamage = Math.floor(eDamage);
				graphData.e.ship[ii].nowHP -= eDamage;

				if (graphData.e.ship[ii].nowHP < 0) graphData.e.ship[ii].nowHP = 0;
			}
		}
	}

};

var BattleCalcCombined =
{
	// 開幕航空戦
	calcKoukuStage3: function (data)
	{
		for (var ii = 0; ii < 6; ii++)
		{
			var fDamage = data.api_fdam[ii + 1];
			if (fDamage && fDamage != -1)
			{
				fDamage = Math.floor(fDamage);
				graphData.c.ship[ii].nowHP -= fDamage;
				if (graphData.c.ship[ii].nowHP < 0) graphData.c.ship[ii].nowHP = 0;
			}

			if (data.api_edam)
			{
				var eDamage = data.api_edam[ii + 1];
				if (eDamage && eDamage != -1)
				{
					eDamage = Math.floor(eDamage);
					graphData.e.ship[ii].nowHP -= eDamage;
					if (graphData.e.ship[ii].nowHP < 0) graphData.e.ship[ii].nowHP = 0;
				}
			}
		}
	},

	// 開幕雷撃戦
	calcOpeningAttack: function(data)
	{
		for (var ii = 0; ii < 6; ii++)
		{
			if (data.api_fdam)
			{
				var fDamage = data.api_fdam[ii + 1];
				if (fDamage && fDamage != -1)
				{
					fDamage = Math.floor(fDamage);
					graphData.c.ship[ii].nowHP -= fDamage;

					if (graphData.c.ship[ii].nowHP < 0) graphData.c.ship[ii].nowHP = 0;
				}
			}

			if (data.api_edam)
			{
				var eDamage = data.api_edam[ii + 1];
				if (eDamage && eDamage != -1)
				{
					eDamage = Math.floor(eDamage);
					graphData.e.ship[ii].nowHP -= eDamage;

					if (graphData.e.ship[ii].nowHP < 0) graphData.e.ship[ii].nowHP = 0;
				}
			}
		}
	},

	// 砲撃戦
	calcHougeki: function(data)
	{
		if (! data.api_df_list) return;
		for(var ii = 0; ii < data.api_df_list.length; ii++)
		{
			var df = data.api_df_list[ii];
			var damage = data.api_damage[ii];

			// df, damage はスカラーとリストが混在する
			//		dfがスカラーならdamageもスカラー、リストならリスト
			if (getType(damage) == "Array")
			{
				// リストの場合
				for(var jj = 0; jj < data.api_df_list[ii].length; jj++)
				{
					var df = data.api_df_list[ii][jj];
					var damage = data.api_damage[ii][jj];

					if (df != -1 && damage > 0)
					{
						damage = Math.floor(damage);
		
						if (df >= 7)
						{
							graphData.e.ship[df - 7].nowHP -= damage;
							if (graphData.e.ship[df - 7].nowHP < 0) graphData.e.ship[df - 7].nowHP = 0;
						}
						else
						{
							graphData.c.ship[df - 1].nowHP -= damage;
							if (graphData.c.ship[df - 1].nowHP < 0) graphData.c.ship[df - 1].nowHP = 0;
						}
					}
				}
			}
			else
			{
				// スカラーの場合
				if (df != -1 && damage != -1)
				{
					damage = Math.floor(damage);

					if (df >= 7)
					{
						graphData.e.ship[df - 7].nowHP -= damage;
						if (graphData.e.ship[df - 7].nowHP < 0) graphData.e.ship[df - 7].nowHP = 0;
					}
					else
					{
						graphData.c.ship[df - 1].nowHP -= damage;
						if (graphData.c.ship[df - 1].nowHP < 0) graphData.c.ship[df - 1].nowHP = 0;
					}
				}
			}
		}
	},

	// 砲撃戦(夜戦)
	calcHougekiYasen: function(data)
	{
		for(var ii = 0; ii < data.api_df_list.length; ii++)
		{
			for(var jj = 0; jj < data.api_df_list[ii].length; jj++)
			{
				var df = data.api_df_list[ii][jj];
				var damage = data.api_damage[ii][jj];

				if (df != -1 && damage > 0)
				{
					damage = Math.floor(damage);
		
					if (df >= 7)
					{
						graphData.e.ship[df - 7].nowHP -= damage;
						if (graphData.e.ship[df - 7].nowHP < 0) graphData.e.ship[df - 7].nowHP = 0;
					}
					else
					{
						graphData.c.ship[df - 1].nowHP -= damage;
						if (graphData.c.ship[df - 1].nowHP < 0) graphData.c.ship[df - 1].nowHP = 0;
					}
				}
			}
		}
	},

	// 雷撃戦
	calcRaigeki: function(data)
	{
		for (var ii = 0; ii < 6; ii++)
		{
			var fDamage = data.api_fdam[ii + 1];
			if (fDamage && fDamage != -1)
			{
				fDamage = Math.floor(fDamage);
				graphData.c.ship[ii].nowHP -= fDamage;

				if (graphData.c.ship[ii].nowHP < 0) graphData.c.ship[ii].nowHP = 0;
			}

			if (data.api_edam)
			{
				var eDamage = data.api_edam[ii + 1];

				if (eDamage && eDamage != -1)
				{
					eDamage = Math.floor(eDamage);
					graphData.e.ship[ii].nowHP -= eDamage;

					if (graphData.e.ship[ii].nowHP < 0) graphData.e.ship[ii].nowHP = 0;
				}
			}
		}
	}
};
