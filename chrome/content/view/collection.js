Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};

var p = {};

var lastUpdateTime = 0;

var f =
{
	initialize: function()
	{
		Form.initializeSTypeItem(document.getElementById("FilterSType"));

		document.getElementById("HederInterval").value = KancolleFoxConfig.loadHeaderRowInterval(window.name);
		if (KancolleFoxConfig.loadBoolParameter(window.name, "configured"))
		{
			document.getElementById("DispSpec").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.spec");
			document.getElementById("DispEffect").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.effect");
			document.getElementById("DispUpgrade").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.upgrade");
			document.getElementById("DispGetMes").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.getMes");
			document.getElementById("DispDiscription").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.discription");
		}

		f.initParameters();

		document.onkeydown = f.onKeyDown;

		setInterval(function() { f.update(); }, 500);
	}
	,
	finalize: function()
	{
		windowSetings.save();
		KancolleFoxConfig.saveBoolParameter(window.name, "configured", true);
		KancolleFoxConfig.saveHeaderRowInterval(window.name, Util.getInputInteger("HederInterval"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.spec", Util.getChecked("DispSpec"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.effect", Util.getChecked("DispEffect"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.upgrade", Util.getChecked("DispUpgrade"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.getMes", Util.getChecked("DispGetMes"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.discription", Util.getChecked("DispDiscription"));
	}
	,
	onKeyDown: function(e)
	{
		var keyEvent = e || window.event;
		if (keyEvent.ctrlKey)
		{
			if (keyEvent.keyCode.toString() == 70)
			{
				document.getElementById("FilterName").focus();
			}
			else if (keyEvent.keyCode.toString() == 82)
			{
				f.clearFilter();
			}
		}
	}
	,
	update: function()
	{
		Form.initializeSTypeItem(document.getElementById("FilterSType"));

		if (!f.checkUpdated()) return;

		lastUpdateTime = new Date();

		document.getElementById("contents").innerHTML = generateHtml();
		document.getElementById("ShipCount").innerHTML = Object.keys(ApiMember.ship.data).length;
		document.getElementById("ShipMax").innerHTML = ApiMember.basic.data.api_max_chara;

		document.title = title + " 最終更新日時: " + Util.dateTimeToString(lastUpdateTime);
	}
	,
	initParameters: function()
	{
		p = {
			headerInterval : Util.getInputInteger("HederInterval"),

			filterName : "",
//			filterCType : "",
			filterSType : "",
			filterUpgrade : "",
			filterGet : "",

			dispSpec : "",
			dispEffect : "",
			dispUpgrade: "",
			dispGetMes : "",
			dispDiscription : ""
		};
	}
	,
	clearFilter: function()
	{
		document.getElementById("FilterName").value = "";
//		document.getElementById("FilterCType").value = "";
		document.getElementById("FilterSType").value = "";
		document.getElementById("FilterUpgrade").value = "";
		document.getElementById("FilterGet").value = "";
	}
	,
	checkUpdated: function()
	{
		var ret = false;

		var headerInterval		= Util.getInputInteger("HederInterval");

		var filterName			= Util.getInputText("FilterName");
//		var filterCType			= Util.getInputText("FilterCType");
		var filterSType			= Util.getSelectedOptionValue("FilterSType");
		var filterUpgrade		= Util.getSelectedOptionValue("FilterUpgrade");
		var filterGet		= Util.getSelectedOptionValue("FilterGet");

		var dispSpec		= Util.getChecked("DispSpec");
		var dispEffect		= Util.getChecked("DispEffect");
		var dispUpgrade		= Util.getChecked("DispUpgrade");
		var dispGetMes		= Util.getChecked("DispGetMes");
		var dispDiscription	= Util.getChecked("DispDiscription");

		ret = 
			(
				ApiMember.ship.updateTime
			)
			&&
			(
				// 初回更新
				(! lastUpdateTime)

				// APIデータが更新されたとき
				|| (lastUpdateTime < ApiMember.ship.updateTime)

				// ヘッダ行間隔の変更
				|| (p.headerInterval != headerInterval)

				// フィルターの変更
				|| (p.filterName != filterName)
//				|| (p.filterCType != filterCType)
				|| (p.filterSType != filterSType)
				|| (p.filterUpgrade != filterUpgrade)
				|| (p.filterGet != filterGet)

				// 表示項目の変更
				|| (p.dispSpec != dispSpec)
				|| (p.dispEffect != dispEffect)
				|| (p.dispUpgrade != dispUpgrade)
				|| (p.dispGetMes != dispGetMes)
				|| (p.dispDiscription != dispDiscription)
			);

		if (ret)
		{
			p = {
				headerInterval : headerInterval,

				filterName : filterName,
//				filterCType : filterCType,
				filterSType : filterSType,
				filterUpgrade : filterUpgrade,
				filterGet : filterGet,

				dispSpec : dispSpec,
				dispEffect : dispEffect,
				dispUpgrade : dispUpgrade,
				dispGetMes : dispGetMes,
				dispDiscription : dispDiscription
			};
		}

		return ret;
	}
};

function selectShip()
{
	var shipCounts = countShip();

	var defaultShips = Object.keys(ApiMaster.ship);
	var completedShips = [];

	for (var key in ApiMaster.ship)
	{
		var ship = ApiMaster.ship[key];
		var next = ship.api_aftershipid;

		if (ApiMaster.ship[next])
		{
			if (defaultShips.indexOf(next) != -1)
			{
				// 那珂と那珂Ｓが那珂改を削除しようとするので、存在する場合だけ削除します。
				defaultShips.splice(defaultShips.indexOf(next), 1);
			}
		}
		else
		{
			completedShips.push(key);
		}
	}

	var ships = [];
	for (var key in ApiMaster.ship)
	{
		var ship = ApiMaster.ship[key];
		var upgradeRoadMap = scanRoadMap(ship, []);

		var shipCount = shipCounts[ship.api_id] ? shipCounts[ship.api_id] : 0;
		var upgradeCount = 0;
		for (var memberKey in ApiMember.ship.data)
		{
			var member = ApiMember.ship.data[memberKey];
			if (upgradeRoadMap.indexOf(member.api_ship_id) != -1)
			{
				upgradeCount++;
			}
		}

		if (
			(
				"" == p.filterName
				|| ship.api_name.match(p.filterName)
				|| ship.api_yomi.match(p.filterName)
			) &&
/*
			(
				"" == p.filterCType
				|| (
					(
						localData.CType[ship.api_ctype]
						&& localData.CType[ship.api_ctype].match(p.filterCType)
					)
					||
					(
						! localData.CType[ship.api_ctype]
						&& ship.api_ctype == p.filterCType
					)
				)
			) &&
*/
			(
				"" == p.filterSType
				|| ship.api_stype == p.filterSType
			) &&

			(
				"" == p.filterUpgrade
				|| ("初期形態のみ" == p.filterUpgrade && defaultShips.indexOf(ship.api_id) != -1)
				|| ("初期形態以外" == p.filterUpgrade && defaultShips.indexOf(ship.api_id) == -1)
				|| ("最終形態のみ" == p.filterUpgrade && completedShips.indexOf(ship.api_id) != -1)
				|| ("最終形態以外" == p.filterUpgrade && completedShips.indexOf(ship.api_id) == -1)
			) &&

			(
				"" == p.filterGet
				|| ("あり" == p.filterGet && shipCount > 0)
				|| ("あり(上位含む)" == p.filterGet && shipCount + upgradeCount > 0)
				|| ("なし" == p.filterGet && shipCount == 0)
				|| ("なし(上位含む)" == p.filterGet && shipCount + upgradeCount == 0)
			)
		)
		{
			var data = 
			{
				ship: ship,
				shipCount: shipCounts[ship.api_id] ? shipCounts[ship.api_id] : 0,
				upgradeCount: upgradeCount,
				default: defaultShips.indexOf(ship.api_id) != -1,
				completed: completedShips.indexOf(ship.api_id) != -1
			};
			ships.push(data);
		}
	}

	var ret = [];
	for (var ii = 0; ii < ships.length; ii++)
	{
		ret.push(ships[ii]);
	}

	ret.sort(
		function (a, b)
		{
//			if (a.ship.api_ctype > b.ship.api_ctype) return 1;
//			if (a.ship.api_ctype < b.ship.api_ctype) return -1;
//			if (a.ship.api_cnum > b.ship.api_cnum) return 1;
//			if (a.ship.api_cnum < b.ship.api_cnum) return -1;

			if (a.ship.api_stype > b.ship.api_stype) return 1;
			if (a.ship.api_stype < b.ship.api_stype) return -1;

			if (a.ship.api_name > b.ship.api_name) return 1;
			if (a.ship.api_name < b.ship.api_name) return -1;
			return 0;
		}
	);

	return ret;
}

function generateHtml()
{
	var shipList = selectShip();

	var html = "";
	html += "	<table id=\"ShipTable\"><tbody>";

	var colspan = 5
		+ (p.dispSpec ? 15: 0)
		+ (p.dispEffect ? 8: 0)
		;

	var rowspan = 1
		+ (p.dispUpgrade ? 1: 0)
		+ (p.dispGetMes ? 1: 0)
//		+ (p.dispDiscription ? 1: 0)
		;

	var rowNumber = 0;
	for(var ii = 0; ii < shipList.length; ii++)
	{
		if (
			(p.headerInterval == 0 && rowNumber == 0)
			|| (rowNumber % p.headerInterval) == 0)
		{
			html += generateThr(colspan, rowspan + 1);
		}
		html += generateTdr(rowNumber, shipList[ii], colspan, rowspan);
		rowNumber ++;
	}

	html += "	</tbody></table>";

	return html;
}

function generateThr(colspan, rowspan)
{
	var html = "";

	html+= "			<tr>";
	html+= "				<th rowspan=\"" + rowspan + "\">ID</th>";
	html+= "				<th rowspan=\"" + rowspan + "\">Sort<br>No.</th>";

	html+= "				<th colspan=\"2\">艦名</th>";

	html+= "				<th colspan=\"2\">保有数</th>";

//	html+= "				<th colspan=\"2\">型</th>";
	html+= "				<th rowspan=\"2\">艦種</th>";

	if (p.dispSpec)
	{
		html+= "				<th rowspan=\"2\">ｽﾛｯﾄ</th>";
		html+= "				<th colspan=\"2\">積載</th>";

		html+= "				<th colspan=\"2\">耐久</th>";
		html+= "				<th colspan=\"2\">装甲</th>";

		html+= "				<th colspan=\"2\">砲撃</th>";
		html+= "				<th colspan=\"2\">雷装</th>";
	//	html+= "				<th colspan=\"2\">爆撃</th>";
		html+= "				<th colspan=\"2\">対空</th>";
//		html+= "				<th colspan=\"2\">対潜</th>";

	//	html+= "				<th colspan=\"2\">砲命</th>";
	//	html+= "				<th colspan=\"2\">雷命</th>";

//		html+= "				<th colspan=\"2\">回避</th>";
	//	html+= "				<th colspan=\"2\">砲避</th>";
	//	html+= "				<th colspan=\"2\">雷避</th>";
	//	html+= "				<th colspan=\"2\">爆避</th>";

//		html+= "				<th colspan=\"2\">索敵</th>";
		html+= "				<th colspan=\"2\">運</th>";
	}

	if (p.dispEffect)
	{
		html+= "				<th colspan=\"4\">分解資材</th>";
		html+= "				<th colspan=\"4\">改造素材</th>";
	}
	html+= "			</tr>";
	html+= "			<tr>";
	html+= "				<th>名</th>";
	html+= "				<th>読み</th>";

	html+= "				<th>同</th>";
	html+= "				<th>改</th>";

//	html+= "				<th>型名</th>";
//	html+= "				<th>番</th>";

	if (p.dispSpec)
	{
		html+= "				<th>燃</th>";
		html+= "				<th>弾</th>";

		html+= "				<th>初</th>	<!-- 耐久 -->";
		html+= "				<th>限</th>	<!-- 耐久 -->";
		html+= "				<th>初</th>	<!-- 装甲 -->";
		html+= "				<th>限</th>	<!-- 装甲 -->";

		html+= "				<th>初</th>	<!-- 砲撃 -->";
		html+= "				<th>限</th>	<!-- 砲撃 -->";
		html+= "				<th>初</th>	<!-- 雷装 -->";
		html+= "				<th>限</th>	<!-- 雷装 -->";
	//	html+= "				<th>初</th>	<!-- 爆撃 -->";
	//	html+= "				<th>限</th>	<!-- 爆撃 -->";
		html+= "				<th>初</th>	<!-- 対空 -->";
		html+= "				<th>限</th>	<!-- 対空 -->";
//		html+= "				<th>初</th>	<!-- 対潜 -->";
//		html+= "				<th>限</th>	<!-- 対潜 -->";

	//	html+= "				<th>初</th>	<!-- 砲命 -->";
	//	html+= "				<th>初</th>	<!-- 砲命 -->";
	//	html+= "				<th>初</th>	<!-- 雷命 -->";
	//	html+= "				<th>初</th>	<!-- 雷命 -->";

//		html+= "				<th>初</th>	<!-- 回避 -->";
//		html+= "				<th>限</th>	<!-- 回避 -->";
	//	html+= "				<th>初</th>	<!-- 砲避 -->";
	//	html+= "				<th>限</th>	<!-- 砲避 -->";
	//	html+= "				<th>初</th>	<!-- 雷避 -->";
	//	html+= "				<th>限</th>	<!-- 雷避 -->";
	//	html+= "				<th>初</th>	<!-- 爆避 -->";
	//	html+= "				<th>限</th>	<!-- 爆避 -->";

//		html+= "				<th>初</th>	<!-- 索敵 -->";
//		html+= "				<th>限</th>	<!-- 索敵 -->";
		html+= "				<th>初</th>	<!-- 運 -->";
		html+= "				<th>限</th>	<!-- 運 -->";
	}

	if (p.dispEffect)
	{
		html+= "				<th>燃</th>";
		html+= "				<th>弾</th>";
		html+= "				<th>鋼</th>";
		html+= "				<th>ボ</th>";

		html+= "				<th>火</th>";
		html+= "				<th>雷</th>";
		html+= "				<th>空</th>";
		html+= "				<th>装</th>";
	}

	html+= "			</tr>";

	if (p.dispUpgrade)
	{
		html+= "			<tr>";
		html+= "				<th colspan=\"" + colspan + "\">改造</th>";
		html+= "			</tr>";
	}

	if (p.dispGetMes)
	{
		html+= "			<tr>";
		html+= "				<th colspan=\"" + colspan + "\">着任ボイス</th>";
		html+= "			</tr>";
	}
/*
	if (p.dispDiscription)
	{
		html+= "			<tr>";
		html+= "				<th colspan=\"" + colspan + "\">説明</th>";
		html+= "			</tr>";
	}
*/
	return html;
}

function generateTdr(number, data, colspan, rowspan)
{
//	var cName = localData.CType[data.ship.api_ctype] ? localData.CType[data.ship.api_ctype] : "#" + data.ship.api_ctype;
	var roadmap = generateRoadMap(data.ship, []);

	var html = "";

	html+= "			<tr onmouseover=\"RowCursor.hover(this)\" onmouseout=\"RowCursor.leave(this)\">";
	html+= "				<td class=\"number\" rowspan=\"" + rowspan + "\">" + data.ship.api_id + "</td>";
	html+= "				<td class=\"number\" rowspan=\"" + rowspan + "\">" + data.ship.api_sortno + "</td>";

	html+= "				<td>" + data.ship.api_name + "</td>";
	html+= "				<td>" + data.ship.api_yomi + "</td>";

	html+= "				<td class=\"number\">" + data.shipCount + "</td>"; // 保有
	html+= "				<td class=\"number\">" + data.upgradeCount + "</td>"; // 上位

//	html+= "				<td>" + cName + "</td>";
//	html+= "				<td class=\"center\">" + data.ship.api_cnum + "番艦</td>";
	html+= "				<td>" + ApiMaster.sType[data.ship.api_stype].api_name + "</td>";

	if (p.dispSpec)
	{
		html+= "				<td class=\"number\">" + data.ship.api_slot_num + "</td>";
		html+= "				<td class=\"number\">" + data.ship.api_fuel_max + "</td>";
		html+= "				<td class=\"number\">" + data.ship.api_bull_max + "</td>";

		html+= "				<td class=\"number\">" + data.ship.api_taik[0] + "</td>	<!-- 耐久 -->";
		html+= "				<td class=\"number\">" + data.ship.api_taik[1] + "</td>	<!-- 耐久 -->";
		html+= "				<td class=\"number\">" + data.ship.api_souk[0] + "</td>	<!-- 装甲 -->";
		html+= "				<td class=\"number\">" + data.ship.api_souk[1] + "</td>	<!-- 装甲 -->";

		html+= "				<td class=\"number\">" + data.ship.api_houg[0] + "</td>	<!-- 砲撃 -->";
		html+= "				<td class=\"number\">" + data.ship.api_houg[1] + "</td>	<!-- 砲撃 -->";
		html+= "				<td class=\"number\">" + data.ship.api_raig[0] + "</td>	<!-- 雷装 -->";
		html+= "				<td class=\"number\">" + data.ship.api_raig[1] + "</td>	<!-- 雷装 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_baku[1] + "</td>	<!-- 爆撃 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_baku[1] + "</td>	<!-- 爆撃 -->";
		html+= "				<td class=\"number\">" + data.ship.api_tyku[0] + "</td>	<!-- 対空 -->";
		html+= "				<td class=\"number\">" + data.ship.api_tyku[1] + "</td>	<!-- 対空 -->";
//		html+= "				<td class=\"number\">" + data.ship.api_tais[0] + "</td>	<!-- 対潜 -->";
//		html+= "				<td class=\"number\">" + data.ship.api_tais[1] + "</td>	<!-- 対潜 -->";

	//	html+= "				<td class=\"number\">" + data.ship.api_houm[0] + "</td>	<!-- 砲命 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_houm[1] + "</td>	<!-- 砲命 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_raim[0] + "</td>	<!-- 雷命 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_raim[1] + "</td>	<!-- 雷命 -->";

//		html+= "				<td class=\"number\">" + data.ship.api_kaih[0] + "</td>	<!-- 回避 -->";
//		html+= "				<td class=\"number\">" + data.ship.api_kaih[1] + "</td>	<!-- 回避 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_houk[0] + "</td>	<!-- 砲避 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_houk[1] + "</td>	<!-- 砲避 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_raik[0] + "</td>	<!-- 雷避 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_raik[1] + "</td>	<!-- 雷避 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_bakk[0] + "</td>	<!-- 爆避 -->";
	//	html+= "				<td class=\"number\">" + data.ship.api_bakk[1] + "</td>	<!-- 爆避 -->";


//		html+= "				<td class=\"number\">" + data.ship.api_saku[0] + "</td>	<!-- 索敵 -->";
//		html+= "				<td class=\"number\">" + data.ship.api_saku[1] + "</td>	<!-- 索敵 -->";
		html+= "				<td class=\"number\">" + data.ship.api_luck[0] + "</td>	<!-- 運 -->";
		html+= "				<td class=\"number\">" + data.ship.api_luck[1] + "</td>	<!-- 運 -->";
	}

	if (p.dispEffect)
	{
		html+= "				<td class=\"number\">" + data.ship.api_broken[0] + "</td>";
		html+= "				<td class=\"number\">" + data.ship.api_broken[1] + "</td>";
		html+= "				<td class=\"number\">" + data.ship.api_broken[2] + "</td>";
		html+= "				<td class=\"number\">" + data.ship.api_broken[3] + "</td>";

		html+= "				<td class=\"number\">" + data.ship.api_powup[0] + "</td>";
		html+= "				<td class=\"number\">" + data.ship.api_powup[1] + "</td>";
		html+= "				<td class=\"number\">" + data.ship.api_powup[2] + "</td>";
		html+= "				<td class=\"number\">" + data.ship.api_powup[3] + "</td>";
	}

	html+= "			</tr>";

	if (p.dispUpgrade)
	{
		html+= "			<tr>";
		html+= "				<td colspan=\"" + colspan + "\"><div class=\"description\">" + roadmap.join(">") + "</div></td>";
		html+= "			</tr>";
	}

	if (p.dispGetMes)
	{
		html+= "			<tr>";
		html+= "				<td colspan=\"" + colspan + "\"><div class=\"description\">" + data.ship.api_getmes + "</div></td>";
		html+= "			</tr>";
	}
/*
	if (p.dispDiscription)
	{
		html+= "			<tr>";
		html+= "				<td colspan=\"" + colspan + "\"><div class=\"description\">" + data.ship.api_sinfo + "</div></td>";
		html+= "			</tr>";
	}
*/
	return html;
}



function countShip()
{
	var ret = {};

	for (var key in ApiMember.ship.data)
	{
		var id = ApiMember.ship.data[key].api_ship_id;
		if (!ret[id]) ret[id] = 0;
		ret[id] ++;
	}

	return ret;
}

function scanRoadMap(ship, roadmap)
{
	var nextShip = ApiMaster.ship[ship.api_aftershipid];

	if (nextShip)
	{
		roadmap.push(nextShip.api_id);
		roadmap = scanRoadMap(nextShip, roadmap);
	}

	return roadmap;
}

function generateRoadMap(ship, roadmap)
{
	var nextShip = ApiMaster.ship[ship.api_aftershipid];

	if (nextShip)
	{
		roadmap.push("Lv." + ship.api_afterlv + " " + nextShip.api_name);
		roadmap = generateRoadMap(nextShip, roadmap);
	}

	return roadmap;
}
