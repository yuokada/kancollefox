Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};

var p = {};

var lastUpdateTime = 0;

var f =
{
	initialize: function()
	{
		document.getElementById("ExportTsv").onclick = f.exportTsv_onclick;

		Form.initializeSTypeItem(document.getElementById("FilterSType"));

		document.getElementById("HederInterval").value = KancolleFoxConfig.loadHeaderRowInterval(window.name);
		if (KancolleFoxConfig.loadBoolParameter(window.name, "configured"))
		{
			document.getElementById("DispId").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.id");
			document.getElementById("DispDeck").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.deck");
			document.getElementById("DispSally").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.sally");
			document.getElementById("DispLockStatus").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.lockStatus");
			document.getElementById("DispYomi").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.yomi");
			document.getElementById("DispType").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.type");
			document.getElementById("DispLv").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.level");
			document.getElementById("DispCondition").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.condition");
			document.getElementById("DispFuel").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.supply");
			document.getElementById("DispSpec").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.spec");
			document.getElementById("DispPowerUp").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.powerUp");
			document.getElementById("DispUpGrade").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.upGrade");
			document.getElementById("DispRepairTime").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.repairTime");
		}

		f.initParameters();

		document.onkeydown = f.onKeyDown;

		setInterval(function() { f.update(); }, 500);
	}
	,
	finalize: function()
	{
		windowSetings.save();
		KancolleFoxConfig.saveHeaderRowInterval(window.name, Util.getInputInteger("HederInterval"));
		KancolleFoxConfig.saveBoolParameter(window.name, "configured", true);
		KancolleFoxConfig.saveBoolParameter(window.name, "display.id", Util.getChecked("DispId"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.deck", Util.getChecked("DispDeck"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.sally", Util.getChecked("DispSally"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.lockStatus", Util.getChecked("DispLockStatus"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.yomi", Util.getChecked("DispYomi"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.type", Util.getChecked("DispType"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.level", Util.getChecked("DispLv"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.condition", Util.getChecked("DispCondition"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.supply", Util.getChecked("DispFuel"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.spec", Util.getChecked("DispSpec"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.powerUp", Util.getChecked("DispPowerUp"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.upGrade", Util.getChecked("DispUpGrade"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.repairTime", Util.getChecked("DispRepairTime"));
	}
	,
	onKeyDown: function(e)
	{
		var keyEvent = e || window.event;
		if (keyEvent.ctrlKey)
		{
			if (keyEvent.keyCode.toString() == 70)
			{
				document.getElementById("FilterName").focus();
			}
			else if (keyEvent.keyCode.toString() == 82)
			{
				f.clearFilter();
			}
		}
	}
	,
	update: function()
	{
		Form.initializeSTypeItem(document.getElementById("FilterSType"));

		if (!f.checkUpdated()) return;

		lastUpdateTime = new Date();

		document.getElementById("contents").innerHTML = generateHtml();
		document.getElementById("ShipCount").innerHTML = Object.keys(ApiMember.ship.data).length;
		document.getElementById("ShipMax").innerHTML = ApiMember.basic.data.api_max_chara;

		document.title = title + " 最終更新日時: " + Util.dateTimeToString(lastUpdateTime);
	}
	,
	initParameters: function()
	{
		p = {
			sortOrder : Util.getSelectedOptionText("SortOrder"),
			headerInterval : Util.getInputInteger("HederInterval"),

			filterName : "",
//			filterCType : "",
			filterSType : "",
			filterLvMin : "",
			filterLvMax : "",
			filterLockStatus : "",
			filterHealth : "",
			filterPowerUp : "",
			filterId : "",

			dispId : "",
			dispDeck : "",
			dispSally : "",
			dispLockStatus : "",
			dispYomi : "",
			dispType : "",
			dispLv : "",
			dispCondition : "",
			dispFuel : "",
			dispSpec : "",
			dispPowerUp : "",
			dispUpGrade : "",
			dispRepairTime : ""
		};
	}
	,
	clearFilter: function()
	{
		document.getElementById("FilterName").value = "";
//		document.getElementById("FilterCType").value = "";
		document.getElementById("FilterSType").value = "";
		document.getElementById("FilterLvMin").value = "";
		document.getElementById("FilterLvMax").value = "";
		document.getElementById("FilterLockStatus").value = "";
		document.getElementById("FilterHealth").value = "";
		document.getElementById("FilterPowerUp").value = "";
		document.getElementById("FilterId").value = "";
	}
	,
	checkUpdated: function()
	{
		var ret = false;

		var sortOrder			= Util.getSelectedOptionText("SortOrder");
		var headerInterval		= Util.getInputInteger("HederInterval");

		var filterName			= Util.getInputText("FilterName");
//		var filterCType			= Util.getInputText("FilterCType");
		var filterSType			= Util.getSelectedOptionValue("FilterSType");
		var filterLvMin			= Util.getInputInteger("FilterLvMin");
		var filterLvMax			= Util.getInputInteger("FilterLvMax");
		var filterLockStatus	= Util.getSelectedOptionText("FilterLockStatus");
		var filterHealth		= Util.getSelectedOptionText("FilterHealth");
		var filterPowerUp		= Util.getSelectedOptionText("FilterPowerUp");
		var filterId			= Util.getInputText("FilterId");

		var dispId			= Util.getChecked("DispId");
		var dispDeck		= Util.getChecked("DispDeck");
		var dispSally		= Util.getChecked("DispSally");
		var dispLockStatus	= Util.getChecked("DispLockStatus");
		var dispYomi		= Util.getChecked("DispYomi");
		var dispType		= Util.getChecked("DispType");
		var dispLv			= Util.getChecked("DispLv");
		var dispCondition	= Util.getChecked("DispCondition");
		var dispFuel		= Util.getChecked("DispFuel");
		var dispSpec		= Util.getChecked("DispSpec");
		var dispPowerUp		= Util.getChecked("DispPowerUp");
		var dispUpGrade		= Util.getChecked("DispUpGrade");
		var dispRepairTime	= Util.getChecked("DispRepairTime");

		ret = 
			(
				ApiMember.ship.updateTime
				&& ApiMember.shipInDeck.updateTime
				&& ApiMember.deck.updateTime
			)
			&&
			(
				// 初回更新
				(! lastUpdateTime)

				// APIデータが更新されたとき
				|| (lastUpdateTime < ApiMember.ship.updateTime)
				|| (lastUpdateTime < ApiMember.deck.updateTime)
				|| (lastUpdateTime < ApiMember.slotItem.updateTime)

				// ソート条件・ヘッダ行間隔の変更
				|| (p.sortOrder != sortOrder)
				|| (p.headerInterval != headerInterval)

				// フィルターの変更
				|| (p.filterName != filterName)
//				|| (p.filterCType != filterCType)
				|| (p.filterSType != filterSType)
				|| (p.filterLvMin != filterLvMin)
				|| (p.filterLvMax != filterLvMax)
				|| (p.filterLockStatus != filterLockStatus)
				|| (p.filterHealth != filterHealth)
				|| (p.filterPowerUp != filterPowerUp)
				|| (p.filterId != filterId)

				// 表示項目の変更
				|| (p.dispId != dispId)
				|| (p.dispDeck != dispDeck)
				|| (p.dispSally != dispSally)
				|| (p.dispLockStatus != dispLockStatus)
				|| (p.dispYomi != dispYomi)
				|| (p.dispType != dispType)
				|| (p.dispLv != dispLv)
				|| (p.dispCondition != dispCondition)
				|| (p.dispFuel != dispFuel)
				|| (p.dispSpec != dispSpec)
				|| (p.dispPowerUp != dispPowerUp)
				|| (p.dispUpGrade != dispUpGrade)
				|| (p.dispRepairTime != dispRepairTime)
			);

		if (ret)
		{
			p = {
				sortOrder : sortOrder,
				headerInterval : headerInterval,

				filterName : filterName,
//				filterCType : filterCType,
				filterSType : filterSType,
				filterLvMin : filterLvMin,
				filterLvMax : filterLvMax,
				filterLockStatus : filterLockStatus,
				filterHealth : filterHealth,
				filterPowerUp : filterPowerUp,
				filterId : filterId,

				dispId : dispId,
				dispDeck : dispDeck,
				dispSally : dispSally,
				dispLockStatus : dispLockStatus,
				dispYomi : dispYomi,
				dispType : dispType,
				dispLv : dispLv,
				dispCondition : dispCondition,
				dispFuel : dispFuel,
				dispSpec : dispSpec,
				dispPowerUp : dispPowerUp,
				dispUpGrade : dispUpGrade,
				dispRepairTime : dispRepairTime
			};
		}

		return ret;
	}
	,
	exportTsv_onclick: function()
	{
		var table = document.getElementById("ShipTable");
		if (!table)
		{
			return;
		}


		var header = "";
		if (p.dispId)
		{
			header += "No.	";
			header += "ID	";
			header += "Ship ID	";
			header += "Sort No.	";
		}
		if (p.dispDeck)
		{
			header += "艦隊	";
			header += "艦番	";
		}
		if (p.dispSally)
		{
			header += "作戦	";
		}
		if (p.dispLockStatus)
		{
			header += "鍵	";
		}
		header += "艦名	";
		if (p.dispYomi)
		{
			header += "艦名(かな)	";
		}
		if (p.dispType)
		{
//			header += "型名	";
			header += "艦種	";
		}
		if (p.dispLv)
		{
			header += "Lv	";
			header += "経験値 現在	";
			header += "経験値 次	";
			header += "経験値 %	";
		}
		if (p.dispCondition)
		{
			header += "調子	";
		}
		if (p.dispFuel)
		{
			header += "燃料 現在	";
			header += "燃料 最大	";
			header += "弾薬 現在	";
			header += "弾薬 最大	";
			header += "耐久 現在	";
			header += "耐久 最大	";
		}
		if (p.dispSpec)
		{
			header += "装備 装着	";
			header += "装備 ｽﾛｯﾄ	";
			header += "装甲	";
			header += "回避	";
			header += "火力	";
			header += "雷装	";
			header += "対空	";
			header += "対潜	";
			header += "索敵	";
			header += "運	";
		}
		if (p.dispPowerUp)
		{
			header += "強化火 現在	";
			header += "強化火 最大	";
			header += "強化火 残り	";
			header += "強化雷 現在	";
			header += "強化雷 最大	";
			header += "強化雷 残り	";
			header += "強化空 現在	";
			header += "強化空 最大	";
			header += "強化空 残り	";
			header += "強化装 現在	";
			header += "強化装 最大	";
			header += "強化装 残り	";
		}
		if (p.dispUpGrade)
		{
			header += "改造 Lv	";
			header += "改造 艦名	";
		}
		if (p.dispRepairTime)
		{
			header += "修復時間	";
		}
		header += "\n";

		FileIO.saveTable("ship.tsv", table, header);
		return false;
	}
};

function selectShip()
{
	var ret = [];

	for (var key in ApiMember.ship.data)
	{
		var data = ApiMember.ship.data[key];
		if (
			(
				"" == p.filterName
				|| ApiMaster.ship[data.api_ship_id].api_name.match(p.filterName)
				|| ApiMaster.ship[data.api_ship_id].api_yomi.match(p.filterName)
			) &&
/*
			(
				"" == p.filterCType
				|| (
					(
						localData.CType[ApiMaster.ship[data.api_ship_id].api_ctype]
						&& localData.CType[ApiMaster.ship[data.api_ship_id].api_ctype].match(p.filterCType)
					)
					||
					(
						ApiMaster.ship[data.api_ship_id].api_ctype == p.filterCType
					)
				)
			) &&
*/
			(
				"" == p.filterSType
				|| ApiMaster.ship[data.api_ship_id].api_stype == p.filterSType
			) &&

			(
				"" == p.filterLvMin
				|| data.api_lv >= p.filterLvMin
			) &&

			(
				"" == p.filterLvMax
				|| data.api_lv <= p.filterLvMax
			) &&

			(
				"" == p.filterLockStatus
				|| (p.filterLockStatus == "あり" && data.api_locked)
				|| (p.filterLockStatus == "なし" && !data.api_locked)
			) &&

			(
				"" == p.filterId
				|| p.filterId.replace(/ /g, ",").split(",").indexOf(data.api_id) != -1
			) &&

			checkFilterHealth(data) &&
			checkFilterPowerUp(data)
		)
		{
			ret.push(data);
		}
	}

	switch (p.sortOrder)
	{
		case "ID順(入手順)":
			ret.sort(
				function (a, b)
				{
					return a.api_id - b.api_id;
				}
			);
			break;
		case "SortNo.順(図鑑順)":
			ret.sort(
				function (a, b)
				{
					var ret = a.api_sortno - b.api_sortno;
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "艦隊順":
			ret.sort(
				function (a, b)
				{
					var ret = 0;

					var deckA = ApiMember.shipInDeck.data[a.api_id];
					var deckB = ApiMember.shipInDeck.data[b.api_id];

					var deckNoA = (deckA != undefined) ? deckA.deckNo : Number.MAX_VALUE;
					var deckNoB = (deckB != undefined) ? deckB.deckNo : Number.MAX_VALUE;
					ret = deckNoA - deckNoB;
					if (ret) return ret;

					var shipNoA = (deckA != undefined) ? deckA.shipNo : Number.MAX_VALUE;
					var shipNoB = (deckB != undefined) ? deckB.shipNo : Number.MAX_VALUE;

					ret = shipNoA - shipNoB;
					if (ret) return ret;

					return a.api_id - b.api_id;
				}
			);
			break;
		case "作戦別":
			ret.sort(
				function(a, b)
				{
					var ret = 0;

					ret = b.api_sally_area - a.api_sally_area;
					if (ret) return ret;

					var deckA = ApiMember.shipInDeck.data[a.api_id];
					var deckB = ApiMember.shipInDeck.data[b.api_id];

					var deckNoA = (deckA != undefined) ? deckA.deckNo : Number.MAX_VALUE;
					var deckNoB = (deckB != undefined) ? deckB.deckNo : Number.MAX_VALUE;
					ret = deckNoA - deckNoB;
					if (ret) return ret;

					var shipNoA = (deckA != undefined) ? deckA.shipNo : Number.MAX_VALUE;
					var shipNoB = (deckB != undefined) ? deckB.shipNo : Number.MAX_VALUE;

					ret = shipNoA - shipNoB;
					if (ret) return ret;

					return b.api_exp[0] - a.api_exp[0];
				}
			);
			break;
		case "艦種・艦名別":
			ret.sort(
				function (a, b)
				{
					var ret = 0;

					ret = ApiMaster.ship[a.api_ship_id].api_stype - ApiMaster.ship[b.api_ship_id].api_stype;
					if (ret != 0) return ret;

					ret = Util.compare(ApiMaster.ship[a.api_ship_id].api_yomi, ApiMaster.ship[b.api_ship_id].api_yomi);
					if (ret != 0) return ret;

					ret = Util.compare(ApiMaster.ship[a.api_ship_id].api_name, ApiMaster.ship[b.api_ship_id].api_name);
					if (ret != 0) return ret;

					return a.api_id - b.api_id;
				}
			);
			break;
/*
		case "艦種・型・艦名別":
			ret.sort(
				function (a, b)
				{
					var ret = 0;

					ret = ApiMaster.ship[a.api_ship_id].api_stype - ApiMaster.ship[b.api_ship_id].api_stype;
					if (ret != 0) return ret;

					var ctypeA = ApiMaster.ship[a.api_ship_id].api_ctype;
					var ctypeB = ApiMaster.ship[b.api_ship_id].api_ctype;

					if (ctypeA == undefined) ctypeA = -1;
					if (ctypeB == undefined) ctypeB = -1;

					ret = ctypeA - ctypeB;
					if (ret != 0) return ret;

					ret = Util.compare(ApiMaster.ship[a.api_ship_id].api_yomi, ApiMaster.ship[b.api_ship_id].api_yomi);
					if (ret != 0) return ret;

					ret = Util.compare(ApiMaster.ship[a.api_ship_id].api_name, ApiMaster.ship[b.api_ship_id].api_name);
					if (ret != 0) return ret;

					return a.api_id - b.api_id;
				}
			);
			break;
		case "型・艦名別":
			ret.sort(
				function (a, b)
				{
					var ret = 0;

					var ctypeA = ApiMaster.ship[a.api_ship_id].api_ctype;
					var ctypeB = ApiMaster.ship[b.api_ship_id].api_ctype;

					if (ctypeA == undefined) ctypeA = -1;
					if (ctypeB == undefined) ctypeB = -1;

					ret = ctypeA - ctypeB;
					if (ret != 0) return ret;

					ret = Util.compare(ApiMaster.ship[a.api_ship_id].api_yomi, ApiMaster.ship[b.api_ship_id].api_yomi);
					if (ret != 0) return ret;

					ret = Util.compare(ApiMaster.ship[a.api_ship_id].api_name, ApiMaster.ship[b.api_ship_id].api_name);
					if (ret != 0) return ret;

					return a.api_id - b.api_id;
				}
			);
			break;
*/
		case "五十音順":
			ret.sort(
				function (a, b)
				{
					var ret = 0;

					ret = Util.compare(ApiMaster.ship[a.api_ship_id].api_yomi, ApiMaster.ship[b.api_ship_id].api_yomi);
					if (ret != 0) return ret;

					ret = Util.compare(ApiMaster.ship[a.api_ship_id].api_name, ApiMaster.ship[b.api_ship_id].api_name);
					if (ret != 0) return ret;

					return a.api_id - b.api_id;
				}
			);
			break;
		case "Lv・経験値(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_exp[0] - a.api_exp[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "Lv・経験値(昇順)":
			ret.sort(
				function (a, b)
				{
					var ret = a.api_exp[0] - b.api_exp[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "修復時間(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_ndock_time - a.api_ndock_time;
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "火力(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_karyoku[0] - a.api_karyoku[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "雷装(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_raisou[0] - a.api_raisou[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "対空(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_taiku[0] - a.api_taiku[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "対潜(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_taisen[0] - a.api_taisen[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "耐久(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_maxhp - a.api_maxhp;
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "装甲(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_soukou[0] - a.api_soukou[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "回避(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_kaihi[0] - a.api_kaihi[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "索敵(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_sakuteki[0] - a.api_sakuteki[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;
		case "運(降順)":
			ret.sort(
				function (a, b)
				{
					var ret = b.api_lucky[0] - a.api_lucky[0];
					if (ret != 0) return ret;
					return a.api_id - b.api_id;
				}
			);
			break;

		default:
			ret.sort(
				function (a, b)
				{
					return a.no - b.no;
				}
			);
			break;
	}

	return ret;
}

function checkFilterHealth(data)
{
	if ("" == p.filterHealth) return true;
	
	var damaged = false;
	var tired = false;
	var supply = false;

	damaged = (data.api_nowhp < data.api_maxhp);
	tired = data.api_cond < 49;
	supply = 
		(data.api_fuel < ApiMaster.ship[data.api_ship_id].api_fuel_max) ||
		(data.api_bull < ApiMaster.ship[data.api_ship_id].api_bull_max);

	switch (p.filterHealth)
	{
		case "正常":
			return !damaged && !tired && !supply;
		case "損傷":
			return damaged;
		case "疲労":
			return tired;
		case "消耗":
			return supply;
		case "異常":
			return damaged || tired || supply;
	}

	return trye;
}

function checkFilterPowerUp(data)
{
	if ("" == p.filterPowerUp) return true;

	var kyouka = Calc.calcKyouka(data);

	switch (p.filterPowerUp)
	{
		case "改修余地あり":
			return kyouka.karyoku.lack > 0
				|| kyouka.raisou.lack > 0
				|| kyouka.taiku.lack > 0
				|| kyouka.soukou.lack > 0;

		case "火力UP可能":
			return kyouka.karyoku.lack > 0
		case "雷装UP可能":
			return kyouka.raisou.lack > 0
		case "対空UP可能":
			return kyouka.taiku.lack > 0
		case "装甲UP可能":
			return kyouka.soukou.lack > 0
		case "改修MAX":
			return kyouka.karyoku.lack <= 0
				&& kyouka.raisou.lack <= 0
				&& kyouka.taiku.lack <= 0
				&& kyouka.soukou.lack <= 0;
	}

	return true;
}

function generateHtml()
{
	var shipList = selectShip();

	var html = "";
	html += "	<table id=\"ShipTable\"><tbody>";

	var rowNumber = 0;
	for(var ii = 0; ii < shipList.length; ii++)
	{
		if (
			(p.headerInterval == 0 && rowNumber == 0)
			|| (rowNumber % p.headerInterval) == 0)
		{
			html += generateThr();
		}
		html += generateTdr(rowNumber, shipList[ii]);
		rowNumber ++;
	}

	html += "	</tbody></table>";

	return html;
}

function generateThr()
{
	var html = "";

	html += "		<tr>";
	if (p.dispId)
	{
		html += "			<th rowspan=\"2\">No.</th>";
		html += "			<th rowspan=\"2\">ID</th>";
		html += "			<th rowspan=\"2\">Ship<br>ID</th>";
		html += "			<th rowspan=\"2\">Sort<br>No.</th>";
	}
	if (p.dispDeck)
	{
		html += "			<th rowspan=\"2\">艦隊</th>";
		html += "			<th rowspan=\"2\">艦番</th>";
	}
	if (p.dispSally)
	{
		html += "			<th rowspan=\"2\">作戦</th>";
	}
	if (p.dispLockStatus)
	{
		html += "			<th rowspan=\"2\">鍵</th>";
	}
	if (p.dispYomi)
	{
		html += "			<th rowspan=\"2\" colspan=\"2\">艦名</th>";
	}
	else
	{
		html += "			<th rowspan=\"2\">艦名</th>";
	}
	if (p.dispType)
	{
//		html += "			<th rowspan=\"2\">型名</th>";
		html += "			<th rowspan=\"2\">艦種</th>";
	}
	if (p.dispLv)
	{
		html += "			<th rowspan=\"2\">Lv</th>";
		html += "			<th colspan=\"3\">経験値</th>";
	}
	if (p.dispCondition)
	{
		html += "			<th rowspan=\"2\">調子</th>";
	}
	if (p.dispFuel)
	{
		html += "			<th colspan=\"2\">燃料</th>";
		html += "			<th colspan=\"2\">弾薬</th>";
		html += "			<th colspan=\"2\">耐久</th>";
	}
	if (p.dispSpec)
	{
		html += "			<th colspan=\"2\">装備</th>";
		html += "			<th rowspan=\"2\">装甲</th>";
		html += "			<th rowspan=\"2\">回避</th>";
		html += "			<th rowspan=\"2\">火力</th>";
		html += "			<th rowspan=\"2\">雷装</th>";
		html += "			<th rowspan=\"2\">対空</th>";
		html += "			<th rowspan=\"2\">対潜</th>";
		html += "			<th rowspan=\"2\">索敵</th>";
		html += "			<th rowspan=\"2\">運</th>";
	}
	if (p.dispPowerUp)
	{
		html += "			<th colspan=\"3\">強化火</th>";
		html += "			<th colspan=\"3\">強化雷</th>";
		html += "			<th colspan=\"3\">強化空</th>";
		html += "			<th colspan=\"3\">強化装</th>";
	}
	if (p.dispUpGrade)
	{
		html += "			<th colspan=\"2\">改造</th>";
	}
	if (p.dispRepairTime)
	{
		html += "			<th rowspan=\"2\">修復時間</th>";
	}
	html += "		</tr>";

	html += "		<tr>";
	if (p.dispLv)
	{
		html += "			<th>現在</th>";// 経験値";
		html += "			<th>次</th>";
		html += "			<th>%</th>";
	}
	if (p.dispFuel)
	{
		html += "			<th>現在</th>";// 燃料";
		html += "			<th>最大</th>";
		html += "			<th>現在</th>";// 弾薬";
		html += "			<th>最大</th>";
		html += "			<th>現在</th>";// 耐久";
		html += "			<th>最大</th>";
	}
	if (p.dispSpec)
	{
		html += "			<th>装着</th>";// 装備";
		html += "			<th>ｽﾛｯﾄ</th>";
	}
	if (p.dispPowerUp)
	{
		html += "			<th>現在</th>";// 強化火";
		html += "			<th>最大</th>";
		html += "			<th>残り</th>";
		html += "			<th>現在</th>";// 強化雷";
		html += "			<th>最大</th>";
		html += "			<th>残り</th>";
		html += "			<th>現在</th>";// 強化空";
		html += "			<th>最大</th>";
		html += "			<th>残り</th>";
		html += "			<th>現在</th>";// 強化装";
		html += "			<th>最大</th>";
		html += "			<th>残り</th>";
	}
	if (p.dispUpGrade)
	{
		html += "			<th>Lv</th>";
		html += "			<th>艦名</th>";
	}
	html += "		</tr>";

	return html;
}

function generateTdr(number, data)
{
	var html = "";

	var deckClass = "";
	if (ApiMember.shipInDeck.data[data.api_id])
	{
		deckClass = ["第一艦隊", "第二艦隊", "第三艦隊", "第四艦隊"][ApiMember.shipInDeck.data[data.api_id].deckNo];
	}

	html += "		<tr class=\"" + deckClass + "\" onmouseover=\"RowCursor.hover(this)\" onmouseout=\"RowCursor.leave(this)\">";
	if (p.dispId)
	{
		html += "			<td class=\"number\">" + (number + 1) + "</td>";
		html += "			<td class=\"number\">" + data.api_id + "</td>";
		html += "			<td class=\"number\">" + data.api_ship_id + "</td>";
		html += "			<td class=\"number\">" + data.api_sortno + "</td>";
	}

	if (p.dispDeck)
	{
		if (ApiMember.shipInDeck.data[data.api_id])
		{
			var shipNo = ["旗", 2, 3, 4, 5, 6][ApiMember.shipInDeck.data[data.api_id].shipNo];

			html += "			<td>" + ApiMember.shipInDeck.data[data.api_id].deckName + "</td>";
			html += "			<td class=\"center\">" + shipNo + "</td>";
		}
		else
		{
			html += "			<td></td>";
			html += "			<td></td>";
		}
	}

	if (p.dispSally)
	{
		if (data.api_sally_area)
		{
			html += "			<td class=\"center monotype\">#" + data.api_sally_area + "</td>";
		}
		else
		{
			html += "			<td></td>";
		}
	}

	if (p.dispLockStatus)
	{
		html += "			<td class=\"center\">" + (data.api_locked ? "✓": "") + "</td>";
	}
	html += "			<td>" + ApiMaster.ship[data.api_ship_id].api_name + "</td>";
	if (p.dispYomi)
	{
		html += "			<td>" + ApiMaster.ship[data.api_ship_id].api_yomi + "</td>";
	}
	if (p.dispType)
	{
/*
		if (localData.CType[ApiMaster.ship[data.api_ship_id].api_ctype])
		{
			html += "			<td>" + localData.CType[ApiMaster.ship[data.api_ship_id].api_ctype] + "</td>";
		}
		else
		{
			html += "			<td> #" + ApiMaster.ship[data.api_ship_id].api_ctype + "</td>";
		}
*/
		html += "			<td>" + ApiMaster.sType[ApiMaster.ship[data.api_ship_id].api_stype].api_name + "</td>";
	}
	if (p.dispLv)
	{
		html += "			<td class=\"number\">" + data.api_lv + "</td>";
		html += "			<td class=\"number\">" + data.api_exp[0] + "</td>";
		html += "			<td class=\"number\">" + data.api_exp[1] + "</td>";
		html += "			<td class=\"number\">" + data.api_exp[2] + "</td>";
	}
	if (p.dispCondition)
	{
		var condClass = Calc.calcTired(data);
		html += "			<td class=\"number " + condClass + "\">" + data.api_cond + "</td>";
	}
	if (p.dispFuel)
	{
		var damageClass = Calc.calcDamage(data);
		var fuelClass = (data.api_fuel < ApiMaster.ship[data.api_ship_id].api_fuel_max) ? "補給" : "";
		var bullClass = (data.api_bull < ApiMaster.ship[data.api_ship_id].api_bull_max) ? "補給" : "";
		html += "			<td class=\"number " + fuelClass + "\">" + data.api_fuel + "</td>";
		html += "			<td class=\"number\">" + ApiMaster.ship[data.api_ship_id].api_fuel_max + "</td>";
		html += "			<td class=\"number " + bullClass + "\">" + data.api_bull + "</td>";
		html += "			<td class=\"number\">" + ApiMaster.ship[data.api_ship_id].api_bull_max + "</td>";
		html += "			<td class=\"number " + damageClass + "\">" + data.api_nowhp + "</td>";
		html += "			<td class=\"number\">" + data.api_maxhp + "</td>";
	}
	if (p.dispSpec)
	{
		//装備
		var slot = Calc.countSlot(data);
		html += "			<td class=\"number\">" + slot.attached + "</td>";
		html += "			<td class=\"number\">" + slot.total + "</td>";

		//装甲
		html += "			<td class=\"number\">" + data.api_soukou[0] + "</td>";
		//回避
		html += "			<td class=\"number\">" + data.api_kaihi[0] + "</td>";
		//火力
		html += "			<td class=\"number\">" + data.api_karyoku[0] + "</td>";
		//雷装
		html += "			<td class=\"number\">" + data.api_raisou[0] + "</td>";
		//対空
		html += "			<td class=\"number\">" + data.api_taiku[0] + "</td>";
		//対潜
		html += "			<td class=\"number\">" + data.api_taisen[0] + "</td>";
		//索敵
		html += "			<td class=\"number\">" + data.api_sakuteki[0] + "</td>";
		//運
		html += "			<td class=\"number\">" + data.api_lucky[0] + "</td>";
	}
	if (p.dispPowerUp)
	{
		var kyouka = Calc.calcKyouka(data);

		var kyoukaClass = "";

		// 強化火"
		if (kyouka.karyoku.lack) kyoukaClass = "";
		else kyoukaClass = "改修MAX";
		html += "			<td class=\"number " + "" + "\">" + kyouka.karyoku.now + "</td>";
		html += "			<td class=\"number " + "" + "\">" + kyouka.karyoku.max + "</td>";
		html += "			<td class=\"number " + kyoukaClass + "\">" + kyouka.karyoku.lack + "</td>";
		
		// 強化雷"
		if (kyouka.raisou.lack) kyoukaClass = "";
		else kyoukaClass = "改修MAX";
		html += "			<td class=\"number " + "" + "\">" + kyouka.raisou.now + "</td>";
		html += "			<td class=\"number " + "" + "\">" + kyouka.raisou.max + "</td>";
		html += "			<td class=\"number " + kyoukaClass + "\">" + kyouka.raisou.lack + "</td>";
		
		// 強化空"
		if (kyouka.taiku.lack) kyoukaClass = "";
		else kyoukaClass = "改修MAX";
		html += "			<td class=\"number " + "" + "\">" + kyouka.taiku.now + "</td>";
		html += "			<td class=\"number " + "" + "\">" + kyouka.taiku.max + "</td>";
		html += "			<td class=\"number " + kyoukaClass + "\">" + kyouka.taiku.lack + "</td>";
		
		// 強化装"
		if (kyouka.soukou.lack) kyoukaClass = "";
		else kyoukaClass = "改修MAX";
		html += "			<td class=\"number " + "" + "\">" + kyouka.soukou.now + "</td>";
		html += "			<td class=\"number " + "" + "\">" + kyouka.soukou.max + "</td>";
		html += "			<td class=\"number " + kyoukaClass + "\">" + kyouka.soukou.lack + "</td>";
		
	}
	if (p.dispUpGrade)
	{
		var afterShip = Calc.checkAfterShip(data);

		var upgradeClass = afterShip.upgradeOk ? "改造可" : "";

		if (afterShip.level)
		{
			html += "			<td class=\"number " + upgradeClass + "\">" + afterShip.level + "</td>";
			html += "			<td class=\"" + upgradeClass + "\">" + afterShip.ship.api_name + "</td>";
		}
		else
		{
			html += "			<td class=\"number\"></td>";
			html += "			<td class=\"number\"></td>";
		}

	}
	if (p.dispRepairTime)
	{
		if (data.api_ndock_time)
		{
			html += "			<td class=\"number\">" + Util.millSecondsToTimeString(data.api_ndock_time) + "</td>";
		}
		else
		{
			html += "			<td class=\"number\"></td>";
		}
	}

	html += "		</tr>";

	return html;
}