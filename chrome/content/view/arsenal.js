Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};

var lastUpdateTime = 0;

var f = {
	initialize: function()
	{
		setInterval(function() { f.update(); }, 500);
	}
	,
	finalize: function()
	{
		windowSetings.save();
	}
	,
	update: function()
	{
		if (!f.checkUpdated()) return;

		lastUpdateTime = new Date();

		document.getElementById("contents").innerHTML = generateHtml();

		document.title = title + " 最終更新日時: " + Util.dateTimeToString(lastUpdateTime);
	}
	,
	checkUpdated: function()
	{
		var ret = false;

		ret = 
			(
				ApiMaster.updateTime
				&& ApiMember.material.updateTime
			)
			&&
			(
				// 初回更新
				(! lastUpdateTime)

				// APIデータが更新されたとき
				|| (lastUpdateTime < ApiMember.material.updateTime)
			);

		return ret;
	}
}


function generateHtml()
{
	var html = "";

	html += "	<table><tbody>";

	html += "		<tr>";
	html += "			<th>資材</th>";
	html += "			<th>備蓄量</th>";
	html += "			<th>資材</th>";
	html += "			<th>備蓄量</th>";
	html += "		</tr>";

	html += "		<tr class=\"oddRow\">";
	html += "			<td class=\"head\">燃料</td>";
	html += "			<td class=\"number\">" + ApiMember.material.data[0].api_value + "</td>";
	html += "			<td class=\"head\">鋼材</td>";
	html += "			<td class=\"number\">" + ApiMember.material.data[2].api_value + "</td>";
	html += "		</tr>";

	html += "		<tr class=\"evenRow\">";
	html += "			<td class=\"head\">弾薬</td>";
	html += "			<td class=\"number\">" + ApiMember.material.data[1].api_value + "</td>";
	html += "			<td class=\"head\">ボーキサイト</td>";
	html += "			<td class=\"number\">" + ApiMember.material.data[3].api_value + "</td>";
	html += "		</tr>";

	html += "		<tr class=\"oddRow\">";
	html += "			<td class=\"head\">高速建造材</td>";
	html += "			<td class=\"number\">" + ApiMember.material.data[4].api_value + "</td>";
	html += "			<td class=\"head\">高速修復材</td>";
	html += "			<td class=\"number\">" + ApiMember.material.data[5].api_value + "</td>";
	html += "		</tr>";

	html += "		<tr class=\"evenRow\">";
	html += "			<td class=\"head\">開発資材</td>";
	html += "			<td class=\"number\">" + ApiMember.material.data[6].api_value + "</td>";
	html += "			<td class=\"head\">改装資材</td>";
	html += "			<td class=\"number\">" + ApiMember.material.data[7].api_value + "</td>";
	html += "		</tr>";

	html += "	</tbody></table>";

	return html;
}