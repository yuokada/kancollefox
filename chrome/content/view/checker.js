Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};

var p = {};

var lastUpdateTime = 0;

var f = {
	initialize: function()
	{
		if (KancolleFoxConfig.loadBoolParameter(window.name, "configured"))
		{
			document.getElementById("DispSupply").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.supply");
			document.getElementById("DispFormation").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.formation");
			document.getElementById("DispDescription").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.description");
		}

		f.initParameters();

		setInterval(function() { f.update(); }, 500);
	}
	,
	finalize: function()
	{
		windowSetings.save();
		KancolleFoxConfig.saveBoolParameter(window.name, "configured", true);
		KancolleFoxConfig.saveBoolParameter(window.name, "display.supply", Util.getChecked("DispSupply"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.formation", Util.getChecked("DispFormation"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.description", Util.getChecked("DispDescription"));
	}
	,
	update: function()
	{
		if (!f.checkUpdated()) return;

		lastUpdateTime = new Date();

		document.getElementById("contents").innerHTML = generateHtml();

		document.title = title + " 最終更新日時: " + Util.dateTimeToString(lastUpdateTime);
	}
	,
	initParameters: function()
	{
		p = {
			dispSupply : "",
			dispFormation : "",
			dispDescription : ""
		};
	}
	,
	checkUpdated: function()
	{
		var ret = false;

		var dispSupply			= Util.getChecked("DispSupply");
		var dispFormation		= Util.getChecked("DispFormation");
		var dispDescription	= Util.getChecked("DispDescription");

		ret = 
			(
				ApiMaster.updateTime
				&& ApiMember.updateTime
				&& ApiMember.deck.updateTime
				&& ApiMember.slotItem.updateTime
			)
			&&
			(
				// 初回更新
				(! lastUpdateTime)

				// APIデータが更新されたとき
				|| (lastUpdateTime < ApiMember.ship.updateTime)
				|| (lastUpdateTime < ApiMember.deck.updateTime)

				// 表示項目の変更
				|| (p.dispSupply != dispSupply)
				|| (p.dispFormation != dispFormation)
				|| (p.dispDescription != dispDescription)
			);

		if (ret)
		{
			p = {
				dispSupply : dispSupply,
				dispFormation : dispFormation,
				dispDescription : dispDescription
			};
		}

		return ret;
	}
};

function createData()
{
	var data = [];

	for (var id in localData.Expedition)
	{
		var expedition = {};
		expedition.id = id;
		expedition.master = ApiMaster.mission[id];
		expedition.deck = [];

		for (var ii = 0; ii < ApiMember.basic.data.api_count_deck; ii++)
		{
			expedition.deck.push({});

			expedition.deck[ii]["編成"] = {};
			expedition.deck[ii]["編成"]["駆逐"] = -localData.Expedition[id]["編成"]["駆逐"];
			expedition.deck[ii]["編成"]["軽巡"] = -localData.Expedition[id]["編成"]["軽巡"];
			expedition.deck[ii]["編成"]["重巡"] = -localData.Expedition[id]["編成"]["重巡"];
			expedition.deck[ii]["編成"]["雷巡"] = -localData.Expedition[id]["編成"]["雷巡"];
			expedition.deck[ii]["編成"]["戦艦"] = -localData.Expedition[id]["編成"]["戦艦"];
			expedition.deck[ii]["編成"]["航巡"] = -localData.Expedition[id]["編成"]["航巡"];
			expedition.deck[ii]["編成"]["航戦"] = -localData.Expedition[id]["編成"]["航戦"];
			expedition.deck[ii]["編成"]["潜水"] = -localData.Expedition[id]["編成"]["潜水"];
			expedition.deck[ii]["編成"]["潜空"] = -localData.Expedition[id]["編成"]["潜空"];
			expedition.deck[ii]["編成"]["潜母"] = -localData.Expedition[id]["編成"]["潜母"];
			expedition.deck[ii]["編成"]["水母"] = -localData.Expedition[id]["編成"]["水母"];
			expedition.deck[ii]["編成"]["軽空"] = -localData.Expedition[id]["編成"]["軽空"];
			expedition.deck[ii]["編成"]["正空"] = -localData.Expedition[id]["編成"]["正空"];
			expedition.deck[ii]["編成"]["空系"] = -localData.Expedition[id]["編成"]["空系"];
			expedition.deck[ii]["編成"]["総数"] = -localData.Expedition[id]["編成"]["総数"];

			expedition.deck[ii]["旗Lv"] = -localData.Expedition[id]["旗Lv"];
			expedition.deck[ii]["Lv計"] = -localData.Expedition[id]["Lv計"];
			expedition.deck[ii]["ド艦"] = -localData.Expedition[id]["ド艦"];
			expedition.deck[ii]["ド数"] = -localData.Expedition[id]["ド数"];

			for (var jj = 0; jj < ApiMember.deck.data[ii].api_ship.length; jj ++)
			{
				if (ApiMember.deck.data[ii].api_ship[jj] == -1) continue;

				var ship = ApiMember.ship.data[ApiMember.deck.data[ii].api_ship[jj]];

				var stype = ApiMaster.ship[ship.api_ship_id].api_stype;
				var snameList = [
					"", 	"海防", "駆逐", "軽巡", "雷巡", "重巡", "航巡", "軽空", "戦艦", "戦艦",
					"航戦", "正空", "弩戦", "潜水", "潜空", "補給", "水母", "揚陸", "装空", "工作", "潜母"];

				var sname = "";
				if (stype < snameList.length)
				{
					sname = snameList[stype];

					if (sname != "" && expedition.deck[ii]["編成"][sname] != undefined)
						expedition.deck[ii]["編成"][sname] ++;

					// 水母、軽空、正空は空母系としてもカウントします。
					if (sname == "水母")
						expedition.deck[ii]["編成"]["空系"] ++;
					if (sname == "軽空")
						expedition.deck[ii]["編成"]["空系"] ++;
					if (sname == "正空")
						expedition.deck[ii]["編成"]["空系"] ++;

					// 潜空は潜水艦としてもカウントします。
					if (sname == "潜空")
						expedition.deck[ii]["編成"]["潜水"] ++;
				}

				// 総艦隊数
				expedition.deck[ii]["編成"]["総数"] ++;

				// 旗艦レベル
				if (jj == 0)
					expedition.deck[ii]["旗Lv"] += ship.api_lv;

				// 艦隊総レベル
				expedition.deck[ii]["Lv計"] += ship.api_lv;

				// 艦載装備からドラム缶を検索します。
				var found = false;
				for (var kk = 0; kk < ship.api_slot.length; kk++)
				{
					if (ship.api_slot[kk] == -1) continue;

					if (ApiMember.slotItem.data[ship.api_slot[kk]].api_slotitem_id == 75)
					{
						// ドラム缶艦載数
						expedition.deck[ii]["ド数"] ++;

						// ドラム缶搭載艦数
						if (!found)
						{
							expedition.deck[ii]["ド艦"] ++;
							found = true;
						}
					}
				}
			}
		}

		data.push(expedition);
	}

	return data;
}

function generateHtml()
{
	data = createData();
	var lastArea = "";

	var html = "";
	html += "	<table><tbody>";
	for (var ii = 0; ii < data.length; ii++)
	{
		var area = ApiMaster.mapArea[data[ii].master.api_maparea_id].api_name;

		if (lastArea != area)
		{
			html += generateHeader(area);
			lastArea = area;
		}
		html += generateRow(data[ii]);
	}
	html += "	</tbody></table>";

	return html;
}

function generateHeader(area)
{
	var html = "";

	html += "		<tr>";
	html += "			<th rowspan=\"" + (p.dispDescription ? 3 : 2) + "\">ID</th>";
	html += "			<th colspan=\"2\">" + area + "</th>";
	if (p.dispSupply)
	{
		html += "			<th colspan=\"2\">消費資材</th>";
	}
	if (p.dispFormation)
	{
		html += "			<th>駆逐</th>";
		html += "			<th>軽巡</th>";
		html += "			<th>重巡</th>";
		html += "			<th>雷巡</th>";
		html += "			<th>戦艦</th>";
		html += "			<th>航巡</th>";
		html += "			<th>航戦</th>";
		html += "			<th>潜水</th>";
		html += "			<th>潜空</th>";
		html += "			<th>潜母</th>";
	}
	for (var ii = 1; ii < ApiMember.basic.data.api_count_deck; ii++)
	{
		html += "			<th rowspan=\"2\">" + ApiMember.deck.data[ii].api_name+ "</th>";
	}
	html += "		</tr>";
	html += "		<tr>";
	html += "			<th>難度</th>";
	html += "			<th>遠征時間</th>";

	if (p.dispSupply)
	{
		html += "			<th>燃</th>";
		html += "			<th>弾</th>";
	}

	if (p.dispFormation)
	{
		html += "			<th>水母</th>";
		html += "			<th>軽空</th>";
		html += "			<th>正空</th>";
		html += "			<th>空系</th>";
		html += "			<th>総数</th>";

		html += "			<th>旗Lv</th>";
		html += "			<th>Lv計</th>";
		html += "			<th>ド艦</th>";
		html += "			<th>ド数</th>";
		html += "			<th></th>";
	}
	html += "		</tr>";
	if (p.dispDescription)
	{
		html += "		<tr>";
		html += "			<th colspan=\""
			+
			(
				5 +
				(p.dispSupply ? 2 : 0) +
				(p.dispFormation ? 10 : 0)
			)
			+ "\">内容</th>";
		html += "		</tr>";
	}

	return html;
}

function generateRow(data)
{
	var html = "";

	html += "		<tr onmouseover=\"RowCursor.hover(this)\" onmouseout=\"RowCursor.leave(this)\">";
	html += "			<td rowspan=\"" + (p.dispDescription ? 3 : 2) + "\">" + data.id + "</td>";
	html += "			<td colspan=\"2\">" + data.master.api_name + "</td>";
	if (p.dispSupply)
	{
		html += "			<td rowspan=\"2\" class=\"number\">" + (data.master.api_use_fuel * 100 + "%") + "</td>";
		html += "			<td rowspan=\"2\" class=\"number\">" + (data.master.api_use_bull * 100 + "%") + "</td>";
	}
	if (p.dispFormation)
	{
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["駆逐"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["軽巡"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["重巡"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["雷巡"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["戦艦"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["航巡"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["航戦"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["潜水"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["潜空"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["潜母"] + "</td>";
	}

	for (var ii = 1; ii < ApiMember.basic.data.api_count_deck; ii++)
	{
		var judge = [];

		if (data.deck[ii]["編成"]["駆逐"] < 0)
			judge.push("駆逐 " + data.deck[ii]["編成"]["駆逐"]);

		if (data.deck[ii]["編成"]["軽巡"] < 0)
			judge.push("軽巡 " + data.deck[ii]["編成"]["軽巡"]);

		if (data.deck[ii]["編成"]["重巡"] < 0)
			judge.push("重巡 " + data.deck[ii]["編成"]["重巡"]);

		if (data.deck[ii]["編成"]["雷巡"] < 0)
			judge.push("雷巡 " + data.deck[ii]["編成"]["雷巡"]);

		if (data.deck[ii]["編成"]["戦艦"] < 0)
			judge.push("戦艦 " + data.deck[ii]["編成"]["戦艦"]);

		if (data.deck[ii]["編成"]["航巡"] < 0)
			judge.push("航巡 " + data.deck[ii]["編成"]["航巡"]);

		if (data.deck[ii]["編成"]["航戦"] < 0)
			judge.push("航戦 " + data.deck[ii]["編成"]["航戦"]);

		if (data.deck[ii]["編成"]["潜水"] < 0)
			judge.push("潜水 " + data.deck[ii]["編成"]["潜水"]);

		if (data.deck[ii]["編成"]["潜空"] < 0)
			judge.push("潜空 " + data.deck[ii]["編成"]["潜空"]);

		if (data.deck[ii]["編成"]["潜母"] < 0)
			judge.push("潜母 " + data.deck[ii]["編成"]["潜母"]);

		if (data.deck[ii]["編成"]["水母"] < 0)
			judge.push("水母 " + data.deck[ii]["編成"]["水母"]);

		if (data.deck[ii]["編成"]["軽空"] < 0)
			judge.push("軽空 " + data.deck[ii]["編成"]["軽空"]);

		if (data.deck[ii]["編成"]["正空"] < 0)
			judge.push("正空 " + data.deck[ii]["編成"]["正空"]);

		if (data.deck[ii]["編成"]["空系"] < 0)
			judge.push("空系 " + data.deck[ii]["編成"]["空系"]);

		if (data.deck[ii]["編成"]["総数"] < 0)
			judge.push("総数 " + data.deck[ii]["編成"]["総数"]);

		if (data.deck[ii]["旗Lv"] < 0)
			judge.push("旗艦Lv " + data.deck[ii]["旗Lv"]);

		if (data.deck[ii]["Lv計"] < 0)
			judge.push("総Lv " + data.deck[ii]["Lv計"]);

		if (data.deck[ii]["ド艦"] < 0)
			judge.push("ﾄﾞﾗﾑ搭載艦 " + data.deck[ii]["ド艦"]);

		if (data.deck[ii]["ド数"] < 0)
			judge.push("ﾄﾞﾗﾑ艦載数 " + data.deck[ii]["ド数"]);

		html += "			<td rowspan=\"2\" class=\"judge\">" + judge.join("<br>") + "</td>";
	}

	html += "		</tr>";
	html += "		<tr onmouseover=\"RowCursor.hover(this)\" onmouseout=\"RowCursor.leave(this)\">";
	html += "			<td class=\"center\">" + data.master.api_difficulty + "</td>";
	html += "			<td class=\"number\">" + Util.minutesToTime(data.master.api_time) + "</td>";

	if (p.dispFormation)
	{
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["水母"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["軽空"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["正空"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["空系"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["編成"]["総数"] + "</td>";

		html += "			<td class=\"number\">" + localData.Expedition[data.id]["旗Lv"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["Lv計"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["ド艦"] + "</td>";
		html += "			<td class=\"number\">" + localData.Expedition[data.id]["ド数"] + "</td>";
		html += "			<td class=\"number\"></td>";
	}
	html += "		</tr>";
	if (p.dispDescription)
	{
		html += "		<tr>";
		html += "			<td colspan=\""
				+
				(
					5 +
					(p.dispSupply ? 2 : 0) +
					(p.dispFormation ? 10 : 0)
				)
				+ "\"><div class=\"description\">" + data.master.api_details + "</div></td>";
		html += "		</tr>";
	}


	return html;
}
