Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};

var lastUpdateTime = 0;

var graphData = {};
var formation = [];

var battleMap = ""
var battleArea = "";
var barWidth = 80;


var f = {
	initialize: function()
	{
		setInterval(function() { f.update(); }, 500);
	}
	,
	finalize: function()
	{
		windowSetings.save();
	}
	,
	update: function()
	{
		if (!f.checkUpdated()) return;

		lastUpdateTime = new Date();
		try{
			document.getElementById("contents").innerHTML = generateHtml();
		}
		catch (e)
		{
			document.getElementById("contents").innerHTML = "";
			alert("例外が発生しました。\n" + e.toString() + "\n\n" + e.stack);
		}

		document.title = title + " 最終更新日時: " + Util.dateTimeToString(lastUpdateTime);
	}
	,
	checkUpdated: function()
	{
		var ret = false;

		ret = 
			(
				ApiMaster.updateTime
				&& ApiMember.battleLog.updateTime
			)
			&&
			(
				// 初回更新
				(! lastUpdateTime)

				// APIデータが更新されたとき
				|| (lastUpdateTime < ApiMember.battleLog.updateTime)
			);

		return ret;
	}
};

function generateHtml()
{
	var html = "";

	if (! ApiMember.battleLog || ! ApiMember.battleLog.data || ApiMember.battleLog.data.length == 0)
	{
		return html;
	}

	graphData = {};
	var combined = false;

	for (var ii = 0; ii < ApiMember.battleLog.data.length; ii++)
	{
		var data = ApiMember.battleLog.data[ii];

		formation = data.formation;

		if (ApiMember.battleLog.start.api_maparea_id)
		{
			battleMap = getMapAreaName(ApiMember.battleLog.start.api_maparea_id);
			battleArea = getMapInfoName(ApiMember.battleLog.start.api_mapinfo_no);
		}

		switch (data.LogType)
		{
			case "battle":
				createBattleData(data);
				break;
			case "result":
				createResultData(data);
				break;

			case "battleCombined":
			case "battleCombinedAir":
			case "battleCombinedMidnight":
			case "battleCombinedSpMidnight":
			case "battleCombinedWater":
				combined = true;
				createBattleCombinedData(data);
				break;
			case "resultCombined":
				combined = true;
				createResultCombinedData(data);
				break;
		}
	}

	if (combined)
	{
		html += createCombinedLog();
	}
	else
	{
		html += createLog();
	}

	return html;
}

function createBattleData(data)
{
	graphData = {
		battleField:
		{
			area: battleArea,
			map:  battleMap
		},
		f:
		{
			name: data.deck.api_name,
			ship: []
		},
		e:
		{
			name: "敵艦隊",
			ship: []
		},
		formation:
		{
			f: getFormationName(formation[0]),
			e: getFormationName(formation[1]),
			i: getInterceptName(formation[2])
		},
		winRank: "",
		mvp: null,
		getShip:
		{
			id: "",
			stype: "",
			sname: "",
			getmes: ""
		}
	};

	// 自軍艦名・レベル
	for (var ii = 0; ii < data.deck.api_ship.length; ii++)
	{
		var id = data.deck.api_ship[ii];
		if (id == -1) continue;
		graphData.f.ship.push({
			name: ApiMaster.ship[ApiMember.ship.data[id].api_ship_id].api_name,
			lv: ApiMember.ship.data[id].api_lv
		});
	}
	// 自軍耐久初期値
	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_maxhps[ii + 1] != -1)
		{
			graphData.f.ship[ii].startHP = data.api_nowhps[ii + 1];
			graphData.f.ship[ii].nowHP = data.api_nowhps[ii + 1];
			graphData.f.ship[ii].maxHP = data.api_maxhps[ii + 1];
		}
	}

	// 敵軍艦名
	for (var ii = 0; ii < data.api_ship_ke.length; ii++)
	{
		var id = data.api_ship_ke[ii];
		if (id != -1)
		{
			graphData.e.ship.push({
				name: ApiMaster.enemy[id].api_name,
				class: ApiMaster.enemy[id].api_yomi
			});
		}
	}

	// 敵軍耐久初期値
	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_maxhps[ii + 7] != -1)
		{
			graphData.e.ship[ii].startHP = data.api_nowhps[ii + 7];
			graphData.e.ship[ii].nowHP = data.api_nowhps[ii + 7];
			graphData.e.ship[ii].maxHP = data.api_maxhps[ii + 7];
		}
	}

	// 開幕航空戦
	if (data.api_stage_flag != undefined && data.api_stage_flag[2] == 1)
	{
		BattleCalc.calcKoukuStage3(data.api_kouku.api_stage3);
	}

	// 支援艦攻撃
	if (data.api_support_flag && data.api_support_flag != -1)
	{
		BattleCalc.calcSupportAttack(data.api_support_info);
	}

	// 開幕雷撃戦
	if (data.api_opening_flag == 1)
	{
		BattleCalc.calcOpeningAttack(data.api_opening_atack);
	}

	// 砲撃戦
	if (data.api_hourai_flag != undefined && data.api_hourai_flag[0] == 1)
	{
		BattleCalc.calcHougeki(data.api_hougeki1);
	}
	if (data.api_hourai_flag != undefined && data.api_hourai_flag[1] == 1)
	{
		BattleCalc.calcHougeki(data.api_hougeki2);
	}
	if (data.api_hourai_flag != undefined && data.api_hourai_flag[2] == 1)
	{
		BattleCalc.calcHougeki(data.api_hougeki3);
	}
	// 砲撃戦(夜戦)
	if (data.api_hougeki != null)
	{
		BattleCalc.calcHougekiYasen(data.api_hougeki);
	}

	// 雷撃戦
	if (data.api_hourai_flag != undefined && data.api_hourai_flag[3] == 1)
	{
		BattleCalc.calcRaigeki(data.api_raigeki);
	}
}

function createResultData(data)
{
	graphData.mvp = data.api_mvp;

	graphData.e.name = data.api_enemy_info.api_deck_name;

	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_get_ship_exp[ii + 1] != -1)
		{
			graphData.f.ship[ii].getExp = data.api_get_ship_exp[ii + 1];
			graphData.f.ship[ii].nowExp = data.api_get_exp_lvup[ii][0];
			graphData.f.ship[ii].nextExp = data.api_get_exp_lvup[ii][1];
		}
	}

	if (data.api_get_ship)
	{
		graphData.getShip.id	= data.api_get_ship.api_ship_id;
		graphData.getShip.stype = data.api_get_ship.api_ship_type;
		graphData.getShip.sname = data.api_get_ship.api_ship_name;
		graphData.getShip.getmes = data.api_get_ship.api_ship_getmes;

	}

	switch (data.api_win_rank)
	{
		case "S":
			graphData.winRank = " <span style=\"color:#ffff00;\">S </span> ";
			break;
		case "A":
			graphData.winRank = " <span style=\"color:#ff0000;\">A </span> ";
			break;
		case "B":
			graphData.winRank = " <span style=\"color:#ff8000;\">B </span> ";
			break;
		case "C":
			graphData.winRank = " <span style=\"color:#ffff00;\">C </span> ";
			break;
		case "D":
			graphData.winRank = " <span style=\"color:#00ffff;\">D </span> ";
			break;
		case "E":
			graphData.winRank = " <span style=\"color:#0000ff;\">E </span> ";
			break;
	}
}

function createLog()
{
	var map = battleMap;
	var area = battleArea;

	// var haveShip = "";

	var getShipInfo = "";
	if (graphData.getShip.id)
	{
		var getShip = ApiMaster.ship[graphData.getShip.id];

		// haveShip = checkHave(getShip);

		getShipInfo += graphData.getShip.sname;
//		getShipInfo += " - " + localData.CType[getShip.api_ctype];
//		getShipInfo += " " + getShip.api_cnum + "番艦";
		getShipInfo += " " + graphData.getShip.stype;
		// getShipInfo += " " + haveShip;
	}

	var html = "";

	html +="	<table><tbody>";
	html +="		<tr>";
	html +="			<th colspan=\"8\" class=\"center\">" + area + " - " + map + "</th>";
	html +="		</tr>";
	html +="		<tr>";
	html +="			<th colspan=\"5\">" + graphData.winRank + graphData.f.name + "</th>";
	html +="			<th colspan=\"3\">" + graphData.e.name + "</th>";
	html +="		</tr>";
	html +="		<tr>";
	html +="			<th>艦名</th>";
	html +="			<th colspan=\"2\">経験値</th>";
	html +="			<th colspan=\"4\">"
							+ graphData.formation.f
							+ " x "
							+ graphData.formation.i
							+ " x "
							+ graphData.formation.e + "</th>";
	html +="			<th>艦名</th>";
	html +="		</tr>";

	for (var ii = 0; ii < 6; ii++)
	{
		var f =
		{
			name: "",
			lv: "",
			state: "",
			stateText: "",
			mvp: "",

			maxHp: "",
			nowHp: "",
			startHp: "",

			maxBar: 0,
			nowBar: 0,
			startBar: 0,

			getExp: "",
			nowExp: "",
			nextExp: ""
		};
		var e =
		{
			name: "",
			class: "",
			state: "",
			stateText: "",

			maxHp: "",
			nowHp: "",
			startHp: "",

			maxBar: 0,
			nowBar: 0,
			startBar: 0
		};

		if (graphData.f.ship.length > ii)
		{
			f.name = graphData.f.ship[ii].name;
			f.lv = "Lv." + graphData.f.ship[ii].lv;
			f.state = calcState(graphData.f.ship[ii]);
			f.stateText = "(" + f.state + ")";

			f.maxHp = graphData.f.ship[ii].maxHP;
			f.nowHp = graphData.f.ship[ii].nowHP;
			f.startHp = graphData.f.ship[ii].startHP;

			f.maxBar = barWidth;
			f.nowBar = barWidth * f.nowHp / f.maxHp;
			f.startBar = barWidth * f.startHp / f.maxHp;

			if (graphData.f.ship[ii].getExp)
			{
				f.getExp = graphData.f.ship[ii].getExp;
				f.nowExp = graphData.f.ship[ii].nowExp + graphData.f.ship[ii].getExp;
				f.nextExp = graphData.f.ship[ii].nextExp;
			}

			if (graphData.mvp == ii + 1) f.mvp = "<span style=\"color: #ffff00;\">★</span>";
			else f.mvp = "";
		}

		if (graphData.e.ship.length > ii)
		{
			e.name = graphData.e.ship[ii].name;
			e.class = graphData.e.ship[ii].class;
			e.state = calcState(graphData.e.ship[ii]);
			e.stateText = "(" + e.state + ")";

			e.maxHp = graphData.e.ship[ii].maxHP;
			e.nowHp = graphData.e.ship[ii].nowHP;
			e.startHp = graphData.e.ship[ii].startHP;

			e.maxBar = barWidth;
			e.nowBar = barWidth * e.nowHp / e.maxHp;
			e.startBar = barWidth * e.startHp / e.maxHp;
		}

		html +="		<tr>";
		html +="			<td class=\"center\">"
								+ f.name + f.mvp
								+ "<br>" + f.lv
								+ "<br><span class=\"" + f.state + "\">" + f.stateText + "</span></td>";
		html +="			<td class=\"center\">獲得<br>現在<br>次</td>";
		html +="			<td class=\"number\">" + f.getExp + "<br>" + f.nowExp + "<br>" + f.nextExp + "</td>";
		html +="			<td class=\"right\">";
		html +="				<div class=\"bar max\" style=\"width: " + f.maxBar + "px\">" + f.maxHp + "</div><br>";
		html +="				<div class=\"bar before\" style=\"width: " + f.startBar + "px\">" + f.startHp + "</div><br>";
		html +="				<div class=\"bar after\" style=\"width: " + f.nowBar + "px\">" + f.nowHp + "</div><br>";
		html +="			<td colspan=\"2\" class=\"center\">最大<br>戦闘前<br>戦闘後</td>";
		html +="			<td>";
		html +="				<div class=\"bar max\" style=\"width: " + e.maxBar + "px\">" + e.maxHp + "</div><br>";
		html +="				<div class=\"bar before\" style=\"width: " + e.startBar + "px\">" + e.startHp + "</div><br>";
		html +="				<div class=\"bar after\" style=\"width: " + e.nowBar + "px\">" + e.nowHp + "</div><br>";
		html +="			</td>";
		html +="			<td class=\"center\">"
								+ e.name
								+ "<br>" + e.class
								+ "<br><span class=\"" + e.state + "\">" + e.stateText + "</span></td>";
		html +="		</tr>";
	}

	html +="	</tbody></table>";
	html +="	<table id=\"getShip\"><tbody>";
	html +="		<tr>";
	html +="			<th>獲得艦</th>";
	html +="			<td>" + getShipInfo + "</td>";
	html +="		</tr>";
	html +="	</tbody></table>";

	return html;
}

function createBattleCombinedData(data)
{
	graphData = {
		battleField:
		{
			area: battleArea,
			map:  battleMap
		},
		f:
		{
			name: data.deck.api_name,
			ship: []
		},
		c:
		{
			name: data.comb.api_name,
			ship: []
		},
		e:
		{
			name: "敵艦隊",
			ship: []
		},
		formation:
		{
			f: getFormationName(formation[0]),
			e: getFormationName(formation[1]),
			i: getInterceptName(formation[2])
		},
		winRank: "",
		mvp: null,
		getShip:
		{
			id: "",
			stype: "",
			sname: "",
			getmes: ""
		}
	};

	// 自軍艦名・レベル
	for (var ii = 0; ii < data.deck.api_ship.length; ii++)
	{
		var id = data.deck.api_ship[ii];
		if (id == -1) continue;
		graphData.f.ship.push({
			name: ApiMaster.ship[ApiMember.ship.data[id].api_ship_id].api_name,
			lv: ApiMember.ship.data[id].api_lv
		});
	}
	for (var ii = 0; ii < data.comb.api_ship.length; ii++)
	{
		var id = data.comb.api_ship[ii];
		if (id == -1) continue;
		graphData.c.ship.push({
			name: ApiMaster.ship[ApiMember.ship.data[id].api_ship_id].api_name,
			lv: ApiMember.ship.data[id].api_lv
		});
	}
	// 自軍耐久初期値
	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_maxhps[ii + 1] != -1)
		{
			graphData.f.ship[ii].startHP = data.api_nowhps[ii + 1];
			graphData.f.ship[ii].nowHP = data.api_nowhps[ii + 1];
			graphData.f.ship[ii].maxHP = data.api_maxhps[ii + 1];
		}
	}
	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_maxhps[ii + 1] != -1)
		{
			graphData.c.ship[ii].startHP = data.api_nowhps_combined[ii + 1];
			graphData.c.ship[ii].nowHP = data.api_nowhps_combined[ii + 1];
			graphData.c.ship[ii].maxHP = data.api_maxhps_combined[ii + 1];
		}
	}

	// 敵軍艦名
	for (var ii = 0; ii < data.api_ship_ke.length; ii++)
	{
		var id = data.api_ship_ke[ii];
		if (id != -1)
		{
			graphData.e.ship.push({
				name: ApiMaster.enemy[id].api_name,
				class: ApiMaster.enemy[id].api_yomi
			});
		}
	}

	// 敵軍耐久初期値
	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_maxhps[ii + 7] != -1)
		{
			graphData.e.ship[ii].startHP = data.api_nowhps[ii + 7];
			graphData.e.ship[ii].nowHP = data.api_nowhps[ii + 7];
			graphData.e.ship[ii].maxHP = data.api_maxhps[ii + 7];
		}
	}

	// 第1艦隊開幕航空戦
	if (data.api_kouku != null && data.api_kouku.api_stage3)
	{
		BattleCalc.calcKoukuStage3(data.api_kouku.api_stage3);
	}
	if (data.api_kouku != null && data.api_kouku.api_stage3_combined)
	{
		BattleCalcCombined.calcKoukuStage3(data.api_kouku.api_stage3_combined);
	}

	// 支援艦攻撃
	if (data.api_support_info != null)
	{
		BattleCalc.calcSupportAttack(data.api_support_info);
	}

	// 第1艦隊航空戦（2回目）
	if (data.api_kouku2 != null && data.api_kouku2.api_stage3 != null)
	{
		BattleCalc.calcKoukuStage3(data.api_kouku2.api_stage3);
	}
	if (data.api_kouku2 != null && data.api_kouku2.api_stage3_combined != null)
	{
		BattleCalcCombined.calcKoukuStage3(data.api_kouku2.api_stage3_combined);
	}

	// 第2艦隊開幕雷撃戦
	if (data.api_opening_atack != null)
	{
		BattleCalcCombined.calcOpeningAttack(data.api_opening_atack);
	}

	if (data.api_hougeki1 != null)
	{
		switch (data.LogType)
		{
			case "battleCombinedWater":
				// 第1艦隊砲撃戦
				BattleCalc.calcHougeki(data.api_hougeki1);
				break;
			default:
				// 第2艦隊砲撃戦
				BattleCalcCombined.calcHougeki(data.api_hougeki1);
				break;
		}
	}

	if (data.api_raigeki != null)
	{
		// 第2艦隊雷撃戦
		BattleCalcCombined.calcRaigeki(data.api_raigeki);
	}

	// 第1艦隊砲撃戦
	if (data.api_hougeki2 != null)
	{
		BattleCalc.calcHougeki(data.api_hougeki2);
	}
	if (data.api_hougeki3 != null)
	{
		switch (data.LogType)
		{
			case "battleCombinedWater":
				// 第2艦隊砲撃戦
				BattleCalcCombined.calcHougeki(data.api_hougeki3);
				break;
			default:
				// 第1艦隊砲撃戦
				BattleCalc.calcHougeki(data.api_hougeki3);
				break;
		}
	}

	// 第2艦隊砲撃戦(夜戦)
	if (data.api_hougeki != null)
	{
		BattleCalcCombined.calcHougekiYasen(data.api_hougeki);
	}
}


function createBattleCombinedWaterData(data)
{
	graphData = {
		battleField:
		{
			area: battleArea,
			map:  battleMap
		},
		f:
		{
			name: data.deck.api_name,
			ship: []
		},
		c:
		{
			name: data.comb.api_name,
			ship: []
		},
		e:
		{
			name: "敵艦隊",
			ship: []
		},
		formation:
		{
			f: getFormationName(formation[0]),
			e: getFormationName(formation[1]),
			i: getInterceptName(formation[2])
		},
		winRank: "",
		mvp: null,
		getShip:
		{
			id: "",
			stype: "",
			sname: "",
			getmes: ""
		}
	};

	// 自軍艦名・レベル
	for (var ii = 0; ii < data.deck.api_ship.length; ii++)
	{
		var id = data.deck.api_ship[ii];
		if (id == -1) continue;
		graphData.f.ship.push({
			name: ApiMaster.ship[ApiMember.ship.data[id].api_ship_id].api_name,
			lv: ApiMember.ship.data[id].api_lv
		});
	}
	for (var ii = 0; ii < data.comb.api_ship.length; ii++)
	{
		var id = data.comb.api_ship[ii];
		if (id == -1) continue;
		graphData.c.ship.push({
			name: ApiMaster.ship[ApiMember.ship.data[id].api_ship_id].api_name,
			lv: ApiMember.ship.data[id].api_lv
		});
	}
	// 自軍耐久初期値
	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_maxhps[ii + 1] != -1)
		{
			graphData.f.ship[ii].startHP = data.api_nowhps[ii + 1];
			graphData.f.ship[ii].nowHP = data.api_nowhps[ii + 1];
			graphData.f.ship[ii].maxHP = data.api_maxhps[ii + 1];
		}
	}
	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_maxhps[ii + 1] != -1)
		{
			graphData.c.ship[ii].startHP = data.api_nowhps_combined[ii + 1];
			graphData.c.ship[ii].nowHP = data.api_nowhps_combined[ii + 1];
			graphData.c.ship[ii].maxHP = data.api_maxhps_combined[ii + 1];
		}
	}

	// 敵軍艦名
	for (var ii = 0; ii < data.api_ship_ke.length; ii++)
	{
		var id = data.api_ship_ke[ii];
		if (id != -1)
		{
			graphData.e.ship.push({
				name: ApiMaster.enemy[id].api_name,
				class: ApiMaster.enemy[id].api_yomi
			});
		}
	}

	// 敵軍耐久初期値
	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_maxhps[ii + 7] != -1)
		{
			graphData.e.ship[ii].startHP = data.api_nowhps[ii + 7];
			graphData.e.ship[ii].nowHP = data.api_nowhps[ii + 7];
			graphData.e.ship[ii].maxHP = data.api_maxhps[ii + 7];
		}
	}

	// 第1艦隊開幕航空戦
	if (data.api_kouku != null && data.api_kouku.api_stage3)
	{
		BattleCalc.calcKoukuStage3(data.api_kouku.api_stage3);
	}
	if (data.api_kouku != null && data.api_kouku.api_stage3_combined)
	{
		BattleCalcCombined.calcKoukuStage3(data.api_kouku.api_stage3_combined);
	}

	// 支援艦攻撃
	if (data.api_support_info != null)
	{
		BattleCalc.calcSupportAttack(data.api_support_info);
	}

	// 第1艦隊航空戦（2回目）
	if (data.api_kouku2 != null && data.api_kouku2.api_stage3 != null)
	{
		BattleCalc.calcKoukuStage3(data.api_kouku2.api_stage3);
	}
	if (data.api_kouku2 != null && data.api_kouku2.api_stage3_combined != null)
	{
		BattleCalcCombined.calcKoukuStage3(data.api_kouku2.api_stage3_combined);
	}

	// 第2艦隊開幕雷撃戦
	if (data.api_opening_atack != null)
	{
		BattleCalcCombined.calcOpeningAttack(data.api_opening_atack);
	}

	// 第2艦隊砲撃戦
	if (data.api_hougeki1 != null)
	{
		BattleCalc.calcHougeki(data.api_hougeki1);
	}

	// 第2艦隊雷撃戦
	if (data.api_raigeki != null)
	{
		BattleCalcCombined.calcRaigeki(data.api_raigeki);
	}

	// 第1艦隊砲撃戦
	if (data.api_hougeki2 != null)
	{
		BattleCalc.calcHougeki(data.api_hougeki2);
	}
	if (data.api_hougeki3 != null)
	{
		BattleCalcCombined.calcHougeki(data.api_hougeki3);
	}

	// 第2艦隊砲撃戦(夜戦)
	if (data.api_hougeki != null)
	{
		BattleCalcCombined.calcHougekiYasen(data.api_hougeki);
	}
}

function createResultCombinedData(data)
{
	graphData.mvp = data.api_mvp;
	graphData.mvp2 = data.api_mvp_combined;

	graphData.e.name = data.api_enemy_info.api_deck_name;

	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_get_ship_exp[ii + 1] != -1)
		{
			graphData.f.ship[ii].getExp = data.api_get_ship_exp[ii + 1];
			graphData.f.ship[ii].nowExp = data.api_get_exp_lvup[ii][0];
			graphData.f.ship[ii].nextExp = data.api_get_exp_lvup[ii][1];
		}
	}
	for (var ii = 0; ii < 6; ii++)
	{
		if (data.api_get_ship_exp_combined[ii + 1] != -1)
		{
			graphData.c.ship[ii].getExp = data.api_get_ship_exp_combined[ii + 1];
			graphData.c.ship[ii].nowExp = data.api_get_exp_lvup_combined[ii][0];
			graphData.c.ship[ii].nextExp = data.api_get_exp_lvup_combined[ii][1];
		}
	}

	if (data.api_get_ship)
	{
		graphData.getShip.id	= data.api_get_ship.api_ship_id;
		graphData.getShip.stype = data.api_get_ship.api_ship_type;
		graphData.getShip.sname = data.api_get_ship.api_ship_name;
		graphData.getShip.getmes = data.api_get_ship.api_ship_getmes;
	}

	switch (data.api_win_rank)
	{
		case "S":
			graphData.winRank = " <span style=\"color:#ffff00;\">S </span> ";
			break;
		case "A":
			graphData.winRank = " <span style=\"color:#ff0000;\">A </span> ";
			break;
		case "B":
			graphData.winRank = " <span style=\"color:#ff8000;\">B </span> ";
			break;
		case "C":
			graphData.winRank = " <span style=\"color:#ffff00;\">C </span> ";
			break;
		case "D":
			graphData.winRank = " <span style=\"color:#00ffff;\">D </span> ";
			break;
		case "E":
			graphData.winRank = " <span style=\"color:#0000ff;\">E </span> ";
			break;
	}
}

function createCombinedLog(data)
{
	var map = battleMap;
	var area = battleArea;

	// var haveShip = "";

	var getShipInfo = "";
	if (graphData.getShip.id)
	{
		var getShip = ApiMaster.ship[graphData.getShip.id];

		// haveShip = checkHave(getShip);

		getShipInfo += graphData.getShip.sname;
//		getShipInfo += " - " + localData.CType[getShip.api_ctype];
//		getShipInfo += " " + getShip.api_cnum + "番艦";
		getShipInfo += " " + graphData.getShip.stype;
		// getShipInfo += " " + haveShip;
	}

	var html = "";

	html +="	<table><tbody>";
	html +="		<tr>";
	html +="			<th colspan=\"8\" class=\"center\">" + area + " - " + map + "</th>";
	html +="		</tr>";
	html +="		<tr>";
	html +="			<th colspan=\"4\">" + graphData.f.name + "</th>";
	html +="			<th colspan=\"2\">" + graphData.winRank + "</th>";
	html +="			<th colspan=\"2\">" + graphData.e.name + "</th>";
	html +="		</tr>";
	html +="		<tr>";
	html +="			<th>艦名</th>";
	html +="			<th colspan=\"2\">経験値</th>";
	html +="			<th colspan=\"4\">"
							+ graphData.formation.f
							+ " x "
							+ graphData.formation.i
							+ " x "
							+ graphData.formation.e + "</th>";
	html +="			<th>艦名</th>";
	html +="		</tr>";

	for (var ii = 0; ii < 12; ii++)
	{
		var f =
		{
			name: "",
			lv: "",
			state: "",
			stateText: "",
			mvp: "",

			maxHp: "",
			nowHp: "",
			startHp: "",

			maxBar: 0,
			nowBar: 0,
			startBar: 0,

			getExp: "",
			nowExp: "",
			nextExp: ""
		};
		var e =
		{
			name: "",
			class: "",
			state: "",
			stateText: "",

			maxHp: "",
			nowHp: "",
			startHp: "",

			maxBar: 0,
			nowBar: 0,
			startBar: 0
		};

		if (graphData.f.ship.length > ii)
		{
			f.name = graphData.f.ship[ii].name;
			f.lv = "Lv." + graphData.f.ship[ii].lv;
			f.state = calcState(graphData.f.ship[ii]);
			f.stateText = "(" + f.state + ")";

			f.maxHp = graphData.f.ship[ii].maxHP;
			f.nowHp = graphData.f.ship[ii].nowHP;
			f.startHp = graphData.f.ship[ii].startHP;

			f.maxBar = barWidth;
			f.nowBar = barWidth * f.nowHp / f.maxHp;
			f.startBar = barWidth * f.startHp / f.maxHp;

			if (graphData.f.ship[ii].getExp)
			{
				f.getExp = graphData.f.ship[ii].getExp;
				f.nowExp = graphData.f.ship[ii].nowExp + graphData.f.ship[ii].getExp;
				f.nextExp = graphData.f.ship[ii].nextExp;
			}

			if (graphData.mvp == ii + 1) f.mvp = "<span style=\"color: #ffff00;\">★</span>";
			else f.mvp = "";
		}
		else if (graphData.c.ship.length > ii - 6)
		{
			var jj = ii - 6;
			f.name = graphData.c.ship[jj].name;
			f.lv = "Lv." + graphData.c.ship[jj].lv;
			f.state = calcState(graphData.c.ship[jj]);
			f.stateText = "(" + f.state + ")";

			f.maxHp = graphData.c.ship[jj].maxHP;
			f.nowHp = graphData.c.ship[jj].nowHP;
			f.startHp = graphData.c.ship[jj].startHP;

			f.maxBar = barWidth;
			f.nowBar = barWidth * f.nowHp / f.maxHp;
			f.startBar = barWidth * f.startHp / f.maxHp;

			if (graphData.c.ship[jj].getExp)
			{
				f.getExp = graphData.c.ship[jj].getExp;
				f.nowExp = graphData.c.ship[jj].nowExp + graphData.c.ship[jj].getExp;
				f.nextExp = graphData.c.ship[jj].nextExp;
			}

			if (graphData.mvp2 == jj + 1) f.mvp = "<span style=\"color: #ffff00;\">★</span>";
			else f.mvp = "";
		}

		if (graphData.e.ship.length > ii)
		{
			e.name = graphData.e.ship[ii].name;
			e.class = graphData.e.ship[ii].class;
			e.state = calcState(graphData.e.ship[ii]);
			e.stateText = "(" + e.state + ")";

			e.maxHp = graphData.e.ship[ii].maxHP;
			e.nowHp = graphData.e.ship[ii].nowHP;
			e.startHp = graphData.e.ship[ii].startHP;

			e.maxBar = barWidth;
			e.nowBar = barWidth * e.nowHp / e.maxHp;
			e.startBar = barWidth * e.startHp / e.maxHp;
		}

		if (ii == 6)
		{
			html += "<tr><th colspan=\"5\">" + graphData.c.name + "</th><th colspan=\"3\"></th></tr>";
		}

		html +="		<tr>";
		html +="			<td class=\"center\">"
								+ f.name + f.mvp
								+ "<br>" + f.lv
								+ "<br><span class=\"" + f.state + "\">" + f.stateText + "</span></td>";
		html +="			<td class=\"center\">獲得<br>現在<br>次</td>";
		html +="			<td class=\"number\">" + f.getExp + "<br>" + f.nowExp + "<br>" + f.nextExp + "</td>";
		html +="			<td class=\"right\">";
		html +="				<div class=\"bar max\" style=\"width: " + f.maxBar + "px\">" + f.maxHp + "</div><br>";
		html +="				<div class=\"bar before\" style=\"width: " + f.startBar + "px\">" + f.startHp + "</div><br>";
		html +="				<div class=\"bar after\" style=\"width: " + f.nowBar + "px\">" + f.nowHp + "</div><br>";
		html +="			<td colspan=\"2\" class=\"center\">最大<br>戦闘前<br>戦闘後</td>";
		html +="			<td>";
		html +="				<div class=\"bar max\" style=\"width: " + e.maxBar + "px\">" + e.maxHp + "</div><br>";
		html +="				<div class=\"bar before\" style=\"width: " + e.startBar + "px\">" + e.startHp + "</div><br>";
		html +="				<div class=\"bar after\" style=\"width: " + e.nowBar + "px\">" + e.nowHp + "</div><br>";
		html +="			</td>";
		html +="			<td class=\"center\">"
								+ e.name
								+ "<br>" + e.class
								+ "<br><span class=\"" + e.state + "\">" + e.stateText + "</span></td>";
		html +="		</tr>";
	}

	html +="	</tbody></table>";
	html +="	<table id=\"getShip\"><tbody>";
	html +="		<tr>";
	html +="			<th>獲得艦</th>";
	html +="			<td>" + getShipInfo + "</td>";
	html +="		</tr>";
	html +="	</tbody></table>";

	return html;
}

function getMapAreaName()
{
	for(var key = 0 in ApiMaster.mapArea)
	{
		if (ApiMaster.mapArea[key].api_id == ApiMember.battleLog.start.api_maparea_id)
		{
			return ApiMaster.mapArea[key].api_name;
		}
	}

	return "";
}

function getMapInfoName()
{
	for(var key in ApiMaster.mapInfo)
	{
		if (ApiMaster.mapInfo[key].api_maparea_id == ApiMember.battleLog.start.api_maparea_id
			 && ApiMaster.mapInfo[key].api_no == ApiMember.battleLog.start.api_mapinfo_no)
		{
			return ApiMaster.mapInfo[key].api_name;
		}
	}

	return "";
}

function getFormationName(id)
{
	// apiの戻り値にintとstringが混在してるのでstringに変換してから評価する
	switch (id.toString())
	{
		case  "1": return "単縦陣";
		case  "2": return "複縦陣";
		case  "3": return "輪形陣";
		case  "4": return "梯形陣";
		case  "5": return "単横陣";

		case "11": return "対潜警戒";
		case "12": return "前方警戒";
		case "13": return "輪形陣";
		case "14": return "戦闘隊形";

		default: return "-";
	}
}

function getInterceptName(id)
{
	switch (id.toString())
	{
		case  "1": return "同航戦";
		case  "2": return "反航戦";
		case  "3": return "T字有利";
		case  "4": return "T字不利";
		default: return "-";
	}
}

// 損傷
function calcState(ship)
{
	return Calc.calcDamage(
		{
			api_nowhp: ship.nowHP,
			api_maxhp: ship.maxHP
		}
	);
}

function getType(obj) {
	return Object.prototype.toString.call(obj).slice(8, -1);
}
