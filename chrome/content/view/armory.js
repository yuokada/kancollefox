Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var title = document.title;

window.onload = function()
{
	f.initialize();
};

window.onunload = function()
{
	f.finalize();
};

var p = {};

var lastUpdateTime = 0;

var f = {
	initialize: function()
	{
		if (KancolleFoxConfig.loadBoolParameter(window.name, "configured"))
		{
			document.getElementById("DispId").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.id");
			document.getElementById("DispSpec").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.spec");
			document.getElementById("DispUser").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.user");
			document.getElementById("DispDescription").checked = KancolleFoxConfig.loadBoolParameter(window.name, "display.description");
		}

		f.initParameters();

		document.onkeydown = f.onKeyDown;

		setInterval(function() { f.update(); }, 500);
	}
	,
	finalize: function()
	{
		windowSetings.save();
		KancolleFoxConfig.saveBoolParameter(window.name, "configured", true);
		KancolleFoxConfig.saveBoolParameter(window.name, "display.id", Util.getChecked("DispId"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.spec", Util.getChecked("DispSpec"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.user", Util.getChecked("DispUser"));
		KancolleFoxConfig.saveBoolParameter(window.name, "display.description", Util.getChecked("DispDescription"));
	}
	,
	onKeyDown: function(e)
	{
		var keyEvent = e || window.event;
		if (keyEvent.ctrlKey)
		{
			if (keyEvent.keyCode.toString() == 70)
			{
				document.getElementById("FilterName").focus();
			}
			else if (keyEvent.keyCode.toString() == 82)
			{
				f.clearFilter();
			}
		}
	}
	,
	update: function()
	{
		if (!f.checkUpdated()) return;

		lastUpdateTime = new Date();

		document.getElementById("contents").innerHTML = generateHtml();
		document.getElementById("ItemCount").innerHTML = Object.keys(ApiMember.slotItem.data).length;
		document.getElementById("ItemMax").innerHTML = ApiMember.basic.data.api_max_slotitem;

		document.title = title + " 最終更新日時: " + Util.dateTimeToString(lastUpdateTime);
	}
	,
	initParameters: function()
	{
		p = {
			filterName : "",

			dispId : "",
			dispSpec : "",
			dispDescription : "",
			dispUser : ""
		};
	}
	,
	clearFilter: function()
	{
		document.getElementById("FilterName").value = "";
	}
	,
	checkUpdated: function()
	{
		var ret = false;

		var filterName		= Util.getInputText("FilterName");

		var dispId			= Util.getChecked("DispId");
		var dispSpec		= Util.getChecked("DispSpec");
		var dispDescription	= Util.getChecked("DispDescription");
		var dispUser		= Util.getChecked("DispUser");

		ret = 
			(
				ApiMaster.updateTime
				&& ApiMember.slotItem.updateTime
			)
			&&
			(
				// 初回更新
				(! lastUpdateTime)

				// APIデータが更新されたとき
				|| (lastUpdateTime < ApiMember.ship.updateTime)
				|| (lastUpdateTime < ApiMember.slotItem.updateTime)


				// フィルターの変更
				|| (p.filterName != filterName)

				// 表示項目の変更
				|| (p.dispId != dispId)
				|| (p.dispSpec != dispSpec)
				|| (p.dispDescription != dispDescription)
				|| (p.dispUser != dispUser)
			);

		if (ret)
		{
			p = {
				filterName : filterName,

				dispId : dispId,
				dispSpec : dispSpec,
				dispDescription : dispDescription,
				dispUser : dispUser
			};
		}

		return ret;
	}

};

function selectSlotItem()
{
	var master = {};
	var count = {};
	var user = {};

	for (var key in ApiMaster.slotItem)
	{
		var data = ApiMaster.slotItem[key];
		master[data.api_id] = data;

		count[data.api_id] = 0;
		user[data.api_id] = [];
	}

	for (var key in ApiMember.slotItem.data)
	{
		var itemId = ApiMember.slotItem.data[key].api_slotitem_id;
		count[itemId] ++;
	}

	for (var key in ApiMember.ship.data)
	{
		var ship = ApiMember.ship.data[key];
		for (var ii = 0; ii < ship.api_slot.length; ii++)
		{
			var item = ship.api_slot[ii];
			if (item != -1)
			{
				try
				{
					var itemId = ApiMember.slotItem.data[item].api_slotitem_id;
					user[itemId].push({ship: ship, itemLevel: ApiMember.slotItem.data[item].api_level});
				}
				catch (e) { }
			}
		}
	}

	var ret = [];
	for (var key in master)
	{
		if (
			(
				"" == p.filterName
				|| master[key].api_name.match(p.filterName)
			)
			&& master[key].api_id < 500
		)
		{
			ret.push({
				item: master[key],
				count: count[key],
				user: user[key]
			});
		}
	}
	ret.sort(
		function (a, b)
		{
			if (a.item.api_type[2] > b.item.api_type[2])	return 1;
			if (a.item.api_type[2] < b.item.api_type[2])	return -1;
			if (a.item.api_type[3] > b.item.api_type[3])	return 1;
			if (a.item.api_type[3] < b.item.api_type[3])	return -1;

			if (a.item.api_type[0] > b.item.api_type[0])	return 1;
			if (a.item.api_type[0] < b.item.api_type[0])	return -1;
			if (a.item.api_type[1] > b.item.api_type[1])	return 1;
			if (a.item.api_type[1] < b.item.api_type[1])	return -1;

			return 0;
		}
	);

	return ret;
}


function generateHtml()
{
	var itemList = selectSlotItem();
	var lastType = "";

	var rowspan = 2 + (p.dispUser ? 1: 0) + (p.dispDescription ? 1: 0);
	var colspan = 3 + (p.dispSpec ? 14: 0);

	var html = "";

	html += "	<table><tbody>";

	for (var ii = 0; ii < itemList.length; ii++)
	{
		var data = itemList[ii];
		if (lastType != data.item.api_type[2])
		{
			lastType = data.item.api_type[2];
			html += generateThr(data.item.api_type[2] + ". " + ApiMaster.slotItemEquipType[data.item.api_type[2]].api_name, rowspan, colspan);
		}

		html += generateTdr(data, colspan);
	}

	html += "	</tbody></table>";

	return html;
}

function generateThr(type, rowspan, colspan)
{
	var html = "";

	html += "		<tr>";
	if (p.dispId)
	{
		html += "			<th rowspan=\"" + rowspan + "\">ID</th>";
	}
	html += "			<th rowspan=\"" + rowspan + "\">" + type + "</th>";
	html += "			<th colspan=\"3\">保有数</th>";
	if (p.dispSpec)
	{
		html += "			<th rowspan=\"2\">希少</th>";
		html += "			<th rowspan=\"2\">装<br>甲</th>";
		html += "			<th colspan=\"5\">火力</th>";
		html += "			<th colspan=\"2\">命中</th>";
		html += "			<th colspan=\"3\">回避</th>";
		html += "			<th rowspan=\"2\">索<br>敵</th>";
		html += "			<th rowspan=\"2\">射<br>程</th>";
	}
	html += "		</tr>";

	html += "		<tr>";
	html += "			<th>総</th>";
	html += "			<th>使</th>";
	html += "			<th>残</th>";
	if (p.dispSpec)
	{
		html += "			<th>砲</th>";
		html += "			<th>雷</th>";
		html += "			<th>爆</th>";
		html += "			<th>空</th>";
		html += "			<th>潜</th>";
		html += "			<th>砲</th>";
		html += "			<th>雷</th>";
		html += "			<th>砲</th>";
		html += "			<th>雷</th>";
		html += "			<th>爆</th>";
	}
	html += "		</tr>";
	if (p.dispUser)
	{
		html += "		<tr>";
		html += "			<th colspan=\"" + colspan + "\">搭載艦娘</th>";
		html += "		</tr>";
	}
	if (p.dispDescription)
	{
		html += "		<tr>";
		html += "			<th colspan=\"" + colspan + "\">説明</th>";
		html += "		</tr>";
	}

	return html;
}

function generateTdr(data, colspan)
{
	var rare = "☆";
	for (var ii = 0; ii < data.item.api_rare; ii++)
	{
		rare += "☆";
	}

	var user = "";
	for (var ii = 0; ii < data.user.length; ii++)
	{
		if (user != "") user += ", ";
		user += ApiMaster.ship[data.user[ii].ship.api_ship_id].api_name;
		user += "(lv." + data.user[ii].ship.api_lv + ")";
		if (data.user[ii].itemLevel > 0)
			user += "<span class=\"itemLevel\">★+" + data.user[ii].itemLevel + "</span>";
	}

	var leng = "";
	if (data.item.api_leng)
		leng = localData.Leng[data.item.api_leng];

	var rowspan = 1 + (p.dispUser ? 1: 0) + (p.dispDescription ? 1: 0);

	var html = "";

	html += "		<tr onmouseover=\"RowCursor.hover(this)\" onmouseout=\"RowCursor.leave(this)\">";
	if (p.dispId)
	{
		html += "			<td class=\"number\" rowspan=\"" + rowspan + "\">" + data.item.api_id + "</td>";
	}
	html += "			<td rowspan=\"" + rowspan + "\">" + data.item.api_name + "</td>";
	html += "			<td class=\"number\">" + data.count + "</td>";
	html += "			<td class=\"number\">" + data.user.length + "</td>";
	html += "			<td class=\"number\">" + (data.count - data.user.length) + "</td>";
	if (p.dispSpec)
	{
		html += "			<td>" + rare + "</td>";
		html += "			<td class=\"number\">" + data.item.api_souk + "</td>";// 装甲
		html += "			<td class=\"number\">" + data.item.api_houg + "</td>";// 砲撃
		html += "			<td class=\"number\">" + data.item.api_raig + "</td>";// 雷装
		html += "			<td class=\"number\">" + data.item.api_baku + "</td>";// 爆装
		html += "			<td class=\"number\">" + data.item.api_tyku + "</td>";// 対空
		html += "			<td class=\"number\">" + data.item.api_tais + "</td>";// 対潜
		html += "			<td class=\"number\">" + data.item.api_houm + "</td>";// 砲命
		html += "			<td class=\"number\">" + data.item.api_raim + "</td>";// 雷命
		html += "			<td class=\"number\">" + data.item.api_houk + "</td>";// 砲避
		html += "			<td class=\"number\">" + data.item.api_raik + "</td>";// 雷避
		html += "			<td class=\"number\">" + data.item.api_bakk + "</td>";// 爆避
		html += "			<td class=\"number\">" + data.item.api_saku + "</td>";// 索敵
		html += "			<td class=\"center\">" + leng + "</td>";// 射程
	}
	html += "		</tr>";
	if (p.dispUser)
	{
		html += "		<tr onmouseover=\"RowCursor.hover(this)\" onmouseout=\"RowCursor.leave(this)\">";
		html += "			<td colspan=\"" + colspan + "\"><div class=\"user\">" + user + "</div></td>";
		html += "		</tr>";
	}
	if (p.dispDescription)
	{
		html += "		<tr onmouseover=\"RowCursor.hover(this)\" onmouseout=\"RowCursor.leave(this)\">";
		html += "			<td colspan=\"" + colspan + "\"><div class=\"description\">" + data.item.api_info + "</div></td>";
		html += "		</tr>";
	}

	return html;
}
