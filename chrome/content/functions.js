var functionNames =
{
	"ship":			"艦娘名簿",
	"deck": 		"艦隊戦力",
	"arsenal":		"資材倉庫",
	"armory":		"武器庫",
	"checker":		"遠征チェッカ",
	"timer":		"タイマー",
	"ndockCalc":	"お風呂の順番",
	"collection":	"これくしょん",
	"capture":		"撮影",
	"battle":		"戦闘ログ"
};
