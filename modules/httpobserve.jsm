var EXPORTED_SYMBOLS = ["KancolleFoxHttpRequestObserver"];



const Cc = Components.classes;
const Ci = Components.interfaces;

var callback = new Array();


function CCIN(cName, ifaceName)
{
	return Cc[cName].createInstance(Ci[ifaceName]);
}

function TracingListener()
{
	this.originalListener = null;
}

TracingListener.prototype =
{
	onDataAvailable: function(request, context, inputStream, offset, count)
	{
		var binaryInputStream = CCIN("@mozilla.org/binaryinputstream;1", "nsIBinaryInputStream");
		var storageStream = CCIN("@mozilla.org/storagestream;1", "nsIStorageStream");
		var binaryOutputStream = CCIN("@mozilla.org/binaryoutputstream;1", "nsIBinaryOutputStream");

		binaryInputStream.setInputStream(inputStream);
		storageStream.init(8192, count, null);
		binaryOutputStream.setOutputStream(storageStream.getOutputStream(0));

		var data = binaryInputStream.readBytes(count);
		this.receivedData.push(data);

		binaryOutputStream.writeBytes(data, count);

		this.originalListener.onDataAvailable(request, context,
						storageStream.newInputStream(0), offset, count);
	},

	onStartRequest: function(request, context)
	{
		this.originalListener.onStartRequest(request, context);
		this.receivedData = new Array();
	},

	onStopRequest: function(request, context, statusCode)
	{
		this.originalListener.onStopRequest(request, context, statusCode);

		var s = this.receivedData.join('');
		for (var k in  callback)
		{
			var f = callback[k];
			if(typeof f == "function")
			{
				f(request, s, "receive");
			}
		}
	},

	QueryInterface: function (aIID)
	{
		if (aIID.equals(Ci.nsIStreamListener) ||
			aIID.equals(Ci.nsISupports))
		{
			return this;
		}
		throw Components.results.NS_NOINTERFACE;
	}
};

var KancolleFoxHttpRequestObserver =
{
	counter: 0,

	observe: function(aSubject, aTopic, aData)
	{
		if( aTopic == "http-on-modify-request" )
		{
			var httpChannel = aSubject.QueryInterface(Components.interfaces.nsIHttpChannel);
			if (httpChannel.requestMethod == 'POST' &&
				httpChannel.URI.spec.match(/^http.*\/kcsapi\//))
			{
				httpChannel.QueryInterface(Components.interfaces.nsIUploadChannel);

				var us = httpChannel.uploadStream;
				us.QueryInterface(Components.interfaces.nsISeekableStream);

				var ss = Components.classes["@mozilla.org/scriptableinputstream;1"]
					.createInstance(Components.interfaces. nsIScriptableInputStream);
				ss.init(us);

				us.seek(0, 0);

				var n = ss.available();
				var postdata = ss.read(n);
				us.seek(0, 0);

				for(var key in callback)
				{
					let f = callback[key];
					if (typeof(f) == "function")
					{
						f({name: httpChannel.URI.spec}, postdata, "post");
					}
				}
			}
		}
		else if (aTopic == "http-on-examine-response")
		{
			var httpChannel = aSubject.QueryInterface(Components.interfaces.nsIHttpChannel);
			if (httpChannel.URI.spec.match(/^http.*\/kcsapi\//))
			{
				var newListener = new TracingListener();
				aSubject.QueryInterface(Ci.nsITraceableChannel);
				newListener.originalListener = aSubject.setNewListener(newListener);
			}
		}
	},

	QueryInterface : function (aIID)
	{
		if (aIID.equals(Ci.nsIObserver) || aIID.equals(Ci.nsISupports))
		{
			return this;
		}
		throw Components.results.NS_NOINTERFACE;
	},

	addCallback: function(f)
	{
		callback.push(f);
	},

	removeCallback: function(f)
	{
		for (var key in callback)
		{
			if (callback[key] == f)
			{
				callback.splice(key, 1);
			}
		}
	},

	init: function()
	{
		if (this.counter == 0)
		{
			this.observerService = Components.classes["@mozilla.org/observer-service;1"]
				.getService(Components.interfaces.nsIObserverService);
			this.observerService.addObserver(this, "http-on-examine-response", false);
			this.observerService.addObserver(this, "http-on-modify-request", false);
		}
		this.counter++;
	},

	destroy: function()
	{
		this.counter--;

		if (this.counter <= 0)
		{
			this.observerService.removeObserver(this, "http-on-examine-response");
			this.observerService.removeObserver(this, "http-on-modify-request");
			this.counter = 0;
		}
	}
};
