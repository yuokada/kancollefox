var EXPORTED_SYMBOLS = ["LocalData", "ApiMaster", "ApiMember", "ApiPost"];
/*
 * APIで取得できないデータをコード内に記述します。
 */
var LocalData = {
};

/*
 * 
 */
var ApiPost =
{
};

/*
 * APIから取得したマスタ系データを保持します。
 */
var ApiMaster =
{
	mapArea:
	{
	},
	mapInfo:
	{
	},
	slotItemEquipType:
	{
	},
	slotItem:
	{
	},
	sType:
	{
	},
	ship:
	{
	},
	enemy:
	{
	},
	mission:
	{
	},

	updateTime: 0
};

/*
 * APIから取得したトラン系データを保持します。
 */
var ApiMember =
{
	basic:
	{
		data: {},
		updateTime: 0
	},
	record:
	{
		data: {},
		updateTime: 0
	},
	ship:
	{
		data: {},
		updateTime: 0
	},
	shipInDeck:
	{
		data: {},
		updateTime: 0
	},
	slotItem:
	{
		data: {},
		updateTime: 0
	},
	deck:
	{
		data: [],
		updateTime: 0
	},
	kdock:
	{
		data: [],
		updateTime: 0
	},
	ndock:
	{
		data: [],
		updateTime: 0
	},
	material:
	{
		data: {},
		updateTime: 0
	},
	battleLog:
	{
		start: {},
		data: [],
		updateTime: 0
	},

	updateTime: 0
};
