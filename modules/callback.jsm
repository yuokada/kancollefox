Components.utils.import("resource://kancollefoxmodules/httpobserve.jsm");
Components.utils.import("resource://kancollefoxmodules/apidata.jsm");

var EXPORTED_SYMBOLS = ["KancolleFoxCallBack", "KancolleFoxError"];

var initialized = false;

var KancolleFoxError =
{
	exception: null,
	message: "",
	updateTime: 0
}

var Stock =
{
	battle:
	{
		deck: null
	}
};

/*
 * 初期化処理を行います。
 */
var KancolleFoxCallBack =
{
	initialize: function()
	{
		if (initialized) return;

		KancolleFoxHttpRequestObserver.init();
		KancolleFoxHttpRequestObserver.addCallback(KancolleFoxCallBack.callback);

		initialized = true;
	},

	callback: function(request, s, topic)
	{

		try
		{
			var url = request.name;
			if (! url.match(/kcsapi\/api_/)) returnl

			if (topic == "post")
			{
				var lines = s.split("\n");
				if (lines.length < 5) return;

				var data = {};

				var params = lines[4].split("&");
				for (var ii = 0; ii < params.length; ii++)
				{
					var kv = params[ii].split("=");
					data[decodeURIComponent(kv[0])] = decodeURIComponent(kv[1]);
				}

				// 解体・廃棄・近代化改修(素材艦)による装備消失を適用します。
				if (url.match(/kcsapi\/api_req_kousyou\/destroyship/))
				{
					Reader.ReadPostDestroyShip(data);
				}
				else if (url.match(/kcsapi\/api_req_kousyou\/destroyitem2/))
				{
					Reader.ReadPostDestroyItem2(data);
				}
				else if (url.match(/kcsapi\/api_req_kaisou\/powerup/))
				{
					Reader.ReadPostPowerUp(data);
				}


				else if (url.match(/kcsapi\/api_req_map\/start/))
				{
					Reader.ReadPostReqMapStart(data);
				}
				//Reader.ReadPost(url, data);
			}
			else
			{
				s = s.substring(s.indexOf('svdata=') + 7);
				var data = JSON.parse(s);

				if (data.api_result == 1)
				{
					if (url.match(/kcsapi\/api_start2/))
					{
						Reader.ReadSTypeMaster(data.api_data.api_mst_stype);
						Reader.ReadShipMaster(data.api_data.api_mst_ship);
						Reader.ReadMapArea(data.api_data.api_mst_maparea);
						Reader.ReadMapInfo(data.api_data.api_mst_mapinfo);
						Reader.ReadMissionMaster(data.api_data.api_mst_mission);
						Reader.ReadSlotItemEquipType(data.api_data.api_mst_slotitem_equiptype);
						Reader.ReadSlotItemMaster(data.api_data.api_mst_slotitem);

						ApiMaster.updateTime = Reader.TimeStamp();

						return;
					}

					else if (url.match(/kcsapi\/api_port\/port/))
					{
						Reader.ReadBasic(data.api_data.api_basic);
						Reader.ReadMaterial(data.api_data.api_material);
						Reader.ReadShip(data.api_data.api_ship);
						Reader.ReadNDock(data.api_data.api_ndock);
						Reader.ReadDeck(data.api_data.api_deck_port);
					}

					else if (url.match(/kcsapi\/api_get_member\/basic/))
					{
						Reader.ReadBasic(data.api_data);
					}
					else if (url.match(/kcsapi\/api_get_member\/deck/))
					{
						Reader.ReadDeck(data.api_data);
					}
					else if (url.match(/kcsapi\/api_get_member\/furniture/))
					{
						// 家具
						return;
					}
					else if (url.match(/kcsapi\/api_get_member\/kdock/))
					{
						Reader.ReadKDock(data.api_data);
					}
					else if (url.match(/kcsapi\/api_get_member\/mapcell/))
					{
						// mapcel
						return;
					}
					else if (url.match(/kcsapi\/api_get_member\/mapinfo/))
					{
						// mapinfo
						return;
					}
					else if (url.match(/kcsapi\/api_get_member\/material/))
					{
						Reader.ReadMaterial(data.api_data);
					}
					else if (url.match(/kcsapi\/api_get_member\/mission/))
					{
						// 遠征
						return;
					}
					else if (url.match(/kcsapi\/api_get_member\/ndock/))
					{
						Reader.ReadNDock(data.api_data);
					}
					else if (url.match(/kcsapi\/api_get_member\/picture_book/))
					{
						// 図鑑
						return;
					}
					else if (url.match(/kcsapi\/api_get_member\/questlist/))
					{
						// 任務リスト
						return;
					}
					else if (url.match(/kcsapi\/api_get_member\/record/))
					{
						Reader.ReadRecord(data.api_data);
					}
					else if (url.match(/kcsapi\/api_get_member\/ship2/))
					{
						Reader.ReadShip(data.api_data);
						Reader.ReadDeck(data.api_data_deck);
					}
					else if (url.match(/kcsapi\/api_get_member\/slot_item/))
					{
						Reader.ReadSlotItem(data.api_data);
					}
					else if (url.match(/kcsapi\/api_get_member\/sortie_conditions/))
					{
						// 通算戦績
						return;
					}
					else if (url.match(/kcsapi\/api_get_member\/unsetslot/))
					{
						// 未装着装備アイテム
						return;
					}

					else if (url.match(/kcsapi\/api_req_kousyou\/createitem/))
					{
						if(data.api_data.api_slot_item)
							Reader.ReadSlotItemAdd(data.api_data.api_slot_item);
					}
					else if (url.match(/kcsapi\/api_req_kousyou\/getship/))
					{
						Reader.ReadKDock(data.api_data.api_kdock);
						Reader.ReadSlotItemAddRange(data.api_data.api_slotitem);
					}
					else if (url.match(/kcsapi\/api_req_hensei\/change/))
					{
						Reader.ReadHenseiHenkou(data.api_data);
					}
					else if (url.match(/kcsapi\/api_req_kaisou\/slotset/))
					{
						// 装着
						return;
					}

					else if (url.match(/kcsapi\/api_req_map\/start/))
					{
						// 出撃開始
						Reader.ReadLogBattleStart(data.api_data);
					}
					else if (url.match(/kcsapi\/api_req_map\/next/))
					{
						// 進撃
						Reader.ReadLogBattleNext(data.api_data);
					}

					// 戦闘系API(通常)
					else if (url.match(/kcsapi\/api_req_sortie\/battleresult/))
					{
						Reader.ReadLogBattleResult(data.api_data);
					}
					else if (url.match(/kcsapi\/api_req_sortie\/battle/))
					{
						Reader.ReadLogBattle(data.api_data);
					}
					else if (url.match(/kcsapi\/api_req_battle_midnight\/battle/))
					{
						Reader.ReadLogBattle(data.api_data);
					}
					else if (url.match(/kcsapi\/api_req_battle_midnight\/sp_midnight/))
					{
						Reader.ReadLogBattle(data.api_data);
					}
					else if (url.match(/kcsapi\/api_req_sortie\/night_to_day/))
					{
						Reader.ReadLogBattle(data.api_data);
					}

					// 戦闘系API(連合艦隊)
					else if (url.match(/kcsapi\/api_req_combined_battle\/battleresult/))
					{
						Reader.ReadLogBattleResultCombined(data.api_data);
					}
					else if (url.match(/kcsapi\/api_req_combined_battle\/airbattle/))
					{
						Reader.ReadLogBattleCombined(data.api_data, "battleCombinedAir");
					}
					else if (url.match(/kcsapi\/api_req_combined_battle\/midnight_battle/))
					{
						Reader.ReadLogBattleCombined(data.api_data, "battleCombinedMidnight");
					}
					else if (url.match(/kcsapi\/api_req_combined_battle\/sp_midnight/))
					{
						Reader.ReadLogBattleCombined(data.api_data, "battleCombinedSpMidnight");
					}
					else if (url.match(/kcsapi\/api_req_combined_battle\/battle_water/))
					{
						Reader.ReadLogBattleCombined(data.api_data, "battleCombinedWater");
					}
					else if (url.match(/kcsapi\/api_req_combined_battle\/battle/))
					{
						Reader.ReadLogBattleCombined(data.api_data, "battleCombined");
					}

					else
					{
						return;
					}

					ApiMember.updateTime = Reader.TimeStamp();
				}
			}
		}
		catch(e)
		{
			KancolleFoxError.exception = e;
			KancolleFoxError.updateTime = Reader.TimeStamp();
			dump(e.toString());
		}
	}
};



/*
 * Api返却値を読み込みます。
 */
var Reader =
{
	/* ****************************************
	 * POSTデータ
	 * ************************************* */
	ReadPost: function(url, data)
	{
		var params = [];
		var keys = Object.keys(data);

		for (var ii = 0; ii < keys.length; ii++)
		{
			params.push(keys[ii] + ":" + data[keys[ii]]);
		}
		dump(url + "\n" + params.join("\n"));
	},

	ReadPostDestroyShip: function(data)
	{
		var ship = ApiMember.ship.data[data.api_ship_id];
		
		for (var ii = 0; ii < ship.api_slot.length; ii++)
		{
			if (ApiMember.slotItem.data[ship.api_slot[ii]])
				delete ApiMember.slotItem.data[ship.api_slot[ii]];
		}
		ApiMember.slotItem.updateTime = Reader.TimeStamp();
	},

	ReadPostDestroyItem2: function(data)
	{
		var items = data.api_slotitem_ids.split(",");
		for (var ii = 0; ii < items.length; ii++)
		{
			if (ApiMember.slotItem.data[items[ii]])
				delete ApiMember.slotItem.data[items[ii]];
		}
		ApiMember.slotItem.updateTime = Reader.TimeStamp();
	},

	ReadPostPowerUp: function(data)
	{
		var ships = data.api_id_items.split(",");
		for (var ii = 0; ii < ships.length; ii++)
		{
			var ship = ApiMember.ship.data[ships[ii]];
			for (var ii = 0; ii < ship.api_slot.length; ii++)
			{
				if (ApiMember.slotItem.data[ship.api_slot[ii]])
					delete ApiMember.slotItem.data[ship.api_slot[ii]];
			}
		}
		ApiMember.slotItem.updateTime = Reader.TimeStamp();
	},

	ReadPostReqMapStart: function(data)
	{
		if (data.api_dock_id)
		{
			Stock.battle.deck = ApiMember.deck.data[data.api_dock_id - 1];
		}
	},

	/* ****************************************
	 * マスタ系API
	 * ************************************* */
	ReadMapArea: function(data)
	{
		var hash = {};

		for(var ii = 0; ii < data.length; ii++)
		{
			hash[data[ii].api_id] = data[ii];
		}

		ApiMaster.mapArea = hash;
	},

	ReadMapInfo: function(data)
	{
		var hash = {};

		for(var ii = 0; ii < data.length; ii++)
		{
			hash[data[ii].api_id] = data[ii];
		}

		ApiMaster.mapInfo = hash;
	},

	ReadSlotItemEquipType: function(data)
	{
		var hash = {};

		for(var ii = 0; ii < data.length; ii++)
		{
			hash[data[ii].api_id] = data[ii];
		}

		ApiMaster.slotItemEquipType = hash;
	},

	ReadSlotItemMaster: function(data)
	{
		var hash = {};

		for(var ii = 0; ii < data.length; ii++)
		{
			hash[data[ii].api_id] = data[ii];
		}

		ApiMaster.slotItem = hash;
	},

	ReadSTypeMaster: function(data)
	{
		var hash = {};

		for(var ii = 0; ii < data.length; ii++)
		{
			hash[data[ii].api_id] = data[ii];
		}

		ApiMaster.sType = hash;
	},

	ReadShipMaster: function(data)
	{
		var ships = {};
		var enemies = {};

		for (var ii = 0; ii < data.length; ii++)
		{
			var ship = data[ii];
			if (ship.api_sortno)
			{
				ships[ship.api_id] = ship;
			}
			else
			{
				enemies[ship.api_id] = ship;
			}
		}

		ApiMaster.ship = ships;
		ApiMaster.enemy = enemies;
	},

	ReadMissionMaster: function(data)
	{
		var hash = {};

		for(var ii = 0; ii < data.length; ii++)
		{
			hash[data[ii].api_id] = data[ii];
		}

		ApiMaster.mission = hash;
	},

	/* ****************************************
	 * トラン系API
	 * ************************************* */
	ReadBasic: function(data)
	{
		ApiMember.basic.data = data;
		ApiMember.basic.updateTime = Reader.TimeStamp();
	},
	ReadRecord: function(data)
	{
		ApiMember.record.data = data;
		ApiMember.record.updateTime = Reader.TimeStamp();
	},

	ReadShip: function(data)
	{
		var hash = {};

		for(var ii = 0; ii < data.length; ii++)
		{
			data[ii].no = ii + 1;
			hash[data[ii].api_id] = data[ii];
		}

		ApiMember.ship.data = hash;
		ApiMember.ship.updateTime = Reader.TimeStamp();
	},

	ReadSlotItem: function(data)
	{
		var hash = {};

		for(var ii = 0; ii < data.length; ii++)
		{
			hash[data[ii].api_id] = data[ii];
		}

		ApiMember.slotItem.data = hash;
		ApiMember.slotItem.updateTime = Reader.TimeStamp();
	},

	ReadSlotItemAdd: function(data)
	{
		ApiMember.slotItem.data[data.api_id] = data;
		ApiMember.slotItem.updateTime = Reader.TimeStamp();
	},

	ReadSlotItemAddRange: function(data)
	{
		for (var ii = 0; ii < data.length; ii++)
		{
			ApiMember.slotItem.data[data[ii].api_id] = data[ii];
		}
		ApiMember.slotItem.updateTime = Reader.TimeStamp();
	},

	ReadDeck: function(data)
	{
		ApiMember.deck.data = data;
		ApiMember.shipInDeck.data = {};
		for (var ii = 0; ii < ApiMember.basic.data.api_count_deck; ii++)
		{
			var deck = ApiMember.deck.data[ii];
			for (var jj = 0; jj < deck.api_ship.length; jj++)
			{
				if (deck.api_ship[jj] != -1)
				{
					ApiMember.shipInDeck.data[deck.api_ship[jj]] = {};
					ApiMember.shipInDeck.data[deck.api_ship[jj]].shipNo = jj;
					ApiMember.shipInDeck.data[deck.api_ship[jj]].deckName = deck.api_name;
					ApiMember.shipInDeck.data[deck.api_ship[jj]].deckNo = ii;
				}
			}
		}

		ApiMember.deck.updateTime = Reader.TimeStamp();
		ApiMember.shipInDeck.updateTime = Reader.TimeStamp();
	},

	ReadKDock: function(data)
	{
		ApiMember.kdock.data = data;
		ApiMember.kdock.updateTime = Reader.TimeStamp();
	},

	ReadNDock: function(data)
	{
		ApiMember.ndock.data = data;
		ApiMember.ndock.updateTime = Reader.TimeStamp();
	},

	ReadMaterial: function(data)
	{
		ApiMember.material.data = data;
		ApiMember.material.updateTime = Reader.TimeStamp();
	},

	ReadHenseiHenkou: function(data)
	{
		// 編成変更時は戦闘ログを消去します。(新編成のメンバーで表示されてしまうため)
/*		ApiMember.battleLog.updateTime = 0;
		ApiMember.battleLog.data = [];
		ApiMember.battleLog.start = {}; */
	},

	/* ****************************************
	 * 戦闘系API
	 * ************************************* */
	ReadLogBattleStart: function(data)
	{
		ApiMember.battleLog.data = [];
		ApiMember.battleLog.start = data;
		ApiMember.battleLog.updateTime = Reader.TimeStamp();
	},

	ReadLogBattleNext: function(data)
	{
	},

	ReadLogBattle: function(data)
	{
		ApiMember.battleLog.data = [];
		data["LogType"] = "battle";
		if (data.api_dock_id)
		{
			Stock.battle.deck = ApiMember.deck.data[data.api_dock_id - 1];
		}
		if (data.api_formation)
		{
			Stock.battle.formation = data.api_formation;
		}
		data["deck"] = Stock.battle.deck;
		data["formation"] = Stock.battle.formation;
		ApiMember.battleLog.data.push(data);
		ApiMember.battleLog.updateTime = Reader.TimeStamp();
	},

	ReadLogBattleResult: function(data)
	{
		data["LogType"] = "result";
		ApiMember.battleLog.data.push(data);
		ApiMember.battleLog.updateTime = Reader.TimeStamp();
	},

	ReadLogBattleCombined: function(data, logType)
	{
		ApiMember.battleLog.data = [];
		data["LogType"] = logType;

		if (data.api_formation)
		{
			Stock.battle.formation = data.api_formation;
		}
		data["deck"] = ApiMember.deck.data[0];
		data["comb"] = ApiMember.deck.data[1];
		data["formation"] = Stock.battle.formation;
		ApiMember.battleLog.data.push(data);
		ApiMember.battleLog.updateTime = Reader.TimeStamp();
	},

	ReadLogBattleResultCombined: function(data)
	{
		data["LogType"] = "resultCombined";
		ApiMember.battleLog.data.push(data);
		ApiMember.battleLog.updateTime = Reader.TimeStamp();
	},

	TimeStamp : function()
	{
		return new Date().getTime();
	}
};





function dump(str){
	try
	{
		var aConsoleService = Components.classes["@mozilla.org/consoleservice;1"].
		getService(Components.interfaces.nsIConsoleService);
		aConsoleService.logStringMessage("[kcfox.dump] callback - " + str);
	}
	catch (e) { }
}